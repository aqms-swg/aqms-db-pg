insert into gazetteertype (code, name, lddate) values (60, 'volcano', current_timestamp);
insert into gazetteertype (code, name, lddate) values (10, 'town', current_timestamp);
insert into gazetteertype (code, name, lddate) values (12, 'bigTown', current_timestamp);
insert into gazetteertype (code, name, lddate) values (30, 'quarry', current_timestamp);
insert into gazetteertype (code, name, lddate) values (40, 'quake', current_timestamp);
insert into gazetteertype (code, name, lddate) values (601, 'fault', current_timestamp);
