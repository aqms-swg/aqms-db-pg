-- RSN regions, please check your polygon, might be outdated.
delete from gazetteer_region where name='AK';
insert into gazetteer_region (name,border) values ('AK',ST_geographyfromtext('SRID=4326;POLYGON((-140 71,
-140 57,
-156 55,
-164 59,
-169 63,
-169 71,
-140 71
))') );
delete from gazetteer_region where name='AT';
insert into gazetteer_region (name,border) values ('AT',ST_geographyfromtext('SRID=4326;POLYGON((-160 41,
-160 41.1,
-159.9 41.1,
-159.9 41,
-160 41
))') );
delete from gazetteer_region where name='CI';
insert into gazetteer_region (name,border) values ('CI',ST_geographyfromtext('SRID=4326;POLYGON((-117.76 37.43,
-121.25 34.5,
-118.5 31.5,
-114 31.5,
-114 34.5,
-117.76 37.43
))') );
delete from gazetteer_region where name='HK_SOCAL';
insert into gazetteer_region (name,border) values ('HK_SOCAL',ST_geographyfromtext('SRID=4326;POLYGON((-117.76 37.43,
-121.25 34.5,
-118.5 31.5,
-114 31.5,
-114 34.5,
-117.76 37.43
))') );
delete from gazetteer_region where name='HV';
insert into gazetteer_region (name,border) values ('HV',ST_geographyfromtext('SRID=4326;POLYGON((-155 18.8333,
-154.75 19.5,
-154.9167 20,
-155.25 20.25,
-155.9167 20.3333,
-156.5 20,
-156.1667 19.1667,
-155.6667 18.75,
-155 18.8333
))') );
delete from gazetteer_region where name='LD';
insert into gazetteer_region (name,border) values ('LD',ST_geographyfromtext('SRID=4326;POLYGON((-72.5 46.5,
-72.5 45.4,
-73.5 40.5,
-73 38,
-75.4 38,
-76.3 38,
-77.25 38.35,
-77.25 39,
-80.5 39.5,
-80.5 43.5,
-75 46.5,
-72.5 46.5
))') );
delete from gazetteer_region where name='MB';
insert into gazetteer_region (name,border) values ('MB',ST_geographyfromtext('SRID=4326;POLYGON((-110 45.1667,
-111.3333 45.1667,
-111.3333 44.5,
-113 44.5,
-116.05 47.95,
-115 48.5,
-113 48.5,
-109.5 46,
-109.5 45.1667,
-110 45.1667
))') );
delete from gazetteer_region where name='NC';
insert into gazetteer_region (name,border) values ('NC',ST_geographyfromtext('SRID=4326;POLYGON((-121.25 34.5,
-117.76 37.43,
-120 39,
-120 42,
-122.7 42,
-125 43,
-126 43,
-126 40,
-121.25 34.5
))') );
delete from gazetteer_region where name='NE';
insert into gazetteer_region (name,border) values ('NE',ST_geographyfromtext('SRID=4326;POLYGON((-73.5 40.5,
-72.5 45.4,
-69.3 47.5,
-67.5 47.3,
-66.6 44.7,
-69.8 41.2,
-73.5 40.5
))') );
delete from gazetteer_region where name='NM';
insert into gazetteer_region (name,border) values ('NM',ST_geographyfromtext('SRID=4326;POLYGON((-86.6797 37.627,
-88.6203 36.627,
-88.4165 34.0273,
-93.5 34,
-93.5 38,
-91.5 40,
-85 40,
-85 38.8,
-86.6797 37.627
))') );
delete from gazetteer_region where name='NN';
insert into gazetteer_region (name,border) values ('NN',ST_geographyfromtext('SRID=4326;POLYGON((-114 34.5,
-117.76 37.43,
-120 39,
-120 41,
-119.2 41.5,
-118.3 41.3,
-116 40,
-114.25 38.25,
-114.25 36.75,
-114 36.75,
-114 34.5
))') );
delete from gazetteer_region where name='PR';
insert into gazetteer_region (name,border) values ('PR',ST_geographyfromtext('SRID=4326;POLYGON((-69 17,
-63.5 17,
-63.5 20,
-69 20,
-69 17
))') );
delete from gazetteer_region where name='SE';
insert into gazetteer_region (name,border) values ('SE',ST_geographyfromtext('SRID=4326;POLYGON((-88.6203 36.627,
-86.6797 37.6702,
-82.1907 37.2553,
-81 39,
-77.25 39,
-77.25 38.35,
-76.3 38,
-75.4 38,
-75.5817 35.953,
-79.6552 32.2235,
-81.0937 30.7383,
-79.8295 26.9808,
-80.2632 24.7603,
-81.473 24.9113,
-85.2255 30.0472,
-85.891 33.4813,
-88.4165 34.0273,
-88.6203 36.627
))') );
delete from gazetteer_region where name='UU';
insert into gazetteer_region (name,border) values ('UU',ST_geographyfromtext('SRID=4326;POLYGON((-108.75 36.75,
-114.25 36.75,
-114.25 42.5,
-108.75 42.5,
-108.75 36.75
))') );
delete from gazetteer_region where name='UW';
insert into gazetteer_region (name,border) values ('UW',ST_geographyfromtext('SRID=4326;POLYGON((-123.3221 49,
-123.0078 48.831,
-123.0091 48.7674,
-123.2741 48.6928,
-123.2189 48.5488,
-123.1605 48.4536,
-123.1155 48.423,
-123.2481 48.2843,
-123.5423 48.2243,
-123.6817 48.2404,
-124.0119 48.2967,
-124.1256 48.3302,
-124.126 48.3291,
-124.7484 48.4997,
-124.7632 48.4997,
-126 48.5,
-125.6 44.5,
-126 43,
-126 43,
-126 42,
-120 42,
-117 42,
-117 49,
-123.3221 49
))') );
delete from gazetteer_region where name='WY';
insert into gazetteer_region (name,border) values ('WY',ST_geographyfromtext('SRID=4326;POLYGON((-109.75 44,
-111.3333 44,
-111.3333 45.1667,
-109.75 45.1667,
-109.75 44
))') );
