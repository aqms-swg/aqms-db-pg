# Types in AQMS schema  
  
These are the database types that are used by AQMS code.  
  
  
``create_types.sql`` creates types that are ancillary descriptive data for quarries.  

``grant_types.sql`` grants priveleges for user created types.  
    
``install_types.sql`` wrapper script to run all the above.  


  
    




  

  
  

  
