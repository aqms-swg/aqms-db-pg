-- used by geo_region
CREATE OR REPLACE TYPE latlon AS (
    lat       numeric(9,7),
    lon       numeric(10,7)
);

