--- run as user postgres on all databases
--- run after creating roles.
---
--- TEMPLATE PASSWORDS
--- First create user trinetdb, as this user will own the main schema
--- Also create the schema here and grant usage to the group-roles as well as code.
---- TRINETDB
create user trinetdb with login password 'TRINETDB_PASS';
grant trinetdb_execute to trinetdb;
create schema if not exists trinetdb authorization trinetdb;
grant usage on schema trinetdb to trinetdb_read, trinetdb_write, trinetdb_execute;
grant usage on schema pglogical to trinetdb;

---- BROWSER
create user browser with login password 'BROWSER_PASS';
grant trinetdb_read to browser;

---- RTEM
create user rtem with login password 'RTEM_PASS';
grant trinetdb_write to rtem;
grant trinetdb_execute to rtem;
grant trinetdb_read to rtem;

---- TPP
create user tpp with login password 'TPP_PASS';
grant trinetdb_read to tpp;
grant trinetdb_write to tpp;
grant trinetdb_execute to tpp;

---- DCARCHIVER
create user dcarchiver with login password 'DCARCHIVER_PASS';
grant trinetdb_read to dcarchiver;
grant trinetdb_write to dcarchiver;
grant trinetdb_execute to dcarchiver;

---- OPERATOR
create user operator with login password 'OPERATOR_PASS';
grant trinetdb_write to operator;
grant trinetdb_execute to operator;

---- CODE
create user code with login password 'CODE_PASS';
grant trinetdb_read to code;
grant trinetdb_write to code;
create schema if not exists code authorization code;
-- schemas to hold stored procedures
create schema if not exists assocamp authorization code;
create schema if not exists db_cleanup authorization code;
create schema if not exists epref authorization code;
create schema if not exists geo_region authorization code;
create schema if not exists magpref authorization code;
create schema if not exists match authorization code;
create schema if not exists pcs authorization code;
create schema if not exists quarry authorization code;
create schema if not exists requestcard authorization code;
create schema if not exists sequence authorization code;
create schema if not exists tools authorization code;
create schema if not exists truetime authorization code;
create schema if not exists util authorization code;
create schema if not exists wavefile authorization code;
create schema if not exists wave authorization code;
create schema if not exists wheres authorization code;
grant usage on schema assocamp to trinetdb_read, trinetdb_execute;
grant usage on schema db_cleanup to trinetdb_read, trinetdb_execute;
grant usage on schema epref to trinetdb_read, trinetdb_execute;
grant usage on schema geo_region to trinetdb_read, trinetdb_execute;
grant usage on schema magpref to trinetdb_read, trinetdb_execute;
grant usage on schema match to trinetdb_read, trinetdb_execute;
grant usage on schema pcs to trinetdb_read, trinetdb_execute;
grant usage on schema quarry to trinetdb_read, trinetdb_execute;
grant usage on schema requestcard to trinetdb_read, trinetdb_execute;
grant usage on schema sequence to trinetdb_read, trinetdb_execute;
grant usage on schema tools to trinetdb_read, trinetdb_execute;
grant usage on schema truetime to trinetdb_read, trinetdb_execute;
grant usage on schema code to trinetdb_read,trinetdb_execute;
grant usage on schema util to trinetdb_read,trinetdb_execute;
grant usage on schema wavefile to trinetdb_read,trinetdb_execute;
grant usage on schema wave to trinetdb_read,trinetdb_execute;
grant usage on schema wheres to trinetdb_read,trinetdb_execute;
grant usage on schema trinetdb to code;

---- REPADMIN
create user repadmin with login password 'REPADMIN_PASS' replication;

---- SET SEARCH_PATH (pg equivalent of public synonyms)
---- every role needs usage to trinetdb, code, and their own schemas, as well as public, otherwise
---- postgis functions have to be prefixed with "public."
alter role trinetdb set search_path to trinetdb, code, public;
alter role browser set search_path to trinetdb,code,browser, public;
alter role rtem set search_path to trinetdb, code, rtem, public;
alter role tpp set search_path to trinetdb, code, tpp, public;
alter role dcarchiver set search_path to trinetdb, code, dcarchiver, public;
alter role operator set search_path to trinetdb, code, operator, public;
alter role code set search_path to code, trinetdb, public;
