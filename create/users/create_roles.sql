--- RUN AS postgres on all databases
--- run before creating users
---
create role trinetdb_read;
create role trinetdb_write;
create role waveform_read;
create role trinetdb_execute;
grant trinetdb_read to trinetdb_write;
grant waveform_read to trinetdb_read;

