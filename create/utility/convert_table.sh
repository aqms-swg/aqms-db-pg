#!/bin/bash

# read variable settings pointing to location of original oracle table creation scripts
# and destination for new PostgreSQL creating scripts.

if [ "x${BINDIR}x" == "xx" ] ; then
    echo "Set environment variable BINDIR to directory that convert_all_tables is running from"
    exit
fi
source ${BINDIR}/paths.env
#ORA_SRC --> source
#PG_DEST  --> destination

#### example how to use convert_table.sh:
# convert_table.sh create_ARRIVAL.sql
# creates a file pg_create_ARRIVAL.sql with PostgreSQL syntax in directory PG_DEST
#
# references:
#
# http://docs.oracle.com/cd/B19306_01/server.102/b14200/sql_elements001.htm
# http://www.postgresql.org/docs/9.4/static/datatype.html
#
# Explanation of substitutions done below:
#
# --------------------------------------------------------------
# ORACLE DATATYPE/identifier     => PostgreSQL DATATYPE/identifier
# --------------------------------------------------------------
# NUMBER(1-4, 0), number(1-4, 0) => SMALLINT -32768 to +32767
# NUMBER(1-4,0), number(1-4,0)   => SMALLINT -32768 to +32767
# NUMBER(5-9, 0), number(5-9, 0) => INTEGER  -2147483648 to +2147483647
# NUMBER(5-9,0), number(5-9,0)   => INTEGER  -2147483648 to +2147483647
# NUMBER(>9, 0), number(>9, 0)   => BIGINT   -9223372036854775808 to +9223372036854775807
# NUMBER                         => DOUBLE PRECISION (a few exceptions, BIGINT, or INTEGER)
# VARCHAR2, varchar2, VarChar2   => VARCHAR (=CHARACTER VARYING)
# DATE                           => TIMESTAMP
# DATE,                          => TIMESTAMP,
# DATE$                          => TIMESTAMP
# date                           => TIMESTAMP 
# SYSDATE                        => LOCALTIMESTAMP
# FLOAT()                        => DOUBLE PRECISION
# FLOAT                          => DOUBLE PRECISION                     
# BLOB                           => BYTEA
# NOBS NUMBER                    => NOBS INTEGER
# cast (sys_extract_utc(systimestamp) as date) => CURRENT_TIMESTAMP AT TIME ZONE 'UTC'
# cast (sys_extract_utc(systimestamp) as       => CURRENT_TIMESTAMP AT TIME ZONE 'UTC'
# cast (sys_extract_utc(systimestamp)          => CURRENT_TIMESTAMP AT TIME ZONE 'UTC'
# as date)                       => (remove, due to cast() wrapped to next line)
#  date)                         => (remove, due to cast() wrapped to next line)
#date                            => (remove, due to cast() wrapped to next line)
# SYS_EXTRACT_UTC(SYSTIMESTAMP)  => CURRENT_TIMESTAMP AT TIME ZONE 'UTC'
# create or replace public synonym => commented out (the equivalent in PostgreSQL is to explicitely 
#                                   set the search_path for each role (user) to include the schema 
#                                   names that need to be accessible)
# grant or GRANT                 => commented out, don't do this in creation script
# "                              => remove, double quotes are part of the identifier 
#                                   when used (i.e. table SYSTEM_STATUS would be "SYSTEM_STATUS")
# ENABLE                         => remove, unknown by PostgreSQL
# DISABLE                        => remove the whole line, unknown by PostgreSQL (cannot disable check constraint)
# offset  (=a table column name) => i_offset, OFFSET is a reserved keyword in PostgreSQL 
#                                   (and in SQL 2008 standard)
#                                   http://www.postgresql.org/docs/9.4/static/sql-keywords-appendix.html
# QB_SCHEDULE                    => use _QB_TIME instead
# COORDINATES                    => use _LATLON instead
# catseq.nextval                 => use nextval('catseq') instead
# /                              => change to semi-colon
# --------------------------------------------------------------

cd $ORA_SRC
file_name=$1
echo "Converting $file_name in $ORA_SRC and putting new file in $PG_DEST"

grep -v "DISABLE" $file_name | sed -e "s:\NUMBER([1-4],\s*[0]):SMALLINT:"i \
     -e "s:NUMBER([1-4]):SMALLINT:"i \
     -e "s:\NUMBER([5-9],\s*[0]):INTEGER:"i \
     -e "s:NUMBER([5-9]):INTEGER:"i \
     -e "s:NUMBER([^0]\+,\s*[0]):BIGINT:"i \
     -e "s: NUMBER([0-9]\+,[0-9]\+): DOUBLE PRECISION:"i \
     -e "s: NUMBER([0-9]\+, [0-9]\+): DOUBLE PRECISION:"i \
     -e "s:NUMBER:DOUBLE PRECISION:"i \
     -e "s:VARCHAR2:VARCHAR:"i \
     -e "s: DATE : TIMESTAMP :" \
     -e "s: DATE,: TIMESTAMP,:" \
     -e "s: DATE$: TIMESTAMP:" \
     -e "s: date : TIMESTAMP :" \
     -e "s:SYSDATE:CURRENT_TIMESTAMP AT TIME ZONE 'UTC':" \
     -e "s:FLOAT([^0]\+):DOUBLE PRECISION:" \
     -e "s:FLOAT:DOUBLE PRECISION:" \
     -e "s:WCOPY DOUBLE PRECISION:WCOPY INTEGER:" \
     -e "s:NOBS DOUBLE PRECISION:NOBS INTEGER:" \
     -e "s:WFID DOUBLE PRECISION:WFID BIGINT:" \
     -e "s:ID DOUBLE PRECISION:ID BIGINT:" \
     -e "s:SECONDARYID DOUBLE PRECISION:SECONDARYID BIGINT:" \
     -e "s:LASTID DOUBLE PRECISION:LASTID BIGINT:" \
     -e "s: LDDATE\s*TIMESTAMP,: LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),:"i \
     -e "s:\sLDDATE TIMESTAMP NOT NULL ,:LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC') NOT NULL,:"i \
     -e "s:\sLDDATE TIMESTAMP,: LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),:"i \
     -e "s:\sLDDATE TIMESTAMP $: LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'):"i \
     -e "s:DEFAULT CURRENT_TIMESTAMP AT TIME ZONE 'UTC':DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'):"i \
     -e "s:DOUBLE PRECISION(15):BIGINT:"i \
     -e "s:BLOB:BYTEA:"i \
     -e "s:cast (sys_extract_utc(systimestamp) as date):CURRENT_TIMESTAMP AT TIME ZONE 'UTC':" \
     -e "s:cast (sys_extract_utc(systimestamp) as:CURRENT_TIMESTAMP AT TIME ZONE 'UTC':" \
     -e "s:cast (sys_extract_utc(systimestamp):CURRENT_TIMESTAMP AT TIME ZONE 'UTC':" \
     -e "s:as date)::" \
     -e "s:\sdate)::" \
     -e "s:^date)::" \
     -e "s:SYS_EXTRACT_UTC(SYSTIMESTAMP):CURRENT_TIMESTAMP AT TIME ZONE 'UTC':" \
     -e "s:create or replace public synonym:-- create or replace public synonym:" \
     -e "s:^grant:-- grant:"i \
     -e "s:\"::"g \
     -e "s:ENABLE::"gi \
     -e "s:offset:i_offset:"i \
     -e "s:QB_SCHEDULE:_qb_time:"i \
     -e "s:COORDINATES:_latlon:"i \
     -e "s:catseq.nextval:nextval('catseq'):" \
     -e "s:staDOUBLE PRECISION:stanumber:" \
     -e "s:"^\/$":;:" \
		> ${PG_DEST}/${file_name}

