/**
 * fillWaveformBlob.c
 * Postress extension to read miniseed channel data from AQMS triggers
 * and archived waveform files.
 * Victor Kress, PNSN 2016/12/12
 */

#include <stdio.h>
#include "postgres.h"
#ifdef PG_VERSION_NUM
#if PG_VERSION_NUM >= 160000
#include "varatt.h"
#endif
#endif
#include "fmgr.h"
#include "libmseed.h"

#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

/**
Utility function to select subset of data within specified time range.
Wraps libmseed's msr_parse_selection() function
@param dbuf On input, start of buffer. On return first data > startt
@param nbytes On input, size of buffer. On output nbytes where startt<datatime<endtt
@param startt start of time window
@param endt end of time window
*/
void _selectTimeRange(char *dbuf,int *nread,double startt,double endt) {
  char *dstart=NULL;
  MSRecord *msr = NULL;
  Selections *sel = NULL;
  int reclen=-1,dflag=0,verb=0;
  int64_t offset=0,nbuf=*nread;

  ms_addselect(&sel, "*", startt, endt);
  /* Loop over all records in dbuf */
  while (offset<nbuf) {
    if (msr_parse_selection(dbuf,*nread,&offset,&msr,reclen,sel,dflag,verb)) {
      /*success*/
      if (dstart==NULL) {
	dstart=dbuf+offset;
      }
      /* Incr offset in buffer for subsequent call to msr_parse_selection() */
      offset += msr->reclen;
    }
    else { /*done*/
      dbuf=dstart;
      *nread=offset;
      break;
    }
  }

  /* Clean up */
  msr_free (&msr);
  ms_freeselections (sel);
  return;
}

PG_FUNCTION_INFO_V1(getWaveformBlob);

/**
PostgreSQL c extension for reading waveform data from file system into Postgres byte array.
If endt>startt, subset of data between startt and endt will be returned.
Set both to zero, NULL or equal) to skip check.
@param filename fully qualified name of file to read
@param offset offset in file to start reading
@param nbytes number of bytes to read
@param startt only return data later than this time (UTC seconds)
@param endt only return data earlier than this time (UTC seconds)
@param return newly allocated postgres bytea array.
*/ 
Datum getWaveformBlob(PG_FUNCTION_ARGS) {
  FILE *fp;
  text *tfilename;
  int64 offset;
  int64 nbytes;
  float8 startt;
  float8 endt;
  char *buffer,*datastart;
  int nread;

  tfilename = PG_GETARG_TEXT_P(0);
  offset = PG_GETARG_INT64(1);
  nbytes = PG_GETARG_INT64(2);
  if (PG_ARGISNULL(3)){
    startt = 0.;
  }
  else {
    startt = PG_GETARG_FLOAT8(3);
  }
  if (PG_ARGISNULL(4)){
    endt = 0.;
  }
  else {
    endt = PG_GETARG_FLOAT8(4);
  }
  buffer=palloc(nbytes*sizeof(char));
  datastart=buffer;

  /*read data into buffer*/
  /*following is ugly but necessary*/
  size_t fnlen=VARSIZE(tfilename)-VARHDRSZ;
  char *fn=palloc(fnlen+1);
  memcpy(fn,VARDATA(tfilename),fnlen);
  fn[fnlen]='\0';
  fp = fopen(fn,"r");
  if (fp) {
    fseek(fp,offset,SEEK_SET);
    for (nread=0;nread<nbytes;nread++) {
      buffer[nread]=fgetc(fp);
      if (feof(fp)) {
	break;
      }
    }
    fclose(fp);
    if (endt>startt) {
      _selectTimeRange(datastart,&nread,startt,endt);
    } 
  } else {
    /* clients seem to want zero-length bytea instead of error
    ereport(ERROR,
	    (errcode(ERRCODE_IO_ERROR),
	     errmsg("getWaveformBlob(): Could not open file %s",fn)));
    PG_RETURN_NULL();
    */
    nread=0;
  }
  /*allocate and fill bytea*/
  bytea *rval = (bytea *)palloc(nread+VARHDRSZ);
  if (nread>0) {
    memcpy(VARDATA(rval),datastart,nread);
  }
  /*  clients seem to want zero-length bytea without error
  else {
    ereport(WARNING,
	    (errcode(ERRCODE_IO_ERROR),
	     errmsg("getWaveformBlob(): No valid data found in %s",fn)));
  }
  */
  
  /*clean up*/  
  pfree(buffer);
  SET_VARSIZE(rval,nread+VARHDRSZ);
  PG_RETURN_BYTEA_P(rval);
}
