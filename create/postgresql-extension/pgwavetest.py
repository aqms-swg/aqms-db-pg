#!/usr/bin/python
#file to test wave package on postgres
import sys
import obspy
import cStringIO
import psycopg2 as pspg

datdir='/usr/local/share/pgsql/'
msfile='testmseed/60512982'
#condict={'database':'rtdb1','user':'trinetdb','password':''}

ri=obspy.io.mseed.util.get_record_information(datdir+msfile)
print ri
fms=obspy.read(datdir+msfile)

#get waveform
con=pspg.connect(**condict)
curs=con.cursor()
resp='c'
offst=0
for ms in fms:
    #next JUST gets segment length (nbyte).  Ugly, but only way.
    msbuf=cStringIO.StringIO()
    ms.write(msbuf,format="MSEED",reclen=ms.stats.mseed['record_length'])
    nbyte=len(msbuf.getvalue())
    print 'nbyte=%d'%nbyte
    msbuf.close()
    startt=ms.stats['starttime'].timestamp
    endt=ms.stats['endtime'].timestamp
    print offst,nbyte,startt,endt
    startt=endt=0.
    print 'From file:'
    print ms.stats
    curs.callproc('wave.get_waveform_blob',[datdir+msfile,offst,nbyte,startt,endt])
    dbmseed=curs.fetchone()
    print len(dbmseed)
    membuf=cStringIO.StringIO(dbmseed[0])
    print membuf
    dbms=obspy.read(membuf)
    membuf.close()
    print 'From db:'
    print dbms
    print dbms[0].stats
    obspy.Stream(traces=[ms,dbms[0]]).plot(automerge=False)
    print ''
    if raw_input('c to continue with next record, q to exit: ')=='c':
        offst+=nbyte
        continue
    else:
        break
curs.close()
con.close()

sys.exit(0)
