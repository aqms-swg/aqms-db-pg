#!/bin/bash

# set variables
source pg.env
CMD="${PSQLDIR}/psql -p $PGPORT"
BASEDIR=$PWD

if [ "$USER" != "postgres" ]; then
   echo "Warning, you are running this as user $USER in stead of postgres,"
   echo "unless you changed the authentication method for postgres db user from"
   echo "peer to md5, and altered the first two psql commands in this file to "
   echo "prompt for a password, this will not work."
   echo ""
   echo "Do you want to continue?"
   select yn in "Yes" "No" ; do
   case $yn in
      Yes ) echo "OK, continuing"; break;;
      No ) exit;;
  esac
  done
fi


if [ "$DBNAME" = "" ]; then
  echo "DBNAME is not defined, source "pg.env" as postgres first. Exiting.."
  exit
fi

if [ -e ${HOME}/.pgpass ]; then
   PWFLAG=-w   # will not prompt for password
else
   PWFLAG=-W   # will prompt for password
fi

echo "\n next: Creating database ${DBNAME}. Continue?"
select yn in "Yes" "Skip" "Exit"; do
case $yn in
   Yes ) break;;
   Skip ) break;;
   Exit ) exit;;
esac
done

if [ $yn != "Skip" ]; then
    echo "--> Creating AQMS database $DBNAME..."
    echo "$CMD < ${BASEDIR}/db/create_${DBNAME}.sql"
    $CMD < ${BASEDIR}/db/create_${DBNAME}.sql
fi

echo "\n next: Activating PostGIS extensions in ${DBNAME}. Continue?"
select yn in "Yes" "Skip" "Exit"; do
case $yn in
   Yes ) break;;
   Skip ) break;;
   Exit ) exit;;
esac
done

if [ $yn != "Skip" ]; then
    echo "--> Activating PostGIS extensions in database $DBNAME as user postgres..."
    echo "$CMD -d ${DBNAME} < ${BASEDIR}/db/activate_extensions.sql"
    $CMD -d ${DBNAME} < ${BASEDIR}/db/activate_extensions.sql
fi

echo "\n next: Creating roles w/o login in ${DBNAME}. Continue?"
select yn in "Yes" "Skip" "Exit"; do
case $yn in
   Yes ) break;;
   Skip ) break;;
   Exit ) exit;;
esac
done

if [ $yn != "Skip" ]; then
    echo "--> Creating database roles as user postgres..."
    echo "$CMD < ${BASEDIR}/users/create_roles.sql"
    $CMD < ${BASEDIR}/users/create_roles.sql
fi

echo "\n next: Creating roles with login in ${DBNAME}, schemas, and granting privs. Continue?"
select yn in "Yes" "Skip" "Exit"; do
case $yn in
   Yes ) break;;
   Skip ) break;;
   Exit ) exit;;
esac
done

if [ $yn != "Skip" ]; then
    echo "--> Creating database users and schemas, and setting user search_path in $DBNAME as user postgres..."
    echo "$CMD -d $DBNAME < ${BASEDIR}/users/create_users.sql"
    $CMD -d $DBNAME < ${BASEDIR}/users/create_users.sql
fi

echo "\n Done."
