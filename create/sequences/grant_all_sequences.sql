--- This script grants privileges to roles for all the sequences in TRINETDB schema.
--- If you want to grant privileges per sequence instead, comment out the "on all sequences" lines 
--- and add individual grant statements.
--- run as TRINETDB user
--- select priv gives access to currval
--- update priv gives access to nextval and setval
--- usage priv gives access to currval and nextval but not setval
grant select on all sequences in schema trinetdb to trinetdb_read, code;
grant select, update on all sequences in schema trinetdb to trinetdb_write, code;
