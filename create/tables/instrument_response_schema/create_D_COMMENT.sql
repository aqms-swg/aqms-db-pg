-- 
-- TABLE: D_Comment 
--

CREATE TABLE D_Comment(
    id             INTEGER    NOT NULL,
    class          VARCHAR(1)     NOT NULL,
    description    VARCHAR(70),
    unit           INTEGER    NOT NULL,
    CONSTRAINT D_C00 PRIMARY KEY (id)
);
