-- 
-- TABLE: DC 
--

CREATE TABLE DC(
    key         INTEGER    NOT NULL,
    name        VARCHAR(80),
    symmetry    VARCHAR(1),
    storage     VARCHAR(1),
    LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT DC00 PRIMARY KEY (key)
);

