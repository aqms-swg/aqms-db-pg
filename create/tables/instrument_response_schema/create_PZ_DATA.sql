-- 
-- TABLE: PZ_Data 
--

CREATE TABLE PZ_Data(
    key        INTEGER        NOT NULL,
    row_key    INTEGER        NOT NULL,
    type       VARCHAR(1),
    r_value    DOUBLE PRECISION    NOT NULL,
    r_error    DOUBLE PRECISION,
    i_value    DOUBLE PRECISION    NOT NULL,
    i_error    DOUBLE PRECISION,
    CONSTRAINT PZD00 PRIMARY KEY (key, row_key)
);
