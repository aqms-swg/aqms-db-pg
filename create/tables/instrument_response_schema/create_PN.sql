-- 
-- TABLE: PN 
--

CREATE TABLE PN(
    key            INTEGER        NOT NULL,
    name           VARCHAR(80),
    poly_type      VARCHAR(1),
    lower_bound    DOUBLE PRECISION,
    upper_bound    DOUBLE PRECISION,
    max_error      DOUBLE PRECISION,
    LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT PN00 PRIMARY KEY (key)
);
