-- 
-- TABLE: D_Format_Data 
--

CREATE TABLE D_Format_Data(
    id        INTEGER    NOT NULL,
    row_id    INTEGER    NOT NULL,
    key_d     VARCHAR(80)    NOT NULL,
    CONSTRAINT D_F_D00 PRIMARY KEY (id, row_id)
);
