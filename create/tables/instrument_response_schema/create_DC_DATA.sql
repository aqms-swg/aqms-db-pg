-- 
-- TABLE: DC_Data 
--

CREATE TABLE DC_Data(
    key            INTEGER        NOT NULL,
    row_key        INTEGER        NOT NULL,
    type           VARCHAR(1),
    coefficient    DOUBLE PRECISION    NOT NULL,
    error          DOUBLE PRECISION,
    CONSTRAINT DCD00 PRIMARY KEY (key, row_key)
);
