-- 
-- TABLE: Decimation 
--

CREATE TABLE Decimation(
    net           VARCHAR(8)     NOT NULL,
    sta           VARCHAR(6)     NOT NULL,
    seedchan      VARCHAR(3)     NOT NULL,
    location      VARCHAR(2)     NOT NULL,
    ondate        TIMESTAMP            NOT NULL,
    stage_seq     INTEGER    NOT NULL,
    channel       VARCHAR(8),
    channelsrc    VARCHAR(8),
    offdate       TIMESTAMP,
    dm_key        INTEGER    NOT NULL,
    LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT De00 PRIMARY KEY (net, sta, seedchan, location, ondate, 
	stage_seq
)
);
