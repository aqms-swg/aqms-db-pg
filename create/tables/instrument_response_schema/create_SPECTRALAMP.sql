
DROP TABLE SPECTRALAMP;
DROP SEQUENCE SPECTRALAMPSEQ;

 CREATE TABLE SPECTRALAMP
 (AMPID BIGINT,
  COMMID BIGINT,
  STA VARCHAR(6) NOT NULL ,
  NET VARCHAR(8),
  AUTH VARCHAR(15) NOT NULL ,
  SUBSOURCE VARCHAR(8),
  CHANNEL VARCHAR(8),
  CHANNELSRC VARCHAR(8),
  SEEDCHAN VARCHAR(3),
  LOCATION VARCHAR(2),
  STARTTIME      DOUBLE PRECISION NOT NULL ,
  DURATION       DOUBLE PRECISION,
  SPECTYPE       VARCHAR(64) NOT NULL,
  DESCRIPTION    VARCHAR(128),
  UNITS_X        VARCHAR(8) NOT NULL,
  UNITS_Y        VARCHAR(8) NOT NULL,
  DAMPING        DOUBLE PRECISION,
  RFLAG VARCHAR(2),
  XYDATA BYTEA,
  LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),

  CONSTRAINT SPECTRALAMP01 CHECK (ampid > 0) ,
  CONSTRAINT SPECTRALAMP12 CHECK (rflag in ('a','h','f','A','H','F')) ,
  CONSTRAINT SPECTRALAMPKEY01 PRIMARY KEY (AMPID)
);

COMMENT ON TABLE SPECTRALAMP IS 'Spectral Amplitude Measurements';
COMMENT ON COLUMN SPECTRALAMP.AMPID IS 'Unique numerical identifier of amplitude measurement (primary key)';
COMMENT ON COLUMN SPECTRALAMP.COMMID IS 'implied foreign key to the REMARK table';
COMMENT ON COLUMN SPECTRALAMP.STA IS 'Station code';
COMMENT ON COLUMN SPECTRALAMP.NET IS 'Network code';
COMMENT ON COLUMN SPECTRALAMP.AUTH IS 'Authority code (usually same as PDL source code)';
COMMENT ON COLUMN SPECTRALAMP.SUBSOURCE IS 'source of amplitude measurement, e.g. RT1, or Jiggle';

CREATE SEQUENCE SPECTRALAMPSEQ
    START WITH 1000001
    INCREMENT BY 5
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
;

ALTER TABLE SPECTRALAMP ALTER COLUMN AMPID SET DEFAULT nextval('SPECTRALAMPSEQ');
