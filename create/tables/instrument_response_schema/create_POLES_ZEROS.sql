CREATE TABLE Poles_Zeros(
    net           VARCHAR(8)         NOT NULL,
    sta           VARCHAR(6)         NOT NULL,
    seedchan      VARCHAR(3)         NOT NULL,
    location      VARCHAR(2)         NOT NULL,
    ondate        TIMESTAMP                NOT NULL,
    stage_seq     INTEGER        NOT NULL,
    channel       VARCHAR(8),
    channelsrc    VARCHAR(8),
    offdate       TIMESTAMP,
    pz_key        INTEGER        NOT NULL,
    tf_type       VARCHAR(1),
    unit_in       INTEGER        NOT NULL,
    unit_out      INTEGER        NOT NULL,
    AO            DOUBLE PRECISION    NOT NULL,
    AF            DOUBLE PRECISION,
    LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT P_Z00 PRIMARY KEY (net, sta, seedchan, location, ondate, 
	stage_seq)
);

