-- 
-- TABLE: Sensitivity 
--

CREATE TABLE Sensitivity(
    net            VARCHAR(8)         NOT NULL,
    sta            VARCHAR(6)         NOT NULL,
    seedchan       VARCHAR(3)         NOT NULL,
    location       VARCHAR(2)         NOT NULL,
    ondate         TIMESTAMP                NOT NULL,
    stage_seq      INTEGER        NOT NULL,
    channel        VARCHAR(8),
    channelsrc     VARCHAR(8),
    offdate        TIMESTAMP,
    sensitivity    DOUBLE PRECISION    NOT NULL,
    frequency      DOUBLE PRECISION,
    LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT Se00 PRIMARY KEY (net, sta, seedchan, location, ondate, stage_seq
)
);

