-- 
-- TABLE: Simple_Response 
--

CREATE TABLE Simple_Response(
    net                  VARCHAR(8)         NOT NULL,
    sta                  VARCHAR(6)         NOT NULL,
    seedchan             VARCHAR(3)         NOT NULL,
    location             VARCHAR(2)         NOT NULL,
    ondate               TIMESTAMP                NOT NULL,
    channel              VARCHAR(8),
    channelsrc           VARCHAR(8),
    natural_frequency    DOUBLE PRECISION,
    damping_constant     DOUBLE PRECISION,
    gain                 DOUBLE PRECISION,
    gain_units           VARCHAR(20)        NOT NULL,
    low_freq_corner      DOUBLE PRECISION,
    high_freq_corner     DOUBLE PRECISION,
    offdate              TIMESTAMP,
    LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    dlogsens             DOUBLE PRECISION,
    CONSTRAINT SiRe00 PRIMARY KEY (net, sta, seedchan, location, ondate)
);

