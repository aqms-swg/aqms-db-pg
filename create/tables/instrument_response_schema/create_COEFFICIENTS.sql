CREATE TABLE Coefficients(
    net           VARCHAR(8)     NOT NULL,
    sta           VARCHAR(6)     NOT NULL,
    seedchan      VARCHAR(3)     NOT NULL,
    location      VARCHAR(2)     NOT NULL,
    ondate        TIMESTAMP            NOT NULL,
    stage_seq     INTEGER    NOT NULL,
    channel       VARCHAR(8),
    channelsrc    VARCHAR(8),
    offdate       TIMESTAMP,
    dc_key        INTEGER,
    unit_in       INTEGER    NOT NULL,
    unit_out      INTEGER    NOT NULL,
    tf_type       VARCHAR(1),
    LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT Co00 PRIMARY KEY (net, sta, seedchan, location, ondate, 
	stage_seq
)
);
