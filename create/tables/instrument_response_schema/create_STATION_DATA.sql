-- 
-- TABLE: Station_Data 
--

CREATE TABLE Station_Data(
    net        VARCHAR(8)         NOT NULL,
    sta        VARCHAR(6)         NOT NULL,
    ondate     TIMESTAMP                NOT NULL,
    lat        DOUBLE PRECISION,
    lon        DOUBLE PRECISION,
    elev       DOUBLE PRECISION,
    staname    VARCHAR(60),
    net_id     INTEGER,
    word_32    INTEGER        NOT NULL,
    word_16    INTEGER        NOT NULL,
    offdate    TIMESTAMP,
    lddate     TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'
       ),
    CONSTRAINT STD02 CHECK (lat >=-90 and lat <= 90),
    CONSTRAINT STD03 CHECK (lon >=-180 and lon <= 180),
    CONSTRAINT StD00 PRIMARY KEY (net, sta, ondate)
);
