-- 
-- TABLE: D_Unit 
--

CREATE TABLE D_Unit(
    id             INTEGER    NOT NULL,
    name           VARCHAR(80),
    description    VARCHAR(70),
    CONSTRAINT D_U00 PRIMARY KEY (id)
);
