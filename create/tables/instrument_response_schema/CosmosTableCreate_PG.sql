-- set define off;

DROP table C_LoggerData;
DROP table C_SensorData;
DROP table C_PhysTyp;
DROP table C_Units;
DROP table C_RecTyp;
DROP table C_Net;
DROP table C_Datum;
DROP table C_Site;
DROP table C_Auth;
DROP table C_TimeClk;
DROP table C_LgrTyp;
DROP table C_SnsTyp;
DROP table C_FltrTyp;
DROP table C_OrntAbbrv;
DROP table C_Ornt;


create table C_ChannelData (
  net      VARCHAR(8) NOT NULL,
  sta      VARCHAR(6) NOT NULL,
  seedchan VARCHAR(3) NOT NULL,
  location VARCHAR(2) NOT NULL,
  ondate   timestamp        NOT NULL,
  offdate  timestamp        NOT NULL,
  netId    integer,
  lgrName  VARCHAR(48),
  lgrId   integer NOT NULL,
  lgrBits integer,
  lgrFsv  double precision,
  snsName  VARCHAR(48),
  snsId  integer NOT NULL,
  maxG   double precision,
  vPerG   double precision,
  siteId  integer,
  orntId   integer,
  timeId   integer,
  datumId  integer,
  staVs30  double precision,
  staGeoId integer,
  chan_number integer,
  description VARCHAR(60),
  lddate   timestamp DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP),
  ref_azi double precision,
  constraint c_channeldata_pk primary key (net,sta,seedchan,location,ondate)
);


create table C_LoggerData (
  lgrName VARCHAR(48) NOT NULL,
  lgrId   integer    NOT NULL,
  lgrBits integer,
  lgrFsV  double precision,
  constraint c_loggerdata_pk primary key (lgrName)
);
-- /
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 108, 'K2', 24, 5.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 108, 'Kinemetrics-K2', 24, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 108, 'Kinemetrics_K2', 24, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 109, 'Kinemetrics_Etna', 24, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 600, 'Q4120', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 602, 'Q730', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 606, 'Q680', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 604, 'Q935', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 604, 'Q980', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 605, 'Q330', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 605, 'Q330HR', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 700, 'RefTek-72A-07', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 701, 'Reftek-130', 24, 13.4);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'CUSP-Reno-Tustin', 12, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'CUSP-Tustin-2', 12, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'CUSP-VAX/750-Tustin', 12, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'DWSSN', 16, 4.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'NCSN-DST', 16, 4.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'Nanometrics-HRD24', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'Nanometrics-HRD24-multi', 12, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (251, 'Nanometrics-Lynx', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (250, 'Nanometrics-Trident', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (250, 'Nanometrics-Trident-multi', 12, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'National-Carr-Hl', 12, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'National-Fremont-Pk', 12, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'National-Geyser-Peak', 12, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'National-Hollister', 12, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'National-LLNL', 12, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'National-MP', 12, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'National-Mammoth', 12, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'National-Mt-Tamalpais', 12, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'National-PG&E', 12, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'National-Sonoma-Mountain', 12, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'National-UNR', 12, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'National-Vollmer-Peak', 12, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'PDPM', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'UNKN', 24, 20.);

insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 605, 'Q330HR-GFE', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 605, 'Q330S', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 605, 'Q330S+', 24, 20.);

insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 700, 'Reftek-72A-07', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 701, 'Reftek-130-01', 24, 13.4);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 701, 'Reftek-130-ANSS', 24, 13.4);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 701, 'Reftek-130-SMHR', 24, 13.4);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'National-Carr-Hill', 12, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'National-Williams-Hill', 12, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 605, 'Quanterra-Q330', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'Unknown', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 600, 'Q4128', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (650, 'CMG-DM24-EAM', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (131, 'Basalt', 24, 20);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (131, 'Basalt-8', 24, 20);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (109, 'Etna', 24, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (551, 'GSR-IA18', 24, 11.1);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (500, 'Geos', 16, 0);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (130, 'Granite', 24, 20);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (130, 'Granite-multi', 24, 20);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (112, 'Makalu', 24, 20);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'Taurus', 24, 8.388);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'Tsa-sma', 24, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (110, 'Whitney', 24, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (110, 'Whitney-multi', 24, 2.5);

insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (10, 'Cra-1', 24, 20);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (251, 'Nanometrics-Lynx-multi', 24, 20);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (7, 'Sma-1', 24, 20);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (131, 'Basalt-multi', 24, 20);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (1000, 'GERI', 24, 20);

insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (651, 'MINIMUS+', 24, 20.);

insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (109, 'ETNA-2', 24, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (109, 'Etna-2', 24, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (109, 'Etna2', 24, 2.5);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (131, 'Basalt-4x', 24, 20);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (131, 'Basalt-8x', 24, 20);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (131, 'BASALT8X', 24, 20);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (131, 'Basalt-internal-epi', 24, 20);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (605, 'Q330S+ USGS', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values (605, 'Quanterra-Q330S+', 24, 20.);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 701, 'Reftek-130S-01', 24, 13.4);
insert into C_LoggerData (lgrId,lgrName,lgrBits,lgrFsV) values ( 701, 'Reftek-130-SMA', 24, 13.4);


create table C_SensorData (
  snsName  VARCHAR(48) NOT NULL,
  snsId    integer   NOT NULL,
  maxG     double precision,
  vPerG double precision,
  constraint c_sensordata_pk primary key (snsName)
);
--/

insert into C_SensorData (snsId, snsName, maxG, vPerG) values (200, 'CMG-5', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (4, 'FBA-11', 3.0, 1.25);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (4, 'FBA_11', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (7, 'Kinemetrics FBA-23 Accel.', 2., 1.25);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (7, 'Kinemetrics FBA-23 Triaxial Accelerometer', 2., 1.25);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (7, 'FBA-23', 2., 1.25);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (7, 'FBA23', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (8, 'FBA-23DH', 1., 2.5);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20, 'Kinemetrics FBA ES-T Triaxial Accelerometer', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20, 'Kinemetrics FBA ES-T Accel.', 2., 10.);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20, 'Episensor', 4., 5.0);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20, 'K2 Episensor', 4., 1.25);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20, 'FBA ES-T', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20, 'FBAEST', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20, 'K2_EPI', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20, 'FBA-D', null,  null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (150, 'Wilcoxon 731A Accel.', 0.5, 10.);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (480, 'TSA-100S', 4., 5.);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (255, 'RT-131A-02', 3.43, 1.20);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (102, 'SSA-320', 2., 1.25);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (900, 'UNKN', null, null);

insert into C_SensorData (snsId, snsName, maxG, vPerG) values (900, 'Unknown', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1202, 'Guralp CMG-3 Broad Band Seismometer', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1202, 'Guralp CMG-3T Broad Band Seismometer', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1204, 'Guralp CMG-40T Triaxial Seismometer', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20, 'Kinemetrics FBA ES-T Accel. GFE', 2., 10.);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1250, 'Streckeisen STS-1 VBB Feedback Seismometer', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1250, 'Streckeisen STS-1/E300 VBB Feedback Seismometer', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1250, 'Streckeisen STS-1 VBB Feedback Vertical Seismome', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1250, 'Streckeisen STS-1/E300 VBB Feedback Vertical Sei', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1250, 'Streckeisen STS-1 VBB Feedback Horizontal Seismo', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1250, 'Streckeisen STS-1/E300 VBB Feedback Horizontal S', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1251, 'Streckeisen STS-2 VBB Tri-Axial Seismometer', null, null);

insert into C_SensorData (snsId, snsName, maxG, vPerG) values (200, 'CMG-5TC', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (200, 'Guralp CMG-5TC', 2, 5);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (400, 'GeoSIG-AC63', 3., 3.333280335);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (450, 'Titan', 2., 10.002783);

insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1251, 'STS2', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (900, 'SF3000L', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1, 'SMA', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1100, 'Trillium-compact', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (4, 'Kinemetrics_FBA-11', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (900, 'SL210/220', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1202, 'Guralp-CMG-3ESPC', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1204, 'Guralp-CMG40T-1', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (900, 'Geospace-HS10', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1300, 'L4', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (900, 'SF1500', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20, 'Kinemetrics_Episensor', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (900, 'L22-3D', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (900, 'L22D', null, null);

insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20,'Episensor (4 g max 5.0 v/g)',4,5);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (11,'FBA-11 (3 g max 1.25 v/g)',3,1.25);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (400,'GeoSIG-AC63 (3 g max 3.33 v/g)',3,3.33);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20,'K2 Episensor (4 g max 1.25 v/g)',4,1.25);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20,'Kinemetrics FBA ES-T Accel. (2 g max 10 v/g)',2,10);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20,'Kinemetrics FBA ES-T Accel. GFE (2 g max 10 v/g)',2,10);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20,'Kinemetrics FBA ES-T Accel. GFE (4 g max 10 v/g)',4,10);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20,'Kinemetrics FBA ES-T Accel. GFE (4 g max 5 v/g)',4,5);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (7,'Kinemetrics FBA-23 Accel. (2 g max 1.25 v/g)',2,1.25);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (255,'RT-131A-02 (3.43 g max 1.20 v/g)',3.43,1.2);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (480,'TSA-100S (4 g max 5 v/g)',4,5);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (150,'Wilcoxon 731A Accel. (0.5 g max 10 v/g)',0.5,10);

insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20,'Kinemetrics FBA ES-T Accel. (4 g max 5 v/g)',4,5);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (200, 'Guralp CMG-5T', null, null);

insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20, 'EPISENSOR ES-T', 4, 5);

insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1202, 'CMG-3T-PH', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (202, 'FORTIS-PH', null, null);

insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1002, 'MBB-2', null, null);

insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20, 'EPISENSOR ES-DECK', 4, 5);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20,'Kinemetrics FBA ES-T Accel. USGS (4 g max 5 v/g)',4,5);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (20,'Kinemetrics FBA ES-T Accel. GFE (4 g max 5v/g)',4,5);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1300, 'L-4C VERTICAL', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (450, 'TITAN', 2., 10.002783);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1100, 'TRILLIUM COMPACT PH', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1202, 'Guralp CMG-3 Extended BB H Seismometer', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1202, 'Guralp CMG-3 Extended BB V Seismometer', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (1202, 'Guralp CMG-3TB Broad Band Seismometer', null, null);
insert into C_SensorData (snsId, snsName, maxG, vPerG) values (259, 'RT-147', null, null);


create table C_PhysTyp (
  physId   integer    NOT NULL,
  physDesc VARCHAR(32) NOT NULL,
  constraint c_phystyp_pk primary key (physId)
);
--/
insert into C_PhysTyp (physId, physDesc) values ( 1, 'Acceleration');
insert into C_PhysTyp (physId, physDesc) values ( 2, 'Velocity');
insert into C_PhysTyp (physId, physDesc) values ( 3, 'Displacement (absolute)');
insert into C_PhysTyp (physId, physDesc) values ( 4, 'Displacement (relative)');
insert into C_PhysTyp (physId, physDesc) values (10, 'Angular acceleration');
insert into C_PhysTyp (physId, physDesc) values (11, 'Angular velocity');
insert into C_PhysTyp (physId, physDesc) values (12, 'Angular displacement');
insert into C_PhysTyp (physId, physDesc) values (20, 'Pressure absolute');
insert into C_PhysTyp (physId, physDesc) values (21, 'Pressure relative (gage)');
insert into C_PhysTyp (physId, physDesc) values (30, 'Volumetric strain');
insert into C_PhysTyp (physId, physDesc) values (31, 'Linear strain');
insert into C_PhysTyp (physId, physDesc) values (60, 'Wind speed');
insert into C_PhysTyp (physId, physDesc) values (61, 'Wind direction');


create table C_Units (
  unitsId   integer    NOT NULL,
  unitsDesc VARCHAR(24) NOT NULL,
  constraint c_units_pk primary key (unitsId)
);
--/
insert into C_Units (unitsId, unitsDesc) values ( 1, 'sec');
insert into C_Units (unitsId, unitsDesc) values ( 2, 'g');
insert into C_Units (unitsId, unitsDesc) values ( 3, 'secs&g');
insert into C_Units (unitsId, unitsDesc) values ( 4, 'cm/sec/sec');
insert into C_Units (unitsId, unitsDesc) values ( 5, 'cm/sec');
insert into C_Units (unitsId, unitsDesc) values ( 6, 'cm');
insert into C_Units (unitsId, unitsDesc) values ( 7, 'in/sec/sec');
insert into C_Units (unitsId, unitsDesc) values ( 8, 'in/sec');
insert into C_Units (unitsId, unitsDesc) values ( 9, 'in');
insert into C_Units (unitsId, unitsDesc) values (10, 'gal');
insert into C_Units (unitsId, unitsDesc) values (11, 'mg');
insert into C_Units (unitsId, unitsDesc) values (12, 'g'); 
insert into C_Units (unitsId, unitsDesc) values (23, 'deg/sec/sec');
insert into C_Units (unitsId, unitsDesc) values (24, 'deg/sec');
insert into C_Units (unitsId, unitsDesc) values (25, 'deg');
insert into C_Units (unitsId, unitsDesc) values (50, 'counts');
insert into C_Units (unitsId, unitsDesc) values (51, 'volts');
insert into C_Units (unitsId, unitsDesc) values (52, 'mvolts');
insert into C_Units (unitsId, unitsDesc) values (60, 'psi');
insert into C_Units (unitsId, unitsDesc) values (61, 'kpa');
insert into C_Units (unitsId, unitsDesc) values (70, 'miles/hour');
insert into C_Units (unitsId, unitsDesc) values (80, 'strain'); 

create table C_RecTyp (
  recId   integer    NOT NULL,
  recDesc VARCHAR(24) NOT NULL,
  constraint c_rectyp_pk primary key (recId)
);
--/
insert into C_RecTyp (recId, recDesc) values ( 1, 'Seismic trigger');
insert into C_RecTyp (recId, recDesc) values ( 2, 'Remote trigger');
insert into C_RecTyp (recId, recDesc) values ( 3, 'Preset trigger');
insert into C_RecTyp (recId, recDesc) values ( 4, 'Manual trigger');
insert into C_RecTyp (recId, recDesc) values ( 5, 'Function test');
insert into C_RecTyp (recId, recDesc) values ( 6, 'Active Source Test');
insert into C_RecTyp (recId, recDesc) values (10, 'Sensor calibration');
insert into C_RecTyp (recId, recDesc) values (11, 'Amplifier calibration');
insert into C_RecTyp (recId, recDesc) values (12, 'Recorder calibration');
insert into C_RecTyp (recId, recDesc) values (13, 'Other calibration');


create table C_Net (
  netId   integer   NOT NULL,
  netAbbr VARCHAR(8) NOT NULL,
  net     VARCHAR(2),
  netAgent VARCHAR(128),
  constraint c_net_pk primary key (netId)
);
--/

insert into C_Net (netId, netAbbr, net, netAgent) values (  1, 'C&GS', 'C_', 'U.S. Coast and Geodetic Survey');
insert into C_Net (netId, netAbbr, net, netAgent) values (  2, 'USGS', 'NP', 'U.S. Geological Survey');
insert into C_Net (netId, netAbbr, net, netAgent) values (  3, 'USBR', 'RE', 'U.S. Bureau of Reclamation');
insert into C_Net (netId, netAbbr, net, netAgent) values (  4, 'ACOE', null, 'U.S. Army Corps of Engineers');
insert into C_Net (netId, netAbbr, net, netAgent) values (  5, 'CGS', 'CE', 'Calif. Div. of Mines and Geology');
insert into C_Net (netId, netAbbr, net, netAgent) values (  6, 'SCSN', 'CI', 'Calif. Institute of Technology');
insert into C_Net (netId, netAbbr, net, netAgent) values (  7, 'BDSN', 'BK', 'UC Berkeley');
insert into C_Net (netId, netAbbr, net, netAgent) values (  8, 'NCSN', 'NC', 'USGS - Northern Calif Regional Network');
insert into C_Net (netId, netAbbr, net, netAgent) values (  9, 'UCSB', 'SB', 'UC Santa Barbara');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 10, 'ANZA', 'AZ', 'UC San Diego - ANZA');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 14, 'UNR',  'NN', 'UNR - W. Great Basin/E. Sierra Nevada');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 15, 'PNSN', 'UW', 'UW - Pacific NW Regional Network');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 16, 'CTO',  'TO', 'Caltech - Tectonics Observatory');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 17, 'AEIC', 'AA', 'UA - Anchorage Strong Motion Network ');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 20, 'CDWR', 'WR', 'Calif. Dept. Water Resources');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 21, 'PG&E', 'PG', 'Pacific Gas & Electric');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 30, 'NEIC', 'US', 'USGS - National Seismic Network');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 31, 'USGS', 'GS', 'US Geological Survey Networks');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 32, 'IRGS', 'IU', 'IRIS/USGS Network');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 33, 'CUGS', 'CU', 'Caribbean USGS Network');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 34, 'NQGS', 'NQ', 'NetQuakes');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 35, 'USGS', 'US', 'US National Seismic Network (USNSN)');

insert into C_Net (netId, netAbbr, net, netAgent) values (100, 'CWB',  'TW', 'Taiwan Central Weather Bureau');
insert into C_Net (netId, netAbbr, net, netAgent) values (107, 'BGSN', 'BG', 'UC Berkeley - Geysers Seismic Network');
insert into C_Net (netId, netAbbr, net, netAgent) values (110, 'NIED', 'BO', 'Bosai-Ken (NIED), Japan');
insert into C_Net (netId, netAbbr, net, netAgent) values (199, 'UNK', null, 'Unspecified');

insert into C_Net (netId, netAbbr, net, netAgent) values ( 36,'ASN','AG','Arkansas Seismic Network');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 37,'UAGI','AK','Alaska Regional Network');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 38,'ASO','AO','Arkansas Seismic Observatory, UALR');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 39,'AVO','AV','Alaska Volcano Observatory');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 40,'CERI','ET','Southern Appalachian Seismic Network');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 46,'LCSN','LD','Lamont-Doherty Coop. Seism. Network');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 47,'LLNL','LL','LLNL NTS Network');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 48,'MRSN','MB','Montana Regional Seismic Network');  
insert into C_Net (netId, netAbbr, net, netAgent) values ( 49,'NUSN','NE','New England Seismic Network');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 50,'CNMSN','NM','Coop New Madrid Seismc Network, St Louis Univ');    
insert into C_Net (netId, netAbbr, net, netAgent) values ( 51,'PNSN','UW','Pacific Northwest Regional Sesmic Network');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 52,'UUSS','UU','Univ of Utah Seismograph Stations'); 
insert into C_Net (netId, netAbbr, net, netAgent) values ( 53,'YWSN','WY','Yellowstone Wyoming Seismic Network');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 54,'UPRM','PR','Puerto Rico Strong Motion Program');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 55,'SCSN','CO','South Carolina Seismic Network');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 56,'HVO','HV','Hawaiian Volcano Observatory Network');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 57,'USGS','IC','New China Digital Seismograph Network');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 58,'SIO','II','IRIS/IDA Seismic Network');   
insert into C_Net (netId, netAbbr, net, netAgent) values ( 59,'USGS','IW','Intermountain West Seismic Network');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 60,'UCSD','NA','Central and Eastern US Network');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 61,'IRIS','TA','USArray Transportable Array (EarthScope_TA)');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 62,'SRTC','TR','Eastern Caribbean Seismograph Network');
insert into C_Net (netId, netAbbr, net, netAgent) values ( 63,'CNSN','C','Univ Chile, Dept of Geophysics');
insert into C_Net (netId, netAbbr, net, netAgent) values (111,'UO','UO','Univ. of Oregon Regional Network');


create table C_Datum (
  datumId integer   NOT NULL,
  datum   VARCHAR(8) NOT NULL,
  constraint c_datum_pk primary key (datumId)
);
--/
insert into C_Datum (datumId, datum) values (1, 'WGS84');
insert into C_Datum (datumId, datum) values (2, 'NAD83');
insert into C_Datum (datumId, datum) values (3, 'NAD27');
insert into C_Datum (datumId, datum) values (4, 'WGS72');

      
create table C_Site (
  siteId   integer     NOT NULL, 
  siteDesc VARCHAR(132) NOT NULL,
  constraint c_site_pk primary key (siteId)
);
--/
insert into C_Site (siteId, siteDesc) values ( 1, 'Small fiberglass or other shelter (typ. ~1m^3; e.g., T-hut)');
insert into C_Site (siteId, siteDesc) values ( 2, 'Small, prefabricated metal building (typically 1-2 m x 1-2 m x 2 m high; e.g., Armco)');
insert into C_Site (siteId, siteDesc) values ( 3, 'Sensors buried/set in ground or deployed in shallow ground vault (within ~1m of surface)');
insert into C_Site (siteId, siteDesc) values ( 4, 'Reference station (1-2 story, small (<4000 ft2 or 370m2), light building without basement)');
insert into C_Site (siteId, siteDesc) values ( 5, 'Base of building larger than above');
insert into C_Site (siteId, siteDesc) values ( 6, 'Freefield, Unspecified');
insert into C_Site (siteId, siteDesc) values ( 7, 'Ocean-bottom sensors');
insert into C_Site (siteId, siteDesc) values ( 8, 'Sensors in small near-surface vault (1-2m deep)');
insert into C_Site (siteId, siteDesc) values ( 9, 'Sensors in underground observatory or large vault (~3m2 or larger)');
insert into C_Site (siteId, siteDesc) values (10, 'Building. Sensor in upper levels.');
insert into C_Site (siteId, siteDesc) values (11, 'Bridge');
insert into C_Site (siteId, siteDesc) values (12, 'Dam ');
insert into C_Site (siteId, siteDesc) values (13, 'Wharf');
insert into C_Site (siteId, siteDesc) values (14, 'Tunnel or mine adit (3m or more from surface)');
insert into C_Site (siteId, siteDesc) values (15, 'Other lifeline structure');
insert into C_Site (siteId, siteDesc) values (20, 'Other structure');
insert into C_Site (siteId, siteDesc) values (50, 'Geotechnical array (borehole) sensors, 2m or greater deep');
insert into C_Site (siteId, siteDesc) values (51, 'Other array');
insert into C_Site (siteId, siteDesc) values (52, 'Borehole');
insert into C_Site (siteId, siteDesc) values (999, 'Unspecified');


create table C_Auth (
  authId   integer     NOT NULL, 
  authDesc VARCHAR(32)  NOT NULL,
  constraint c_auth_pk primary key (authId)
);
--/
insert into C_Auth (authId, authDesc) values (  1, 'USGS');
insert into C_Auth (authId, authDesc) values (  2, 'NEIC, Golden, CO');
insert into C_Auth (authId, authDesc) values (  3, 'UC Berkeley, CA');
insert into C_Auth (authId, authDesc) values (  4, 'Caltech, Pasadena, CA');
insert into C_Auth (authId, authDesc) values (  5, 'NCSN, Northern CA');
insert into C_Auth (authId, authDesc) values (  6, 'SCSN, Pasadena, CA');
insert into C_Auth (authId, authDesc) values (  7, 'UCSD, San Diego, CA');
insert into C_Auth (authId, authDesc) values (  8, 'UNR, Reno, NV');
insert into C_Auth (authId, authDesc) values (  9, 'USCGS');
insert into C_Auth (authId, authDesc) values ( 10, 'CISN');
insert into C_Auth (authId, authDesc) values ( 11, 'CGS');
insert into C_Auth (authId, authDesc) values (100, 'CWB, Taiwan');
insert into C_Auth (authId, authDesc) values (110, 'Japan Meteorological Agency');
insert into C_Auth (authId, authDesc) values (111, 'Bosai-Ken (NIED), Japan');
insert into C_Auth (authId, authDesc) values (200, 'Other');


create table C_TimeClk (
  timeId   integer    NOT NULL,
  timeDesc VARCHAR(48) NOT NULL,
  constraint c_timeclk_pk primary key (timeId)
);
--/
insert into C_TimeClk (timeId, timeDesc) values ( 0, 'None');
insert into C_TimeClk (timeId, timeDesc) values ( 1, 'Recorder clock');
insert into C_TimeClk (timeId, timeDesc) values ( 2, 'Auxiliary clock (e.g., TCG)');
insert into C_TimeClk (timeId, timeDesc) values ( 3, 'Radio time signal (e.g., WWVB, WWVH)');
insert into C_TimeClk (timeId, timeDesc) values ( 4, 'Clock that tracks radio signal (WWVB, etc.)');
insert into C_TimeClk (timeId, timeDesc) values ( 5, 'GPS signal');
insert into C_TimeClk (timeId, timeDesc) values ( 6, 'Network time protocol (NTP)');
insert into C_TimeClk (timeId, timeDesc) values (20, 'Other');


create table C_LgrTyp (
  lgrId    integer    NOT NULL,
  lgrTyp   VARCHAR(1)  NOT NULL,
  lgrName  VARCHAR(48) NOT NULL,
  lgrMaker VARCHAR(48),
  constraint c_lgrtyp_pk primary key (lgrId)
);
--/
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('A',   1, 'C&GSStandard', 'USC&GS');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('A',   2, 'AR-240', 'Teledyne');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('A',   3, 'RFT-250', 'Teledyne');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('A',   4, 'RFT-350', 'Teledyne');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('A',   5, 'MO-2', 'New Zealand');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('A',   6, 'RMT-280', null);
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('A',   7, 'SMA-1', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('A',   8, 'SMA-2', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('A',   9, 'SMA-3', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('A',  10, 'CRA-1', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 100, 'DSA-1', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 101, 'DSA-3', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 102, 'PDR-1', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 103, 'PDR-2', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 104, 'SSA-1', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 105, 'SSA-2', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 106, 'SSA-16', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 107, 'SSR-1', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 108, 'K2', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 109, 'Etna', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 110, 'MtWhitney', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 111, 'Everest', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 112, 'Makalu', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 113, 'Etna-A', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 114, 'Etna-2', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 120, 'QDR', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 130, 'Granite', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 131, 'Basalt', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 132, 'Obsidian', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 133, 'Obsidian Multichannel', 'Kinemetrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 200, 'DR-100', 'Sprengnether');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 201, 'DR-200', 'Sprengnether');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 202, 'DR-300', 'Sprengnether');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 203, 'DR-3024', 'Sprengnether');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 204, 'DR-3016', 'Sprengnether');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 250, 'Trident', 'Nanometrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 251, 'Linx', 'Nanometrics');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 300, 'DCA-300', 'Terratech');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 301, 'DCA-310', 'Terratech');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 302, 'DCA-333', 'Terratech');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 310, 'IDS-3602', 'Terratech(IDS)');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 311, 'IDS-3602A', 'Terratech(IDSA)');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 400, 'A700', 'Geotech');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 401, 'A800', 'Geotech');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 402, 'A900', 'Geotech');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 403, 'A900A', 'Geotech');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 500, 'GEOS', null);
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 550, 'GMS-18', 'Geosig (NetQuakes)');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 551, 'GRS-18', 'Geosig');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 600, 'Q4120', 'Quanterra');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 601, 'Q4128a', 'Quanterra');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 602, 'Q730', 'Quanterra');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 603, 'Q736', 'Quanterra');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 604, 'Q980', 'Quanterra');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 605, 'Q330', 'Quanterra');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 606, 'Q680', 'Quanterra');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 650, 'CMG-DM24-EAM', 'Guralp');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 700, '72A', 'RefTek');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 701, '130-01', 'Reftek');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 702, '130-SM', 'Reftek');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 703, '130-SMA', 'Reftek');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 704, '130-ANSS', 'Reftek');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 705, '130-SM5V', 'Reftek');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 706, '130-MC', 'Reftek');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 707, '130-SMHR', 'Reftek');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 710, '148-01', 'Reftek');
insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 1000, 'Other', null);

insert into C_LgrTyp (lgrTyp,lgrId,lgrName,lgrMaker) values ('D', 651, 'MINIMUS+', 'Guralp');


create table C_SnsTyp (
  snsId    integer    NOT NULL,
  snsTyp   VARCHAR(1)  NOT NULL,
  snsName  VARCHAR(48) NOT NULL,
  snsMaker VARCHAR(48),
  constraint c_snstyp_pk primary key (snsId)
);
--/
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A',   1, 'Optical-mechanical (SMA,RFT,etc)', null);
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A',   2, 'FBA-1', 'Kinemetrics');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A',   3, 'FBA-3', 'Kinemetrics');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A',   4, 'FBA-11', 'Kinemetrics');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A',   5, 'FBA-13', 'Kinemetrics');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A',   6, 'FBA-13DH', 'Kinemetrics');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A',   7, 'FBA-23', 'Kinemetrics');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A',   8, 'FBA-23DH', 'Kinemetrics');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A',  20, 'Episensor', 'Kinemetrics');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A',  21, 'EpisensorES-U', 'Kinemetrics');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A',  22, 'Episensor ESDH', 'Kinemetrics');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A',  50, 'FBX-23', 'Sprengnether');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A',  51, 'FBX-26', 'Sprengnether');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 100, 'SSA 120', 'Terratech');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 101, 'SSA 220', 'Terratech');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 102, 'SSA 320', 'Terratech');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 150, '731A', 'Wilcoxon');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 200, 'CMG-5', 'Guralp');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 250, '131A-02/01', 'Reftek');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 251, '131A-02/03', 'Reftek');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 252, '131A-02/BH', 'Reftek');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 253, '131B-01/01', 'Reftek');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 254, '131B-01/03', 'Reftek');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 255, '131A-02/3INT', 'Reftek');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 256, '131-8019', 'Reftek');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 257, '131-8050', 'Reftek');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 258, '147A-01/1', 'Reftek');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 259, '147A-01/3', 'Reftek');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 260, '131-X', 'Unk');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 300, 'ADXL325', 'Analog Devices');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 330, 'SF2005_MEMS', 'Colibrys');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 350, 'LIS344ALH', 'STMicroelectronics');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 380, '7751-500', 'Endevco');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 400, 'AC63', 'Geosig');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 450, 'Titan', 'Nanometrics');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 480, 'TSA-100S', 'Metrozet');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 900, 'Other accelerometer', null);
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('V', 1001, 'SS-1Ranger', 'Kinemetrics');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('V', 1050, 'S-3000', 'Sprengnether');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('V', 1051, 'S-6000', 'Sprengnether');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('V', 1100, 'Trillium 240', 'Nanometrics');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('V', 1201, 'CMG-1', 'Guralp');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('V', 1202, 'CMG-3T', 'Guralp');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('V', 1203, 'CMG-3ESP', 'Guralp');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('V', 1204, 'CMG-40', 'Guralp');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('V', 1250, 'STS-1', 'Strecheisen');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('V', 1251, 'STS-2', 'Strecheisen');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('V', 1300, 'L4', 'MarkProducts');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('V', 1301, 'L22D', 'MarkProducts');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('V', 1900, 'Other seismometer', null);
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 3000, 'Pressure series', null);
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 3001, 'PDCR-35/D', 'Druck');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 3002, 'PDCR-940', 'Druck');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 3003, 'PTX/PCDR-1830', 'GE/Druck');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 3010, '30', 'MEAS/KPSI');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 3011, '330', 'MEAS/KPSI');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 3012, '730', 'MEAS/KPSI');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 3020, '8WD', 'ParoScientific');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 3200, 'Wind Series', null);
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 3201, 'Young - Speed', null);
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 3202, 'Young - Direction', null);
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 3500, 'Dilatometer series', null);
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 4000, 'Relative displacement series', null);
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 4001, 'UltraS', 'Senix Ultrasonic');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 4002, 'TS-30S', 'Senix Ultrasonic');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 4020, 'Extensometer', 'FirstMark');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 4500, 'Rotational series', null);
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 4501, 'EENT', null);
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 4800, 'Tiltmeter Series', null);
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 4801, 'Model 801-S Longitudinal', 'Jewell Inst.');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 4802, 'Model 801-S Transverse', 'Jewell Inst.');
insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('O', 9000, 'Other sensor', null);

insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 202, 'FORTIS-PH', 'Guralp');

insert into C_SnsTyp (snsTyp, snsId, snsName, snsMaker) values ('A', 1002, 'MBB-2', 'Kinemetrics');


create table C_FltrTyp (
  fltrId   integer NOT NULL,
  fltrDesc VARCHAR(48) NOT NULL,
  constraint c_fltrtyp_pk primary key (fltrId)
);
--/
insert into C_FltrTyp (fltrId, fltrDesc) values (0, 'None');
insert into C_FltrTyp (fltrId, fltrDesc) values (1, 'Rectangular');
insert into C_FltrTyp (fltrId, fltrDesc) values (2, 'Cosine bell');
insert into C_FltrTyp (fltrId, fltrDesc) values (3, 'Ormsby');
insert into C_FltrTyp (fltrId, fltrDesc) values (4, 'Butterworth, single direction (causal)');
insert into C_FltrTyp (fltrId, fltrDesc) values (5, 'Butterworth, bi-directional (noncausal)');
insert into C_FltrTyp (fltrId, fltrDesc) values (6, 'Bessel');


create table C_OrntAbbrv (
  orntId   integer NOT NULL,
  orntAbbrv VARCHAR(32) NOT NULL,
  constraint c_orntabbrv_pk primary key (orntId)
);
--/
insert into C_OrntAbbrv (orntId, orntAbbrv) values ( 400, 'Up');
insert into C_OrntAbbrv (orntId, orntAbbrv) values ( 401, 'Down');
insert into C_OrntAbbrv (orntId, orntAbbrv) values ( 402, 'Vert');
insert into C_OrntAbbrv (orntId, orntAbbrv) values ( 500, 'Radl');
insert into C_OrntAbbrv (orntId, orntAbbrv) values ( 501, 'Tran');
insert into C_OrntAbbrv (orntId, orntAbbrv) values ( 600, 'Long');
insert into C_OrntAbbrv (orntId, orntAbbrv) values ( 601, 'Tang');
insert into C_OrntAbbrv (orntId, orntAbbrv) values ( 700, 'H1');
insert into C_OrntAbbrv (orntId, orntAbbrv) values ( 701, 'H2');
insert into C_OrntAbbrv (orntId, orntAbbrv) values ( 702, 'X');
insert into C_OrntAbbrv (orntId, orntAbbrv) values ( 703, 'Y');
insert into C_OrntAbbrv (orntId, orntAbbrv) values ( 704, 'Z');
insert into C_OrntAbbrv (orntId, orntAbbrv) values (2000, 'Othr');


create table C_Ornt (
  orntId   integer NOT NULL,
  orntDesc VARCHAR(128) NOT NULL,
  constraint c_ornt_pk primary key (orntId)
);
--/
insert into C_Ornt (orntId, orntDesc) values ( 400, 'Up');
insert into C_Ornt (orntId, orntDesc) values ( 401, 'Down');
insert into C_Ornt (orntId, orntDesc) values ( 402, 'Vertical');
insert into C_Ornt (orntId, orntDesc) values ( 500, 'Radial inward');
insert into C_Ornt (orntId, orntDesc) values (-500, 'Radial outward');
insert into C_Ornt (orntId, orntDesc) values ( 501, 'Transverse 90 deg CW from radial');
insert into C_Ornt (orntId, orntDesc) values (-501, 'Tranverse 90 deg CCW from radial');
insert into C_Ornt (orntId, orntDesc) values ( 600, 'Longitudinal (relative to structure)');
insert into C_Ornt (orntId, orntDesc) values ( 601, 'Tangential (relative to structure)');
insert into C_Ornt (orntId, orntDesc) values ( 700, 'H1 horizontal azimuth unknown');
insert into C_Ornt (orntId, orntDesc) values ( 701, 'H2 horizontal azimuth unknown');
insert into C_Ornt (orntId, orntDesc) values ( 702, 'X (horiz. sensor, azimuth unknown)');
insert into C_Ornt (orntId, orntDesc) values ( 703, 'Y (horiz. sensor, azimuth unknown)');
insert into C_Ornt (orntId, orntDesc) values ( 704, 'Z (horiz. sensor, polarity not indicated)');
insert into C_Ornt (orntId, orntDesc) values (2000, 'Other');
insert into C_Ornt (orntId, orntDesc) values (1, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (2, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (3, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (4, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (5, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (6, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (7, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (8, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (9, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (10, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (11, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (12, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (13, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (14, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (15, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (16, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (17, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (18, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (19, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (20, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (21, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (22, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (23, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (24, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (25, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (26, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (27, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (28, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (29, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (30, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (31, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (32, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (33, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (34, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (35, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (36, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (37, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (38, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (39, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (40, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (41, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (42, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (43, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (44, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (45, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (46, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (47, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (48, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (49, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (50, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (51, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (52, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (53, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (54, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (55, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (56, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (57, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (58, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (59, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (60, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (61, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (62, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (63, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (64, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (65, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (66, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (67, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (68, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (69, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (70, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (71, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (72, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (73, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (74, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (75, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (76, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (77, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (78, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (79, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (80, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (81, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (82, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (83, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (84, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (85, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (86, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (87, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (88, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (89, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (90, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (91, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (92, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (93, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (94, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (95, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (96, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (97, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (98, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (99, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (100, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (101, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (102, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (103, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (104, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (105, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (106, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (107, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (108, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (109, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (110, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (111, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (112, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (113, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (114, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (115, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (116, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (117, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (118, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (119, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (120, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (121, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (122, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (123, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (124, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (125, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (126, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (127, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (128, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (129, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (130, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (131, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (132, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (133, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (134, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (135, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (136, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (137, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (138, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (139, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (140, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (141, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (142, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (143, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (144, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (145, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (146, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (147, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (148, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (149, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (150, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (151, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (152, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (153, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (154, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (155, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (156, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (157, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (158, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (159, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (160, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (161, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (162, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (163, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (164, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (165, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (166, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (167, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (168, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (169, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (170, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (171, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (172, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (173, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (174, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (175, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (176, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (177, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (178, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (179, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (180, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (181, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (182, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (183, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (184, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (185, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (186, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (187, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (188, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (189, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (190, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (191, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (192, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (193, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (194, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (195, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (196, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (197, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (198, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (199, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (200, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (201, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (202, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (203, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (204, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (205, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (206, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (207, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (208, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (209, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (210, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (211, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (212, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (213, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (214, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (215, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (216, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (217, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (218, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (219, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (220, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (221, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (222, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (223, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (224, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (225, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (226, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (227, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (228, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (229, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (230, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (231, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (232, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (233, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (234, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (235, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (236, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (237, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (238, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (239, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (240, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (241, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (242, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (243, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (244, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (245, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (246, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (247, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (248, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (249, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (250, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (251, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (252, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (253, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (254, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (255, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (256, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (257, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (258, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (259, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (260, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (261, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (262, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (263, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (264, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (265, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (266, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (267, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (268, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (269, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (270, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (271, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (272, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (273, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (274, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (275, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (276, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (277, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (278, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (279, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (280, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (281, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (282, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (283, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (284, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (285, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (286, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (287, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (288, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (289, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (290, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (291, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (292, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (293, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (294, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (295, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (296, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (297, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (298, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (299, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (300, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (301, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (302, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (303, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (304, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (305, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (306, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (307, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (308, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (309, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (310, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (311, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (312, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (313, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (314, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (315, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (316, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (317, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (318, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (319, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (320, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (321, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (322, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (323, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (324, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (325, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (326, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (327, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (328, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (329, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (330, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (331, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (332, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (333, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (334, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (335, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (336, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (337, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (338, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (339, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (340, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (341, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (342, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (343, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (344, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (345, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (346, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (347, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (348, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (349, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (350, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (351, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (352, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (353, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (354, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (355, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (356, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (357, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (358, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (359, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (360, 'Azim degrees CW of N');
insert into C_Ornt (orntId, orntDesc) values (1001, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1002, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1003, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1004, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1005, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1006, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1007, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1008, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1009, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1010, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1011, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1012, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1013, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1014, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1015, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1016, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1017, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1018, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1019, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1020, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1021, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1022, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1023, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1024, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1025, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1026, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1027, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1028, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1029, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1030, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1031, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1032, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1033, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1034, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1035, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1036, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1037, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1038, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1039, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1040, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1041, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1042, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1043, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1044, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1045, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1046, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1047, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1048, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1049, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1050, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1051, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1052, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1053, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1054, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1055, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1056, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1057, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1058, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1059, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1060, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1061, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1062, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1063, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1064, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1065, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1066, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1067, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1068, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1069, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1070, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1071, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1072, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1073, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1074, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1075, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1076, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1077, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1078, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1079, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1080, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1081, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1082, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1083, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1084, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1085, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1086, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1087, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1088, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1089, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1090, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1091, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1092, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1093, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1094, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1095, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1096, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1097, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1098, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1099, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1100, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1101, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1102, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1103, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1104, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1105, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1106, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1107, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1108, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1109, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1110, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1111, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1112, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1113, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1114, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1115, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1116, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1117, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1118, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1119, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1120, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1121, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1122, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1123, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1124, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1125, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1126, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1127, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1128, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1129, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1130, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1131, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1132, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1133, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1134, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1135, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1136, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1137, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1138, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1139, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1140, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1141, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1142, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1143, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1144, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1145, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1146, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1147, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1148, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1149, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1150, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1151, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1152, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1153, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1154, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1155, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1156, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1157, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1158, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1159, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1160, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1161, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1162, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1163, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1164, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1165, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1166, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1167, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1168, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1169, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1170, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1171, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1172, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1173, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1174, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1175, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1176, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1177, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1178, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1179, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1180, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1181, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1182, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1183, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1184, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1185, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1186, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1187, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1188, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1189, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1190, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1191, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1192, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1193, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1194, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1195, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1196, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1197, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1198, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1199, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1200, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1201, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1202, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1203, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1204, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1205, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1206, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1207, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1208, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1209, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1210, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1211, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1212, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1213, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1214, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1215, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1216, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1217, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1218, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1219, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1220, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1221, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1222, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1223, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1224, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1225, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1226, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1227, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1228, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1229, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1230, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1231, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1232, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1233, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1234, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1235, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1236, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1237, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1238, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1239, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1240, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1241, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1242, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1243, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1244, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1245, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1246, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1247, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1248, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1249, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1250, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1251, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1252, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1253, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1254, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1255, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1256, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1257, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1258, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1259, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1260, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1261, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1262, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1263, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1264, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1265, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1266, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1267, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1268, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1269, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1270, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1271, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1272, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1273, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1274, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1275, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1276, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1277, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1278, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1279, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1280, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1281, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1282, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1283, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1284, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1285, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1286, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1287, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1288, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1289, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1290, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1291, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1292, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1293, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1294, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1295, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1296, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1297, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1298, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1299, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1300, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1301, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1302, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1303, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1304, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1305, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1306, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1307, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1308, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1309, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1310, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1311, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1312, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1313, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1314, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1315, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1316, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1317, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1318, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1319, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1320, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1321, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1322, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1323, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1324, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1325, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1326, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1327, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1328, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1329, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1330, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1331, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1332, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1333, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1334, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1335, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1336, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1337, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1338, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1339, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1340, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1341, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1342, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1343, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1344, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1345, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1346, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1347, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1348, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1349, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1350, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1351, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1352, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1353, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1354, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1355, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1356, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1357, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1358, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1359, '1000+azim degrees CW of chn 1');
insert into C_Ornt (orntId, orntDesc) values (1360, '1000+azim degrees CW of chn 1');


GRANT ALL on C_ChannelData to rtem;
GRANT ALL on C_LoggerData to rtem;
GRANT ALL on C_SensorData to rtem;
GRANT ALL on C_PhysTyp to rtem;
GRANT ALL on C_Units to rtem;
GRANT ALL on C_RecTyp to rtem;
GRANT ALL on C_Net to rtem;
GRANT ALL on C_Datum to rtem;
GRANT ALL on C_Site to rtem;
GRANT ALL on C_Auth to rtem;
GRANT ALL on C_TimeClk to rtem;
GRANT ALL on C_LgrTyp to rtem;
GRANT ALL on C_SnsTyp to rtem;
GRANT ALL on C_FltrTyp to rtem;
GRANT ALL on C_OrntAbbrv to rtem;
GRANT ALL on C_Ornt to rtem;
