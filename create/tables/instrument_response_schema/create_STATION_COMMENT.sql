-- 
-- TABLE: Station_Comment 
--

CREATE TABLE Station_Comment(
    net              VARCHAR(8)     NOT NULL,
    sta              VARCHAR(6)     NOT NULL,
    ondate           TIMESTAMP            NOT NULL,
    comment_id       INTEGER    NOT NULL,
    offdate          TIMESTAMP,
    comment_level    INTEGER,
    LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT StC00 PRIMARY KEY (net, sta, ondate, comment_id)
);
