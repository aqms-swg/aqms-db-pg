-- 
-- TABLE: D_Abbreviation 
--

CREATE TABLE D_Abbreviation(
    id             INTEGER    NOT NULL,
    description    VARCHAR(70),
    CONSTRAINT D_A00 PRIMARY KEY (id)
);
