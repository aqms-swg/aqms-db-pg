--
-- TABLE: Station_Datalogger_PChannel
--

CREATE TABLE Station_Datalogger_PChannel(
    sta             VARCHAR(6)     NOT NULL,
    net             VARCHAR(8)     NOT NULL,
    data_nb         INTEGER    NOT NULL,
    pchannel_nb     INTEGER    NOT NULL,
    ondate          TIMESTAMP            NOT NULL,
    board_type      VARCHAR(1)     NOT NULL,
    channel_type    VARCHAR(1)     NOT NULL,
    seed_io         VARCHAR(2)     NOT NULL,
    nb_lchannel     INTEGER    NOT NULL,
    offdate         TIMESTAMP,
    lddate          TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT StDaP00 PRIMARY KEY (sta, net, data_nb, pchannel_nb, ondate)
)
;

