--
-- TABLE: Datalogger
--

CREATE TABLE Datalogger(
    data_id        INTEGER    NOT NULL,
    data_type      VARCHAR(80),
    serial_nb      VARCHAR(80),
    firmware_nb    VARCHAR(80),
    software       VARCHAR(80),
    software_nb    VARCHAR(80),
    ondate         TIMESTAMP            NOT NULL,
    offdate        TIMESTAMP,
    nb_board       INTEGER,
    word_32        INTEGER    NOT NULL,
    word_16        INTEGER    NOT NULL,
    lddate         DATE	DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT Da00 PRIMARY KEY (data_id)
)
;

