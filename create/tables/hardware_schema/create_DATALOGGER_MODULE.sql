--
-- TABLE: Datalogger_Module
--

CREATE TABLE Datalogger_Module(
    data_id        INTEGER        NOT NULL,
    board_nb       INTEGER        NOT NULL,
    module_nb      INTEGER        NOT NULL,
    serial_nb      VARCHAR(80),
    firmware_nb    VARCHAR(80),
    sensitivity    DOUBLE PRECISION,
    lddate         TIMESTAMP                DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT DaM00 PRIMARY KEY (data_id, board_nb, module_nb)
)
;

