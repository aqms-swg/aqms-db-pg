--
-- TABLE: Response_PN
--

CREATE TABLE Response_PN(
    pn_id          INTEGER        NOT NULL,
    name           VARCHAR(80),
    poly_type      VARCHAR(1)         NOT NULL,
    lower_bound    DOUBLE PRECISION,
    upper_bound    DOUBLE PRECISION,
    max_error      DOUBLE PRECISION,
    nb_coeff       INTEGER,
    lddate         TIMESTAMP                DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT RPN00 PRIMARY KEY (pn_id)
)
;

