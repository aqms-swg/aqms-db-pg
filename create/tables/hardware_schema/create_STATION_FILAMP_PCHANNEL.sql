--
-- TABLE: Station_Filamp_PChannel
--

CREATE TABLE Station_Filamp_PChannel(
    sta                   VARCHAR(6)     NOT NULL,
    net                   VARCHAR(8)     NOT NULL,
    filamp_nb             INTEGER    NOT NULL,
    pchannel_nb           INTEGER    NOT NULL,
    ondate                TIMESTAMP            NOT NULL,
    next_hard_type        VARCHAR(1)     NOT NULL,
    next_hard_nb          INTEGER    NOT NULL,
    next_hard_pchannel    INTEGER    NOT NULL,
    offdate               TIMESTAMP,
    lddate                TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT StFiP00 PRIMARY KEY (sta, net, filamp_nb, pchannel_nb, ondate)
)
;

