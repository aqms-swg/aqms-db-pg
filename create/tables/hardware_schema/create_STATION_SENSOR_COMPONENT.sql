--
-- TABLE: Station_Sensor_Component
--

CREATE TABLE Station_Sensor_Component(
    sta                   VARCHAR(6)         NOT NULL,
    net                   VARCHAR(8)         NOT NULL,
    sensor_nb             INTEGER        NOT NULL,
    component_nb          INTEGER        NOT NULL,
    ondate                TIMESTAMP                NOT NULL,
    next_hard_type        VARCHAR(1)         NOT NULL,
    next_hard_nb          INTEGER        NOT NULL,
    next_hard_pchannel    INTEGER        NOT NULL,
    azimuth               DOUBLE PRECISION,
    dip                   DOUBLE PRECISION,
    offdate               TIMESTAMP,
    lddate                TIMESTAMP                DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT StSeC00 PRIMARY KEY (sta, net, sensor_nb, component_nb, ondate)
)
;

