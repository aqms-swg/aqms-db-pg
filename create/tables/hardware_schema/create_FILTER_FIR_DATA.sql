--
-- TABLE: Filter_FIR_Data
--

CREATE TABLE Filter_FIR_Data(
    fir_id         INTEGER        NOT NULL,
    coeff_nb       INTEGER        NOT NULL,
    type           VARCHAR(1)         NOT NULL,
    coefficient    DOUBLE PRECISION    NOT NULL,
    error          DOUBLE PRECISION,
    CONSTRAINT FFD00 PRIMARY KEY (fir_id, coeff_nb)
)
;

