--- This script creates tables used in AQMS hardware tracking part of schema.
--- Run as TRINETDB user.
---
\i create_DATALOGGER.sql
\i create_DATALOGGER_BOARD.sql
\i create_DATALOGGER_MODULE.sql
\i create_FILAMP.sql
\i create_FILEAMP_PCHANNEL.sql
\i create_FILTER.sql
\i create_FILTER_FIR.sql
\i create_FILTER_FIR_DATA.sql
\i create_FILTER_SEQUENCE.sql
\i create_FILTER_SEQUENCE_DATA.sql
\i create_RESPONSE.sql
\i create_RESPONSE_HP.sql
\i create_RESPONSE_LP.sql
\i create_RESPONSE_PN.sql
\i create_RESPONSE_PN_DATA.sql
\i create_RESPONSE_PZ.sql
\i create_SENSOR.sql
\i create_SENSOR_COMPONENT.sql
\i create_STATION.sql
\i create_STATION_DATALOGGER.sql
\i create_STATION_DATALOGGER_LCHANNEL.sql
\i create_STATION_DATALOGGER_PCHANNEL.sql
\i create_STATION_DIGITIZER.sql
\i create_STATION_DIGITIZER_PCHANNEL.sql
\i create_STATION_FILAMP.sql
\i create_STATION_FILAMP_PCHANNEL.sql
\i create_STATION_SENSOR.sql
\i create_STATION_SENSOR_COMPONENT.sql
