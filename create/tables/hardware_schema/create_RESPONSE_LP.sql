--
-- TABLE: Response_LP
--

CREATE TABLE Response_LP(
    lp_id            INTEGER        NOT NULL,
    filter_type      VARCHAR(2),
    nb_pole          INTEGER,
    corner_freq      DOUBLE PRECISION    NOT NULL,
    damping_value    DOUBLE PRECISION    NOT NULL,
    lddate           TIMESTAMP                DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT RLP00 PRIMARY KEY (lp_id)
)
;

