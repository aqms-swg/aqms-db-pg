--
-- TABLE: Datalogger_Board
--

CREATE TABLE Datalogger_Board(
    data_id        INTEGER    NOT NULL,
    board_nb       INTEGER    NOT NULL,
    serial_nb      VARCHAR(80),
    nb_module      INTEGER    NOT NULL,
    firmware_nb    VARCHAR(80),
    lddate         TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'
),
    CONSTRAINT DaB00 PRIMARY KEY (data_id, board_nb)
)
;

