--
-- TABLE: Station_Datalogger_LChannel
--

CREATE TABLE Station_Datalogger_LChannel(
    sta            VARCHAR(6)         NOT NULL,
    net            VARCHAR(8)         NOT NULL,
    data_nb        INTEGER        NOT NULL,
    pchannel_nb    INTEGER        NOT NULL,
    lchannel_nb    INTEGER        NOT NULL,
    ondate         TIMESTAMP                NOT NULL,
    seqfil_id      INTEGER,
    seedchan       VARCHAR(3),
    channel        VARCHAR(8),
    channelsrc     VARCHAR(8),
    location       VARCHAR(2),
    rgain          DOUBLE PRECISION,
    rfrequency     DOUBLE PRECISION,
    samprate       DOUBLE PRECISION    NOT NULL,
    clock_drift    DOUBLE PRECISION,
    flags          VARCHAR(27),
    data_format    VARCHAR(80)        NOT NULL,
    comp_type      INTEGER        NOT NULL,
    unit_signal    INTEGER        NOT NULL,
    unit_calib     INTEGER        NOT NULL,
    block_size     INTEGER        NOT NULL,
    offdate        TIMESTAMP,
    remark         VARCHAR(30),
    lddate         TIMESTAMP                DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT StDaL00 PRIMARY KEY (sta, net, data_nb, pchannel_nb, lchannel_nb, ondate)
)
;

