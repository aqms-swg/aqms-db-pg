--
-- TABLE: Filamp_PChannel
--

CREATE TABLE Filamp_PChannel(
    filamp_id      INTEGER        NOT NULL,
    pchannel_nb    INTEGER        NOT NULL,
    gain           DOUBLE PRECISION,
    frequency      DOUBLE PRECISION,
    seqresp_id     INTEGER,
    lddate         TIMESTAMP                DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT FiP00 PRIMARY KEY (filamp_id, pchannel_nb)
)
;

