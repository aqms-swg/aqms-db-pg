--
-- TABLE: Station_Datalogger
--

CREATE TABLE Station_Datalogger(
    sta            VARCHAR(6)     NOT NULL,
    net            VARCHAR(8)     NOT NULL,
    data_nb        INTEGER    NOT NULL,
    ondate         TIMESTAMP            NOT NULL,
    data_id        INTEGER    NOT NULL,
    nb_pchannel    INTEGER    NOT NULL,
    offdate        TIMESTAMP,
    lddate         TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT StDa00 PRIMARY KEY (sta, net, data_nb, ondate)
)
;

