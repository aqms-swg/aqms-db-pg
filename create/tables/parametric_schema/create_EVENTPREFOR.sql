 CREATE TABLE EVENTPREFOR
 (	EVID BIGINT NOT NULL ,
	TYPE VARCHAR(2) NOT NULL ,
	ORID BIGINT NOT NULL ,
	LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
	 CONSTRAINT EVENTPREFOR_CHKTYP CHECK (type in ('H','h','C','c','A','a','D','d','U','u')) ,
	 CONSTRAINT EVENTPREFOR_PK PRIMARY KEY (EVID, TYPE)
 );

