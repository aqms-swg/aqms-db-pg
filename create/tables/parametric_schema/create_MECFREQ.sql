
CREATE TABLE MecFreq (
   mecfreqid      BIGINT      NOT NULL,
   mecalgo        VARCHAR(15),
   lddate         TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
   CONSTRAINT MecFreq_PK PRIMARY KEY (mecfreqid)
);

