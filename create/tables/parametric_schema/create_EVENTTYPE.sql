create table EventType
(
ETYPE VARCHAR(2),
NAME VARCHAR(16) not null,
DESCRIPTION VARCHAR(80),
CONSTRAINT EVENTTYPEKEY01 primary key (ETYPE),
CONSTRAINT EVENTTYPE01 UNIQUE (NAME),
CONSTRAINT EVENTTYPE02 CHECK
(etype in
('eq','lp','se','to','tr','vt','nt','qb','ce','ex','sh','sn','th',
'av','co','df','ls','rb','rs','ve','bc','mi','pc','ot','st','uk','px','su','lf', 'ne', 'nr', 'ae'))
);

------POPULATE VALUES -----
insert into eventtype(etype,name,description)
values ('eq', 'earthquake','earthquake')
;
insert into eventtype(etype,name,description)
values ('se', 'slow','slow earthquake') 
;
insert into eventtype(etype,name,description)
values ('lp', 'longperiod','long period volcanic earthquake') 
;
insert into eventtype(etype,name,description)
values ('to', 'tornillo','tornillo wavelet') 
;
insert into eventtype(etype,name,description)
values ('tr', 'nv-tremor','non-volcanic tremor') 
;
insert into eventtype(etype,name,description)
values ('vt', 'v-tremor','volcanic tremor') 
;
insert into eventtype(etype,name,description)
values ('nt', 'nuclear','nuclear test') 
;
insert into eventtype(etype,name,description)
values ('qb', 'quarry','quarry blast') 
;
insert into eventtype(etype,name,description)
values ('ce', 'calibr','calibration') 
;
insert into eventtype(etype,name,description)
values ('ex', 'explosion','generic chemical blast') 
;
insert into eventtype(etype,name,description)
values ('sh', 'shot','refraction/reflection survey shot') 
;
insert into eventtype(etype,name,description)
values ('sn', 'sonic','sonic shockwave') 
;
insert into eventtype(etype,name,description)
values ('th', 'thunder','thunder') 
;
insert into eventtype(etype,name,description)
values ('ve', 'eruption','volcanic eruption') 
;
insert into eventtype(etype,name,description)
values ('co', 'm-collapse','mine/tunnel collapse') 
;
insert into eventtype(etype,name,description)
values ('df', 'debris','debris flow/avalanche') 
;
insert into eventtype(etype,name,description)
values ('av', 'snow-ice','snow/ice avalanche') 
;
insert into eventtype(etype,name,description)
values ('ls', 'landslide','landslide') 
;
insert into eventtype(etype,name,description)
values ('rb', 'rockburst','rockburst') 
;
insert into eventtype(etype,name,description)
values ('rs', 'rockslide','rockslide') 
;
insert into eventtype(etype,name,description)
values ('bc', 'bldg','building collapse/demolition') 
;
insert into eventtype(etype,name,description)
values ('pc', 'plane','plane crash') 
;
insert into eventtype(etype,name,description)
values ('mi', 'meteor','meteor/comet impact') 
;
insert into eventtype(etype,name,description)
values ('st', 'trigger','subnet trigger') 
;
insert into eventtype(etype,name,description)
values ('uk', 'unknown','unknown type') 
;
insert into eventtype(etype,name,description)
values ('ot', 'other','other miscellaneous') 
;
insert into eventtype(etype,name,description)
values ('su', 'surface-event','unspecified surface event such as rock fall, landslide, volcanic explosion')
;
insert into eventtype(etype,name,description)
values ('lf', 'low-frequency','low frequency signal, may or may not have long period source')
;
insert into eventtype(etype,name,description)
values ('px', 'probable-blast','unconfirmed blast or explosion')
;
insert into eventtype(etype,name,description)
values ('ne', 'not existing','unknown type')
;
insert into eventtype(etype,name,description)
values ('nr', 'not reported','unknown type')
;
insert into eventtype(etype,name,description)
values ('ae', 'anthropogenic','unknown type')
;
