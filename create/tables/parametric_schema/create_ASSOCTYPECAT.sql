create table AssocTypeCat
(
CatID SMALLINT,
Etype VARCHAR(2),
constraint AssocTypeCatKey01 primary key (catid, etype)
);

----POPULATE VALUES------------

---earthquake---
insert into assoctypecat
(catid, etype)
select catid, 'eq' from EventCategory where category = 'earthquake'
;
insert into assoctypecat
(catid, etype)
select catid, 'se' from EventCategory where category = 'earthquake'
;
insert into assoctypecat
(catid, etype)
select catid, 'lp' from EventCategory where category = 'earthquake'
;
insert into assoctypecat
(catid, etype)
select catid, 'to' from EventCategory where category = 'earthquake'
;

--- tremor---
----
insert into assoctypecat
(catid, etype)
select catid, 'tr' from EventCategory where category = 'tremor'
;
insert into assoctypecat
(catid, etype)
select catid, 'vt' from EventCategory where category = 'tremor'
;
---explosion---
---
insert into assoctypecat
(catid, etype)
select catid, 'nt' from EventCategory where category = 'explosion'
;
insert into assoctypecat
(catid, etype)
select catid, 'qb' from EventCategory where category = 'explosion'
;
insert into assoctypecat
(catid, etype)
select catid, 'ce' from EventCategory where category = 'explosion'
;
insert into assoctypecat
(catid, etype)
select catid, 'ex' from EventCategory where category = 'explosion'
;
insert into assoctypecat
(catid, etype)
select catid, 'sh' from EventCategory where category = 'explosion'
;
---atmospheric----
---
insert into assoctypecat
(catid, etype)
select catid, 'sn' from EventCategory where category = 'atmospheric'
;
insert into assoctypecat
(catid, etype)
select catid, 'th' from EventCategory where category = 'atmospheric'
;
---earth movement----
---
insert into assoctypecat
(catid, etype)
select catid, 've' from EventCategory where category = 'earth movement'
;
insert into assoctypecat
(catid, etype)
select catid, 'co' from EventCategory where category = 'earth movement'
;
insert into assoctypecat
(catid, etype)
select catid, 'df' from EventCategory where category = 'earth movement'
;
insert into assoctypecat
(catid, etype)
select catid, 'av' from EventCategory where category = 'earth movement'
;
insert into assoctypecat
(catid, etype)
select catid, 'ls' from EventCategory where category = 'earth movement'
;
insert into assoctypecat
(catid, etype)
select catid, 'rb' from EventCategory where category = 'earth movement'
;
insert into assoctypecat
(catid, etype)
select catid, 'rs' from EventCategory where category = 'earth movement'
;
---impact----
-----
insert into assoctypecat
(catid, etype)
select catid, 'bc' from EventCategory where category = 'impact'
;
insert into assoctypecat
(catid, etype)
select catid, 'pc' from EventCategory where category = 'impact'
;
insert into assoctypecat
(catid, etype)
select catid, 'mi' from EventCategory where category = 'impact'
;
---unknown-----
-----
insert into assoctypecat
(catid, etype)
select catid, 'st' from EventCategory where category = 'unknown'
;
insert into assoctypecat
(catid, etype)
select catid, 'uk' from EventCategory where category = 'unknown'
;
insert into assoctypecat
(catid, etype)
select catid, 'ot' from EventCategory where category = 'unknown'
;
--- new values ---
---
insert into assoctypecat
(catid, etype)
select catid, 'lf' from EventCategory where category = 'earthquake';
;
insert into assoctypecat
(catid, etype)
select catid, 'su' from EventCategory where category = 'earth movement';
;
insert into assoctypecat
(catid, etype)
select catid, 'px' from EventCategory where category = 'explosion';
;

