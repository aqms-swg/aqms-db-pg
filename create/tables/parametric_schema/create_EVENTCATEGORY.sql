create table EventCategory
(CatId SMALLINT,
Category VARCHAR(25) not null,
Description VARCHAR(80),
constraint EvCatKey01 PRIMARY KEY (CatID),
constraint EvCat01 UNIQUE (Category)
);

---- POPULATE VALUES---
insert into eventcategory (catid, category)
values (nextval('catseq'), 'earthquake');
insert into eventcategory (catid, category)
values (nextval('catseq'), 'tremor');
insert into eventcategory (catid, category)
values (nextval('catseq'), 'explosion');
insert into eventcategory (catid, category)
values (nextval('catseq'), 'atmospheric');
insert into eventcategory (catid, category)
values (nextval('catseq'), 'earth movement');
insert into eventcategory (catid, category)
values (nextval('catseq'), 'impact');
insert into eventcategory (catid, category)
values (nextval('catseq'), 'unknown');
