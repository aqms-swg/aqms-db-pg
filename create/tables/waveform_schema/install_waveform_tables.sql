--- This script creates tables in AQMS Waveform part of Schema.
--- Run as TRINETDB user
---
\i create_ASSOCWAE.sql
\i create_FILENAME.sql
\i create_PATHNAME.sql
\i create_REQUEST_CARD.sql
\i create_SUBDIR.sql
\i create_WAVEFORM.sql
