--
-- TABLE: credit_alias
--

CREATE TABLE credit_alias(
    alias    VARCHAR(32)    NOT NULL,
    keyid    VARCHAR(32)    NOT NULL,
    CONSTRAINT pk_alias PRIMARY KEY (alias, keyid)
)
;

