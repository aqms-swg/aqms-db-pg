--
-- TABLE: gazetteerline
--

CREATE TABLE gazetteerline(
    gazid     BIGINT          NOT NULL,
    type      SMALLINT    NOT NULL,
    name      VARCHAR(80)    NOT NULL,
    line      BIGINT          NOT NULL,
    format    VARCHAR(16),
    points    BYTEA,
    lddate    TIMESTAMP             DEFAULT (CURRENT_TIMESTAMP),
    CONSTRAINT gazline_pk_gazid PRIMARY KEY (gazid)
);
