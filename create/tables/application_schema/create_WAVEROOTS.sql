create table WAVEROOTS
(ARCHIVE VARCHAR(8) not null,
WAVETYPE VARCHAR(2) not null,
STATUS VARCHAR(2) not null,
NET VARCHAR(8) not null,
WAVE_FMT SMALLINT not null,
DATETIME_ON DOUBLE PRECISION not null,
DATETIME_OFF DOUBLE PRECISION not null,
WCOPY INTEGER not null,
FILEROOT VARCHAR(255) not null
);

alter table WAVEROOTS add constraint wfrootkey01 primary key
(archive, wavetype, status, net, wave_fmt, datetime_on, datetime_off,
wcopy);

alter table WAVEROOTS add constraint wfroot01 check (wcopy > 0);
alter table WAVEROOTS add constraint wfroot02 check (wavetype in ('T','C'));
alter table WAVEROOTS add constraint wfroot03 check (status in ('T','A'));
alter table WAVEROOTS add constraint wfroot04 check (wave_fmt in (1,2,3,4));

comment on table waveroots is 'This table contains the possible locations of 
waveforms based common columns it shares with the WAVEFORM table.';

comment on column WAVEROOTS.ARCHIVE is 'This field specifies the place where 
the seismogram is archived - such as NCEDC/SCEDC.';

comment on column WAVEROOTS.WAVETYPE is 'This attribute denotes the type of
waveform (T)riggered or (C)ontinuous.';

comment on column WAVEROOTS.STATUS is 'This attribute denotes the status of the
waveform in the file:  (A)rchived or (T)emporary.';

comment on column WAVEROOTS.NET is 'Unique network identifier. This character 
string is the name of a seismic network.  A null value means waveforms from 
any network could reside in this location.';

comment on column WAVEROOTS.WAVE_FMT is 'Waveform type - (numeric code for 
format -mSEED, SAC, SEGY etc).';

comment on column WAVEROOTS.FILEROOT is 'The top file directory of the waveform
file.  Subdirectories are allowed (i.e.
;top_directory/subdirectory1/subdirectory2) but the top level must be
included and it must be consistent with entries in SUBDIR.';

comment on column WAVEROOTS.DATETIME_ON is 'The datetime_on of the waveform row
must be >= to this value.';

comment on column WAVEROOTS.DATETIME_OFF is 'The datetime_off of the waveform
row must be <  to this value.';

comment on column WAVEROOTS.WCOPY is 'Copy DOUBLE PRECISION of the waveform file location.
Used to distinguish between locations that contain a copy of the waveform file.
The copy DOUBLE PRECISION should start from 1.  The copy number does not indicate
quality or any preference of one copy over another; it is used to just
distinguish one copy from another.';

-- create or replace public synonym waveroots for waveroots;

-- grant select on waveroots to code, trinetdb_read;
-- grant insert, update, delete on waveroots to code, trinetdb_write;
