--
-- TABLE: program
--

CREATE TABLE program(
    progid    INTEGER    NOT NULL,
    name      VARCHAR(16)    NOT NULL,
    lddate    TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT progkey01 PRIMARY KEY (progid)
)
;

