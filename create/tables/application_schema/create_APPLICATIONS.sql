--
-- TABLE: applications
--

CREATE TABLE applications(
    progid    INTEGER    NOT NULL,
    name      VARCHAR(16)    NOT NULL,
    lddate    TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT progkey01_1 PRIMARY KEY (progid)
)
;
