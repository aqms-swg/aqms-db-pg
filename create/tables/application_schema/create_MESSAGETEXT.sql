Create Table MessageText (
     SrcNet       VARCHAR(2)    NOT NULL,
     TextType     VARCHAR(40)   NOT NULL,
     LineNo       INTEGER      NOT NULL Check (LineNo > 0),
     Text         VARCHAR(132)  NOT NULL,
     Primary Key (SrcNet, TextType, LineNo)
);
-- create or replace public synonym MessageText for MessageText;
-- grant Select ON MessageText TO TRINETDB_READ,CODE;
-- grant insert,update,delete ON MessageText TO TRINETDB_WRITE,CODE;

insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'CI', 'PrelimHeader', 1, '                   == PRELIMINARY EVENT REPORT ==')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'CI', 'SrcHeader', 1, 'Southern California Seismic Network (SCSN) operated by Caltech and USGS')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'CI', 'ReviewedHeader', 1, 'This event has been reviewed by a seismologist.')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'CI', 'UnreviewedHeader', 1, 'This is a computer generated solution and has not yet been reviewed by a seismologist.')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'CI', 'SrcFooter', 1, 'More Information about this event and other earthquakes is available at:')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'CI', 'SrcFooter', 2, 'http://www.scsn.org/earthquakes.html')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'CI', 'QuarryHeader', 1, 'This event is a quarry explosion.')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'CI', 'UpdateHeader', 1, '             >>> UPDATE OF PREVIOUSLY REPORTED EVENT <<<')
;
-- this was a violation of the key when the UW one was hit....
-- not sure why we are even populating CI/NC/UW here...isn't that RSN specific?
--
-- insert into MessageText (SrcNet, TextType, LineNo, Text) values (
-- '??', 'LocalNet', 1,'CI') 
-- ;


insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'NC', 'UpdateHeader', 1, '             +++ UPDATE OF PREVIOUSLY REPORTED EVENT +++')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'NC', 'PrelimHeader', 1, '== PRELIMINARY EVENT REPORT ==')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'NC', 'SrcHeader', 1, 'Northern California Seismic System (NCSS) operated by UC Berkeley and USGS')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'NC', 'ReviewedHeader', 1, 'This event has been reviewed by a seismologist.')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'NC', 'UnreviewedHeader', 1, 'This is a computer generated solution and has not yet been reviewed by a seismologist.')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'NC', 'SrcFooter', 1, 'More Information about this event and other earthquakes is available at:')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'NC', 'SrcFooter', 2, 'http://earthquake.usgs.gov')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'NC', 'QuarryHeader', 1, 'This event is a quarry explosion.')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'UW', 'PrelimHeader', 1, '                   == PRELIMINARY EVENT REPORT ==')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'UW', 'SrcHeader', 1, 'Pacific Northwest Seismic Network (PNSN) operated by University of Washington and USGS')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'UW', 'ReviewedHeader', 1, 'This event has been reviewed by a seismologist.')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'UW', 'UnreviewedHeader', 1, 'This is a computer generated solution and has not yet been reviewed by a seismologist.')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'UW', 'SrcFooter', 1, 'More Information about this event and other earthquakes is available at:')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'UW', 'SrcFooter', 2, 'https://www.pnsn.org')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'UW', 'QuarryHeader', 1, 'This event is a quarry explosion.')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'UW', 'UpdateHeader', 1, '             >>> UPDATE OF PREVIOUSLY REPORTED EVENT <<<')
;
insert into MessageText (SrcNet, TextType, LineNo, Text) values (
'??', 'LocalNet', 1,'UW') 
;
