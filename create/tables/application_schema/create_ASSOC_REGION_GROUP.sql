-- TABLE assoc_region_group
-- grpid: is the tag id of a group found in gazetteer_region_group table
-- name: is the name of gazetteer_region polygon contained in this grpid group
-- auth: network authority assigned to this group association
-- rg_order: order in which to test whether a lat,lon point is inside the region's polygon
create table assoc_region_group (
   grpid VARCHAR(4)  not null,
   name  VARCHAR(24) not null,
   auth  VARCHAR(15) not null,
   rg_order SMALLINT not null,
   constraint pk_assoc_reggrp primary key (grpid, name),
   constraint fk_reg_name foreign key (name) references gazetteer_region (name),
   constraint fk_reg_grpid foreign key (grpid) references gazetteer_region_group
 (grpid),
   constraint chk_rank_assoc_reggrp check (rg_order > 0)
)
;
-- Create public synonyms to make tables accessible to users/roles
-- create or replace public synonym assoc_region_group for assoc_region_group;

-- Substitute in grant statements the appropriate role/user privileges
-- grant select on assoc_region_group to trinetdb_read;
-- grant select,insert,update,delete on assoc_region_group to trinetdb_write;
-- grant select,insert,update,delete on assoc_region_group to code;
-- For AUTH group substitute in insert gazetteer_region polygon name, auth code, and polygon search order values:
-- insert into assoc_region_group values ('AUTH',name,auth,rg_order);
-- Example:
-- /*
-- insert into assoc_region_group values ('AUTH','CI','CI',1);
-- insert into assoc_region_group values ('AUTH','NC','CI',2);
-- insert into assoc_region_group values ('AUTH','NN','CI',3);
-- */

;
