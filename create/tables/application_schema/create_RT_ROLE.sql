--
-- TABLE: rt_role
--

CREATE TABLE rt_role(
    primary_system       VARCHAR(5),
    modification_time    TIMESTAMP           DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')
)
;
