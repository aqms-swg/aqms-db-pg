--
-- TABLE: unassocamp
--

CREATE TABLE unassocamp(
    ampid         BIGINT       NOT NULL,
    commid        BIGINT,
    datetime      DOUBLE PRECISION      NOT NULL,
    sta           VARCHAR(6)         NOT NULL,
    net           VARCHAR(8),
    auth          VARCHAR(15)        NOT NULL,
    subsource     VARCHAR(8),
    channel       VARCHAR(8),
    channelsrc    VARCHAR(8),
    seedchan      VARCHAR(3),
    location      VARCHAR(2),
    iphase        VARCHAR(8),
    amplitude     DOUBLE PRECISION    NOT NULL,
    amptype       VARCHAR(8),
    units         VARCHAR(4)         NOT NULL,
    ampmeas       VARCHAR(1),
    eramp         DOUBLE PRECISION,
    flagamp       VARCHAR(4),
    per           DOUBLE PRECISION,
    snr           DOUBLE PRECISION,
    tau           DOUBLE PRECISION,
    quality       DOUBLE PRECISION,
    rflag         VARCHAR(2),
    cflag         VARCHAR(2),
    wstart        DOUBLE PRECISION    NOT NULL,
    duration      DOUBLE PRECISION    NOT NULL,
    lddate        TIMESTAMP                DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    fileid        BIGINT,
    CONSTRAINT unassocampkey01 PRIMARY KEY (ampid)
)
;

