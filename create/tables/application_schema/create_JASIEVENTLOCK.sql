--
-- TABLE: jasieventlock
--

CREATE TABLE jasieventlock(
    evid           BIGINT          NOT NULL,
    hostname       VARCHAR(40),
    application    VARCHAR(20),
    username       VARCHAR(20),
    LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT PK3 PRIMARY KEY (evid)
)
;

