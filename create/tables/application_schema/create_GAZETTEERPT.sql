--
-- TABLE: gazetteerpt
-- SRID 4326 = WGS84
-- location: POINTZ(lon lat z)

CREATE TABLE gazetteerpt(
    gazid     BIGINT    NOT NULL,
    type      SMALLINT  NOT NULL,
    lat       DOUBLE PRECISION    NOT NULL,
    lon       DOUBLE PRECISION   NOT NULL,
    z         DOUBLE PRECISION,
    name      VARCHAR(48)     NOT NULL,
    state     VARCHAR(2),
    county    VARCHAR(32),
    map       VARCHAR(48),
    datumh    VARCHAR(8),
    datumv    VARCHAR(8),
    lddate    TIMESTAMP             DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT gazpt_pk_gazid PRIMARY KEY (gazid)
)
;
