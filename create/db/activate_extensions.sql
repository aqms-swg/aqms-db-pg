-- RUN THIS AS THE POSTGRES USER on the database you created 
-- i.e. psql -d DBNAME < pg_activate_extensions.sql
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_topology;
CREATE EXTENSION fuzzystrmatch;
CREATE EXTENSION postgis_tiger_geocoder;
