#!/usr/bin/python3

#quick utility to cleanup waveform files with no corresponding db entries.
#Victor Kress, PNSN 2011/11/01
#converted to postgresql and python3 2021/12/10

import sys 
import os
import shutil
import psycopg2
from getpass import getpass

usage='''VERY DANGEROUS PROGRAM!
Usage:cleanOrphinWaveforms.py [delete|test]
no files will actually be deleted unless the final 'delete' argument is
specified.
''' 

trashRoot='/archive/trash'

safe=True

def dbConnect():
    '''get connection object for local database.  Must be run on primary PP
    param password password for browser@localhost
    return open connection object or None
    '''
    pwd=getpass(prompt='password for browser@localhost:')
    constr="host=localhost dbname=archdb user=browser password=%s"%pwd
    try:
        con=psycopg2.connect(constr)
    except psycopg2.Error as e:
        print (e.pgerror)
        return None
    return con

if __name__=="__main__":
    if len(sys.argv)<=1:
        print (usage)
        sys.exit(0)
    if len(sys.argv)>=1 and sys.argv[1]=='delete':
        resp=input('this is very dangerous. Are you sure (yes/no)? :')
        if resp=='yes':
            safe=False

    con=dbConnect()
    if con:
        curs=con.cursor()
    else:
        print ("could not connect to database")
        sys.exit(1)
    #get waveform directory root (i.e /waveforms)
    curs.execute("SELECT fileroot FROM waveroots WHERE status='T'")
    wfDir=curs.fetchone()[0]
    if wfDir[-1]!='/': wfDir=wfDir+'/'
    edirlist=os.listdir(wfDir)
    ndel=0
    print ('Trigger waveforms are in %s'%wfDir) 
    #loop through event directories
    for edir in edirlist:
        if edir.isdigit():
            getstr='''SELECT count(awe.wfid) 
                      FROM AssocWaE awe,waveform w 
                      WHERE awe.evid=%s AND w.wfid=awe.wfid AND w.status='T'
                   '''
            curs.execute(getstr,(edir,))
            n=int(curs.fetchone()[0])
            if n==0:
                dname=wfDir+edir+'/'
                if safe:
                    print ('would be ',end='')
                trash=trashRoot+'/'+edir
                print ('moving %s to %s'%(dname,trash))
                if not safe:
                    #conservative
                    shutil.move(dname,trash)
                    #riskier
                    #print ('removing '+dname)
                    #wfs=os.listdir(dname)
                    #for wf in wfs:
                    #    print ('deleting '+dname+wf)
                    #    os.remove(dname+wf)
                    #os.rmdir(dname)
                ndel+=1
            else:
                print ('%d db waveform refs match waveform directory %s'%(n,edir))
        else:
            print ("Can't process directory %s"%edir)
    curs.close()
    con.close()

    if safe:
        print ('would have ',end='')
    print ('deleted waveforms for %d invalid events'%ndel)
    sys.exit(0)
