select l.pid, l.mode, sa.pid, sa.query
from pg_locks l
inner join pg_stat_activity sa
        on l.pid = sa.pid
where l.mode like '%xclusive%';
