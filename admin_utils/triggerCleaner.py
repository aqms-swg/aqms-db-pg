#!/usr/bin/python3
#triggerCleaner.py
#Utility to clean old triggers in AQMS database
#Victor Kress, PNSN 2016/04/20
#converted to postgreSQL psycopg2 2019/09/27
#converted to python3 202012/10

import re
import datetime
import os.path
import sys
import time
import psycopg2
import traceback
from getpass import getpass

save_n_weeks=52
#write final clean date to text file
#writefile=None
writefile='NextTrigCleanDay'
#delete waveform files?  Functionality not yet implemented.
wfdelete=False

usage='''Utility to clean old triggers in AQMS database
triggerCleaner.py [yyyy/mm/dd | dayfile] [ndays]
ndays is number of days to delete.  Defaults to 1
will not delete triggers younger than %d weeks.
dayfile is just a flat text file with start date.
It is re-written with each run, so subsequent runs can
just use this as a starting point for next run.
'''%save_n_weeks

redate=re.compile('(\d{4})/(\d{2})/(\d{2})')
today=datetime.date.today()
no_younger_than=today-datetime.timedelta(weeks=save_n_weeks)

def getDate(str):
    '''
    parse datetime.date from argument string or file
    @return datetime.date object
    '''
    ddate=None
    m=redate.match(str)
    if m!=None:
        ddate=datetime.date(*map(int,m.groups()))
    elif os.path.isfile(str):
        f=open(str,'r')
        s=f.readline()
        f.close()
        m=redate.match(s)
        if m!=None:
            ddate=datetime.date(*map(int,m.groups()))
    return ddate

def cleanDay(cdate,con):
    '''
    This is the section that does the actual cleaning
    @param cdate datetime.date day to clean
    @parma con open db connection object
    '''
    ret=None
    cdatestr=cdate.strftime('%Y/%m/%d')
    curs=con.cursor()
    print ('Cleaning %s'%cdatestr)
    curs.execute('SELECT db_cleanup.delete_triggers(%s)',(cdatestr,));
    ret=curs.fetchone()
    for notice in con.notices:
        print (notice)
    curs.close()
    if ret==None:
        return 0
    else:
        return ret[0];

def dbConnect():
    '''get connection object for local database.  Must be run on primary PP
    param password password for browser@localhost
    return open connection object or None
    '''
    pwd=getpass(prompt='password for rtem@localhost:')
    constr="host=localhost dbname=archdb user=rtem password=%s"%pwd
    try:
        con=psycopg2.connect(constr)
    except psychopg2.Error as e:
        print (e.pgerror)
        return None
    return con

if __name__=="__main__":
    if len(sys.argv)<2:
        print (usage)
        sys.exit(1)
    ddate=getDate(sys.argv[1])
    if ddate==None:
        print ('Could not parse date from: %s'%sys.argv[1])
        sys.exit(1)
    if len(sys.argv)==4:  #number of days to delete (defaults to 1)
        ndays=int(sys.argv[3])
    else: ndays=1
    con=dbConnect()
    if con==None:
        print ('Could not get db connection for browser@archdb')
        sys.exit(1)

    #db setup and initialization
    curs=con.cursor()
    curs.execute("SELECT db_cleanup.init()")
    curs.execute("SELECT db_cleanup.set_verbosity(3)")
    #stored procedure for wf delete not yet ported to PostgreSQL
    #if wfdelete:
    #    curs.execute("SELECT db_cleanup.set_wavefile_delete('true')")        
    curs.close();

    lastday=min(ddate+datetime.timedelta(days=(ndays-1)),no_younger_than)
    while ddate<=lastday:        
        result=cleanDay(ddate,con)
        print ('deleted %d triggers.'%result)
        ddate+=datetime.timedelta(days=1)
        if ddate<lastday:
            time.sleep(30) #wait 30 seconds to ease load
    print ('Cleaned %d days'%ndays)
    if writefile:
        f=open(writefile,'w')
        f.write(ddate.strftime('%Y/%m/%d'))
        f.close
    print ('done')
    con.commit()
    con.close()
    sys.exit(0)
    
