CREATE OR REPLACE FUNCTION quarry.getPkgId() RETURNS VARCHAR AS $$
DECLARE
  v_version VARCHAR := 'quarry v1.0.0-pg';
BEGIN
  RETURN v_version;
END;
$$ LANGUAGE plpgsql;

-- Return gazid of best fitting quarry, 0 if no match.
CREATE OR REPLACE FUNCTION quarry.isquarry(p_lat ORIGIN.LAT%TYPE, p_lon ORIGIN.LON%TYPE,
                    p_time ORIGIN.DATETIME%TYPE, p_depth ORIGIN.DEPTH%TYPE,
                    p_mag NETMAG.MAGNITUDE%TYPE) RETURNS BIGINT AS $$
  DECLARE
    v_day INTEGER := 0;
    v_hr INTEGER := 0;
    v_ss DOUBLE PRECISION := 0;
    v_mi INTEGER := 0;
    v_dist DOUBLE PRECISION := 0;
--    --
    v_ts_event TIMESTAMP (0) WITH TIME ZONE;
    v_index INTEGER;
    v_qb_day VARCHAR(7);
    v_time_org TIME;
    v_time_qbs TIME;
    v_time_qbe TIME;
    --
    v_status BIGINT := 0;
    v_rows INTEGER := 0;
    --
    quarry_cursor CURSOR ( otime TIMESTAMP WITHOUT TIME ZONE) FOR SELECT g.gazid,g.lat,g.lon,g.name,q.ondate,q.offdate,COALESCE(q.radius,1.0) radius,q.mdepth,q.mmag,q.days,q.stime,q.etime,q.tflg  
        FROM gazetteerpt g LEFT OUTER JOIN gazetteerquarry q ON g.gazid=q.gazid WHERE g.type=30 AND (q.ondate is NULL OR (q.ondate < otime AND q.offdate > otime));

  BEGIN
    -- convert truetime seconds to a timestamp (use nominal to get posix time)
    v_ts_event := TO_TIMESTAMP(truetime.true2nominalf(p_time::double precision));
    v_day := TO_CHAR(v_ts_event,'D');
    v_hr := (EXTRACT ( HOUR FROM v_ts_event ))::INTEGER;
    v_mi := (EXTRACT ( MINUTE FROM v_ts_event ))::INTEGER;
    v_ss := (EXTRACT ( SECOND FROM v_ts_event ))::DOUBLE PRECISION;
    
    <<QUARRY_LOOP>>
    FOR quarry_rec IN quarry_cursor ( otime := v_ts_event ) LOOP
        -- RAISE NOTICE 'quarry: %', quarry_rec.name;
        -- see if event mag is small enough or not restricted
        IF (p_mag IS NULL OR quarry_rec.mmag IS NULL OR p_mag <= quarry_rec.mmag) THEN 
            -- pass
            -- RAISE NOTICE 'passed magnitude check  %', p_mag;
            -- next, check proximity to a known quarry
            v_dist := wheres.separation_km(quarry_rec.lat, quarry_rec.lon, p_lat, p_lon);
            IF ( v_dist <= quarry_rec.radius ) THEN
                -- pass
                -- RAISE NOTICE 'passed distance check  % < %', v_dist, quarry_rec.radius;
                -- check depth
                IF (p_depth IS NULL OR quarry_rec.mdepth IS NULL OR p_depth <= quarry_rec.mdepth) THEN
                    -- pass
                    -- RAISE NOTICE 'passed depth check %', p_depth;
                    -- check schedule if requested (tflg=1)
                    IF ( quarry_rec.tflg IS NOT NULL AND quarry_rec.tflg = 1) THEN

                        -- Yes, check blasting schedule
                        -- days, stime, etime
                        -- '1111111', '00:00:00', '02:00:00' 
                        v_index := 1;

                        <<QB_TIMES_LOOP>>
                        FOREACH v_qb_day IN ARRAY quarry_rec.days LOOP

                            IF (SUBSTR(v_qb_day, v_day, 1) = '1') THEN
                                -- day of the week matches, convert times
                                v_time_org := make_time(v_hr,v_mi,v_ss);
                                v_time_qbs := make_time(substring(quarry_rec.stime[v_index],1,2)::INTEGER, substring(quarry_rec.stime[v_index],4,2)::INTEGER, substring(quarry_rec.stime[v_index],7,2)::DOUBLE PRECISION);
                                v_time_qbe := make_time(substring(quarry_rec.etime[v_index],1,2)::INTEGER, substring(quarry_rec.etime[v_index],4,2)::INTEGER, substring(quarry_rec.etime[v_index],7,2)::DOUBLE PRECISION);
                                -- Is is event time within start-end time range of blast window?
                                IF (v_time_org >= v_time_qbs AND v_time_org <= v_time_qbe) THEN
                                    -- RAISE NOTICE 'yes, active on day of week: %',v_day;
                                    -- pass, found a candidate quarry
                                    v_status := quarry_rec.gazid;
                                    EXIT QUARRY_LOOP;
                                END IF;
                            END IF;
                            v_index := v_index + 1;
                        END LOOP QB_TIMES_LOOP;

                    ELSE
                        -- No need to check blasting schedule, found candidate quarry
                        v_status := quarry_rec.gazid;
                        EXIT QUARRY_LOOP;
                    END IF; --blasting schedule check
                END IF; --depth check
            END IF; --distance check
        END IF; --magnitude check

    END LOOP QUARRY_LOOP;

    GET DIAGNOSTICS v_rows := ROW_COUNT;
    -- RAISE NOTICE 'rows returned: %', v_rows;
    RETURN v_status;

EXCEPTION WHEN OTHERS THEN
   RAISE NOTICE USING MESSAGE='Error in isQuarry(): ' || v_rows || '-' || SQLERRM || ': ' || SQLSTATE;     

RETURN v_status;
END;
$$ LANGUAGE plpgsql;

