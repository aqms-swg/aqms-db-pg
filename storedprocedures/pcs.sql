/**
*   *Current version: 1.0.0 2020-08-10*
*
*   Functions/Procedures in this package are for applications that utilize 
*   process event state processing tables **PCS_SIGNAL**, **PCS_STATE**, **PCS_TRANSITION** 
*  
*   Used in production: yes, UTC version
*  
*   ### DEPENDENT APPLICATIONS
*    - Functions in schema DB_CLEANUP
*    - Functions in schema EPREF
*    - Java org.trinet.pcs.AbstractProcessingController.class and subclasses like:
*     ```
*     Java org.trinet.pcs.HypoMagProcessingController.class 
*     Java org.trinet.pcs.RcgProcessingModule.class
*     Java org.trinet.jasi.TN.SolutionTN.class
*     ```
*  
*    * perl utilities:
*     ```
*     /utils/scripts/next
*     /utils/scripts/post
*     /utils/scripts/postFromList
*     /utils/scripts/result
*     /utils/scripts/resultAll
*     /utils/scripts/resultByRank
*     /utils/scripts/unpost
*  
*     /utils/scripts/perlmodules/PCS.pm
*    ```
*  
*   ### DEPENDS ON
*    PCS_SIGNAL, PCS_STATE, PCS_TRANSITION, and AUTOPOSTER tables 
*/

/**
* Returns version number.
*
*/
CREATE OR REPLACE FUNCTION pcs.getPkgId()
RETURNS varchar(80) AS $$
DECLARE
pkgId varchar(80) := 'pcs 1.0.0 2020-08-10';
BEGIN
    RETURN pkgId;
END;
$$ LANGUAGE plpgsql;

/**
*    pcs.amPrimary()
*    Returns 1 if the most recent row in rt_role.primary_system column is 'true' (case insensitive)
*    Returns 0 otherwise
*/
CREATE OR REPLACE FUNCTION pcs.amPrimary() RETURNS INTEGER AS $$
DECLARE
      v_primary VARCHAR(8) := 'FALSE';
BEGIN
        SELECT primary_system INTO v_primary FROM trinetdb.rt_role
		WHERE modification_time=(select max(modification_time) FROM trinetdb.rt_role);
        IF ( UPPER(v_primary) = 'TRUE' ) THEN
            RETURN 1;
        ELSE 
            RETURN 0;
        END IF;
END
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS pcs.post_id(p_group pcs_state.controlgroup%TYPE,
                   p_source pcs_state.sourcetable%TYPE,
                   p_id pcs_state.id%TYPE,
                   p_state pcs_state.state%TYPE,
                   p_rank pcs_state.rank%TYPE,
                   p_result pcs_state.result%TYPE,
                   p_commit DOUBLE PRECISION,
                   p_lddate pcs_state.lddate%TYPE);
/** -----------------------------------------------------------------
*   pcs.post_id(p_group,p_source,p_id,p_state,p_rank,p_result,p_commit,p_lddate)
*   p_group, p_source, p_id, p_state, p_rank
*   Checks whether the specified state is already in the PCS_STATE table, if so, it only tries 
*   to do a commit unless p_commit=0 (>0 or NULL will commit).
*   If it is a new state, it will insert a row in the PCS_STATE table. If that was successful and the result
*   column of the new state is not 0, then transition to any new state(s). The transition function will then do the
*   commit instead. If the new state.result=0, then just go ahead and commit, unless p_commit=0 (>0 or NULL will commit).
*   RH: original version of this function with this call signature appears to allow two of the
*   same states controlgroup.sourcetable.id.state as long as they have different ranks.
*   Our PCS_State table has primary key controlgroup.sourcetable.id.state so CAN NOT have multiple
*   states even if they have different ranks.
*/
CREATE OR REPLACE FUNCTION pcs.post_id(p_group pcs_state.controlgroup%TYPE,
                   p_source pcs_state.sourcetable%TYPE,
                   p_id pcs_state.id%TYPE,
                   p_state pcs_state.state%TYPE,
                   p_rank pcs_state.rank%TYPE,
                   p_result pcs_state.result%TYPE,
                   p_commit DOUBLE PRECISION,
                   p_lddate pcs_state.lddate%TYPE) RETURNS INTEGER AS $$
  DECLARE
    v_istat INTEGER := 0;
    v_rank_db trinetdb.pcs_state.rank%TYPE := 0;
    v_result_db trinetdb.pcs_state.result%TYPE := 0;
    v_idummy INTEGER := 0;
    c CURSOR FOR SELECT rank, result FROM trinetdb.pcs_state WHERE controlgroup = p_group AND 
	sourcetable = p_source AND state = p_state AND id = p_id ORDER BY rank DESC;

  BEGIN
    OPEN c;
    FETCH FROM c into v_rank_db, v_result_db;
    IF v_rank_db is not NULL THEN
        -- This state is already in the database, no need to do a post.
        v_istat := 0;
    ELSE
        -- insert new state
        -- RAISE NOTICE USING MESSAGE = 'post_id:p_group: ' || p_group || ' p_source: ' || p_source || ' p_id: '|| p_id || ' p_state: ' || p_state || ' p_rank: ' || p_rank || ' p_result: ' || p_result;
        INSERT INTO trinetdb.pcs_state (controlgroup,sourcetable,id,state,rank,result,subsource,lddate) VALUES
         (p_group,p_source,p_id,p_state,p_rank,p_result,current_database()::varchar(8),p_lddate);
        GET DIAGNOSTICS v_istat = ROW_COUNT;
        -- RAISE NOTICE USING MESSAGE = 'inserted ' || v_istat || ' rows into pcs_state';
    END IF;
    CLOSE c;

    -- If no change, v_istat = 0
    IF (v_istat > 0 AND p_result <> 0) THEN --  Transition does a commit !!!
        -- RAISE NOTICE 'Transitioning new state';
        v_idummy := pcs.transition(p_group, p_source, p_state, p_id);
    ELSIF (p_commit > 0 OR p_commit IS NULL) THEN
      -- RAISE NOTICE 'OK to commit';
      --COMMIT; 
    END IF;

    RETURN v_istat;

  EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE USING MESSAGE = 'post_id error: ' || SQLSTATE || ' ' || SQLERRM;
        RETURN -1;
END
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS pcs.post_id(p_group pcs_state.controlgroup%TYPE,
                   p_source pcs_state.sourcetable%TYPE,
                   p_id pcs_state.id%TYPE,
                   p_state pcs_state.state%TYPE,
                   p_rank pcs_state.rank%TYPE,
                   p_result pcs_state.result%TYPE,
                   p_commit DOUBLE PRECISION);
/**
    pcs.post_id(p_group,p_source,p_id,p_state,p_rank,p_result,p_commit)
    Checks whether the specified state is already in the PCS_STATE table, if so, it only tries 
    to do a commit unless p_commit=0 (>0 or NULL will commit).
    If it is a new state, it will insert a row in the PCS_STATE table. If that was successful and the result
    column of the new state is not 0, then transition to any new state(s). The transition function will then do the
    commit instead. If the new state.result=0, then just go ahead and commit, unless p_commit=0 (>0 or NULL will commit).
*/
CREATE OR REPLACE FUNCTION pcs.post_id(p_group pcs_state.controlgroup%TYPE,
                   p_source pcs_state.sourcetable%TYPE,
                   p_id pcs_state.id%TYPE,
                   p_state pcs_state.state%TYPE,
                   p_rank pcs_state.rank%TYPE,
                   p_result pcs_state.result%TYPE,
                   p_commit DOUBLE PRECISION) RETURNS INTEGER AS $$
  DECLARE
    v_istat INTEGER := 0;
    v_rank_db trinetdb.pcs_state.rank%TYPE := 0;
    v_result_db trinetdb.pcs_state.result%TYPE := 0;
    v_idummy INTEGER := 0;
    c CURSOR FOR SELECT rank, result FROM trinetdb.pcs_state WHERE controlgroup = p_group AND 
	sourcetable = p_source AND state = p_state AND id = p_id ORDER BY rank DESC;
--
  BEGIN
    OPEN c;
    FETCH FROM c into v_rank_db, v_result_db;
    IF v_rank_db is not NULL THEN
        -- This state is already in the database, no need to do a post.
        v_istat := 0;
    ELSE
        -- insert new state
        -- RAISE NOTICE USING MESSAGE = 'post_id:p_group: ' || p_group || ' p_source: ' || p_source || ' p_id: '|| p_id || ' p_state: ' || p_state || ' p_rank: ' || p_rank || ' p_result: ' || p_result;
        INSERT INTO trinetdb.pcs_state (controlgroup,sourcetable,id,state,rank,result,subsource) VALUES
         (p_group,p_source,p_id,p_state,p_rank,p_result,current_database()::varchar(8));
        GET DIAGNOSTICS v_istat = ROW_COUNT;
        -- RAISE NOTICE USING MESSAGE = 'inserted ' || v_istat || ' rows into pcs_state';
    END IF;
    CLOSE c;
--
    -- If no change, v_istat = 0
    IF (v_istat > 0 AND p_result <> 0) THEN --  Transition does a commit !!!
        -- RAISE NOTICE 'Transitioning new state';
        v_idummy := pcs.transition(p_group, p_source, p_state, p_id);
    ELSIF (p_commit > 0 OR p_commit IS NULL) THEN
      -- RAISE NOTICE 'OK to commit';
      --COMMIT; 
    END IF;
--
    RETURN v_istat;

  EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE USING MESSAGE = 'post_id error: ' || SQLSTATE || ' ' || SQLERRM;
        RETURN -1;
--
END
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS pcs.post_id(p_group pcs_state.controlgroup%TYPE,
                   p_source pcs_state.sourcetable%TYPE,
                   p_id pcs_state.id%TYPE,
                   p_state pcs_state.state%TYPE,
                   p_rank pcs_state.rank%TYPE);
/**
    pcs.post_id(p_group,p_source,p_id,p_rank)
    NO result value provided, set result=0, and p_commit=1. Because result=0
    this version will NOT call pcs.transition to transition to any new states.
    will always try to commit.
*/
CREATE OR REPLACE FUNCTION pcs.post_id(p_group pcs_state.controlgroup%TYPE,
                   p_source pcs_state.sourcetable%TYPE,
                   p_id pcs_state.id%TYPE,
                   p_state pcs_state.state%TYPE,
                   p_rank pcs_state.rank%TYPE) RETURNS INTEGER AS $$
 BEGIN
   RETURN pcs.post_id(p_group, p_source, p_id, p_state, p_rank, 0, 1); -- 0,1 = > result=0,commit
 END
$$ LANGUAGE plpgsql;
--
DROP FUNCTION IF EXISTS pcs.post_id(p_group pcs_state.controlgroup%TYPE,
                   p_source pcs_state.sourcetable%TYPE,
                   p_id pcs_state.id%TYPE,
                   p_state pcs_state.state%TYPE,
                   p_rank pcs_state.rank%TYPE,
                   p_commit DOUBLE PRECISION);
/**
    pcs.post_id(p_group,p_source,p_id,p_rank,p_commit)
    NO result value provided, set result=0, and p_commit provided. Because result=0
    this version will NOT call pcs.transition to transition to any new states.
    will only try to commit if p_commit >0 or NULL.
*/
CREATE OR REPLACE FUNCTION pcs.post_id(p_group pcs_state.controlgroup%TYPE,
                   p_source pcs_state.sourcetable%TYPE,
                   p_id pcs_state.id%TYPE,
                   p_state pcs_state.state%TYPE,
                   p_rank pcs_state.rank%TYPE,
                   p_commit DOUBLE PRECISION) RETURNS INTEGER AS $$
 BEGIN
   RETURN pcs.post_id(p_group, p_source, p_id, p_state, p_rank, 0, p_commit); -- result 0, flag for commit
 END 
$$ LANGUAGE plpgsql;
--
DROP FUNCTION IF EXISTS pcs.putState
    ( p_group pcs_state.controlgroup%TYPE,
    p_source pcs_state.sourcetable%TYPE,
    p_id pcs_state.id%TYPE,
    p_state pcs_state.state%TYPE,
    p_rank pcs_state.rank%TYPE);
--
/**
    pcs.putState(p_group,p_source,p_id,p_state,p_rank)
    wrapper around pcs.post_id(p_group,p_source,p_id,p_state,p_rank,0,1.0).
    can therefore be replaced by pcs.post_id(p_group,p_source,p_id,p_state,p_rank) 
    which does the same thing (or vice versa).

    No transition, but commits (in the Oracle version)
    DEPRECATED function is an alias for function post_id with same parameters.
*/
CREATE OR REPLACE FUNCTION pcs.putState
  ( p_group pcs_state.controlgroup%TYPE,
    p_source pcs_state.sourcetable%TYPE,
    p_id pcs_state.id%TYPE,
    p_state pcs_state.state%TYPE,
    p_rank pcs_state.rank%TYPE) RETURNS INTEGER AS $$
 BEGIN
   -- RAISE NOTICE USING MESSAGE = 'putState:p_group: ' || p_group || ' p_source: ' || p_source || ' p_state: ' || p_state || ' p_rank: ' || p_rank;
   RETURN pcs.post_id(p_group, p_source, p_id, p_state, p_rank, 0.0, 1.0); -- result 0, commit
 END
$$ LANGUAGE plpgsql;
--
--
-- Does transition if result != 0 , always commits 
--- ------------------------------------------------------------------------------------------
---
DROP FUNCTION IF EXISTS pcs.putState
    ( p_group pcs_state.controlgroup%TYPE,
    p_source pcs_state.sourcetable%TYPE,
    p_id pcs_state.id%TYPE,
    p_state pcs_state.state%TYPE,
    p_rank pcs_state.rank%TYPE,
    p_result pcs_state.result%TYPE);
/** Schedule an event for state processing.
* Creates or updates a row in the PCS_STATE table whose key matches the input parameters.
* If input result is non-zero, all state transitions for input state result will occur
* after row insert. Does a database commit.
* Returns 1 on success, <= 0 failure.
* Example: call PCS.putState('TTP', 'TEST', 1, 'fubar', 100, 1) into :v_istatus;
*/
CREATE OR REPLACE FUNCTION pcs.putState
    ( p_group pcs_state.controlgroup%TYPE,
    p_source pcs_state.sourcetable%TYPE,
    p_id pcs_state.id%TYPE,
    p_state pcs_state.state%TYPE,
    p_rank pcs_state.rank%TYPE,
    p_result pcs_state.result%TYPE) RETURNS INTEGER AS $$
----------------------------------------------------------------------------
--- pcs.putState(p_group,p_source,p_id,p_state,p_rank,p_result)
--- wrapper around pcs.post_id(p_group,p_source,p_id,p_state,p_rank,p_result,1.0).
--- can therefore be replaced by pcs.post_id(p_group,p_source,p_id,p_state,p_rank,p_result) 
--- which does the same thing (or vice versa).
----------------------------------------------------------------------------
 BEGIN
   -- RAISE NOTICE USING MESSAGE = 'putState:p_group: ' || p_group || ' p_source: ' || p_source || ' p_state: ' || p_state || ' p_rank: ' || p_rank || ' p_result: ' || p_result;
   RETURN pcs.post_id(p_group, p_source, p_id, p_state, p_rank, p_result, 1.0); -- commit
 END 
$$ LANGUAGE plpgsql;
--
-- Does transition if result != 0 , always commits 
--- ------------------------------------------------------------------------------------------
---
DROP FUNCTION IF EXISTS pcs.putState
    ( p_group pcs_state.controlgroup%TYPE,
    p_source pcs_state.sourcetable%TYPE,
    p_id pcs_state.id%TYPE,
    p_state pcs_state.state%TYPE,
    p_rank pcs_state.rank%TYPE,
    p_result pcs_state.result%TYPE,
    p_lddate pcs_state.lddate%TYPE);
/**
* Schedule an event for state processing.
* Creates or updates a row in the PCS_STATE table whose key matches the input parameters.
* If input result is non-zero, all state transitions for input state result will occur
* after row insert. Does a database commit.
* Returns 1 on success, <= 0 failure.
* Example: call PCS.putState('TTP', 'TEST', 1, 'fubar', 100, 1) into :v_istatus;
*/
CREATE OR REPLACE FUNCTION pcs.putState
    ( p_group pcs_state.controlgroup%TYPE,
    p_source pcs_state.sourcetable%TYPE,
    p_id pcs_state.id%TYPE,
    p_state pcs_state.state%TYPE,
    p_rank pcs_state.rank%TYPE,
    p_result pcs_state.result%TYPE,
    p_lddate pcs_state.lddate%TYPE) RETURNS INTEGER AS $$
----------------------------------------------------------------------------
--- pcs.putState(p_group,p_source,p_id,p_state,p_rank,p_result)
--- wrapper around pcs.post_id(p_group,p_source,p_id,p_state,p_rank,p_result,1.0).
--- can therefore be replaced by pcs.post_id(p_group,p_source,p_id,p_state,p_rank,p_result) 
--- which does the same thing (or vice versa).
----------------------------------------------------------------------------
 BEGIN
   -- RAISE NOTICE USING MESSAGE = 'putState:p_group: ' || p_group || ' p_source: ' || p_source || ' p_state: ' || p_state || ' p_rank: ' || p_rank || ' p_result: ' || p_result || ' p_lddate: ' || p_lddate;
   RETURN pcs.post_id(p_group, p_source, p_id, p_state, p_rank, p_result, 1.0, p_lddate); -- commit
 END 
$$ LANGUAGE plpgsql;
--
DROP FUNCTION IF EXISTS pcs.unpost(p_id pcs_state.id%TYPE);
/**
* pcs.unpost(p_id)
* remove all rows (states) from the PCS_STATE table for this event p_id
* returns the number of rows that were removed.
*/
CREATE OR REPLACE FUNCTION pcs.unpost(p_id pcs_state.id%TYPE) RETURNS INTEGER AS $$
DECLARE
    v_istat INTEGER := 0;
BEGIN
    DELETE FROM trinetdb.pcs_state WHERE id = p_id;
    GET DIAGNOSTICS v_istat = ROW_COUNT;
    return v_istat;
END
$$ LANGUAGE plpgsql;
--
--
DROP FUNCTION IF EXISTS pcs.unpost(p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE,
                         p_state pcs_state.state%TYPE);
/**
* pcs.unpost(p_group,p_source,p_state)
* remove all rows from the PCS_STATE table for this type of state
* returns the number of rows that were removed.
*/
CREATE OR REPLACE FUNCTION pcs.unpost( p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE,
                                       p_state pcs_state.state%TYPE ) RETURNS INTEGER AS $$
DECLARE
    v_istat INTEGER := 0;
BEGIN
    DELETE FROM trinetdb.pcs_state WHERE controlgroup = p_group AND sourcetable = p_source AND state = p_state;
    GET DIAGNOSTICS v_istat = ROW_COUNT;
    return v_istat;
END
$$ LANGUAGE plpgsql;
 --
 -- 
DROP FUNCTION IF EXISTS pcs.putResult(
    p_group pcs_state.controlgroup%TYPE,
    p_source pcs_state.sourcetable%TYPE,
    p_id pcs_state.id%TYPE,
    p_state pcs_state.state%TYPE,
    p_rank pcs_state.rank%TYPE,
    p_result pcs_state.result%TYPE);
/**
* pcs.putResult(p_group,p_source,p_id,p_state,p_rank,p_result)
* Update the result value of the matching PCS_STATE row if its current result value is not the same. 
* If p_rank is NULL, then update the result value of all the rows for this state and id.
* If p_rank < 0, then only update the row with the highest rank for this state and id.
* else: update all the rows of matching rank, state, and id.
* If at least one row was updated (at least one new result value), transition to any new states.
* Returns 0 if no matching rows were found to update.
* Returns 1 is at least one row was updated.
* Returns 2 if all the matching rows already had the same result value (none updated).
* Returns -1 if an error occurred.
*/
CREATE OR REPLACE FUNCTION pcs.putResult(
    p_group pcs_state.controlgroup%TYPE,
    p_source pcs_state.sourcetable%TYPE,
    p_id pcs_state.id%TYPE,
    p_state pcs_state.state%TYPE,
    p_rank pcs_state.rank%TYPE,
    p_result pcs_state.result%TYPE) RETURNS INTEGER AS $$
DECLARE
    v_istat INTEGER := 0;
    v_idummy INTEGER := 0;
 BEGIN
      v_istat := pcs.result_id(p_group, p_source, p_id, p_state, p_rank, p_result);
      -- 
      -- If no row v_istat = 0, if already an existing row v_istat = 2 
      -- Transition only when matching row has a "new" result
      IF (v_istat = 1 AND p_result <> 0) THEN
        v_idummy := pcs.transition(p_group, p_source, p_state, p_id);
      END IF;
      --
      RETURN v_istat;
      --
 END
$$ LANGUAGE plpgsql;
--
-- ALL posted ranks for input id.
DROP FUNCTION IF EXISTS pcs.putResultAll(
      p_group pcs_state.controlgroup%TYPE,
      p_source pcs_state.sourcetable%TYPE,
      p_id pcs_state.id%TYPE,
      p_state pcs_state.state%TYPE,
      p_result pcs_state.result%TYPE);
/**
* pcs.putResultAll(p_group,p_source,p_id,p_state,p_result)
* Update the result value of the matching PCS_STATE row if its current result value is not the same. 
* Updates the result value of all the rows for this state and id.
* If at least one row was updated (at least one new result value), transition to any new states.
* Returns 0 if no matching rows were found to update.
* Returns 1 is at least one row was updated.
* Returns 2 if all the matching rows already had the same result value (none updated).
* Returns -1 if an error occurred.
*/
CREATE OR REPLACE FUNCTION pcs.putResultAll(
    p_group pcs_state.controlgroup%TYPE,
    p_source pcs_state.sourcetable%TYPE,
    p_id pcs_state.id%TYPE,
    p_state pcs_state.state%TYPE,
    p_result pcs_state.result%TYPE) RETURNS INTEGER AS $$
 BEGIN
     RETURN pcs.putResult(p_group, p_source, p_id, p_state, NULL, p_result);
 END 
$$ LANGUAGE plpgsql;
--
-- Highest ranking for input id
DROP FUNCTION IF EXISTS pcs.putResult
  ( p_group pcs_state.controlgroup%TYPE,
    p_source pcs_state.sourcetable%TYPE,
    p_id pcs_state.id%TYPE,
    p_state pcs_state.state%TYPE,
    p_result pcs_state.result%TYPE);
CREATE OR REPLACE FUNCTION pcs.putResult
  ( p_group pcs_state.controlgroup%TYPE,
    p_source pcs_state.sourcetable%TYPE,
    p_id pcs_state.id%TYPE,
    p_state pcs_state.state%TYPE,
    p_result pcs_state.result%TYPE) RETURNS INTEGER AS $$
----------------------------------------------------------------------------
--- pcs.putResult(p_group,p_source,p_id,p_state,p_result)
--- Update the result value of the matching PCS_STATE row if its current result value is not the same. 
--- Only updates the rows with the highest rank for this state and id.
--- If at least one row was updated (at least one new result value), transition to any new states.
--- Returns 0 if no matching rows were found to update.
--- Returns 1 is at least one row was updated.
--- Returns 2 if all the matching rows already had the same result value (none updated).
--- Returns -1 if an error occurred.
----------------------------------------------------------------------------
 BEGIN
     RETURN pcs.putResult(p_group, p_source, p_id, p_state, -1, p_result);
 END
$$ LANGUAGE plpgsql;
--
-- All ids matching state description
DROP FUNCTION IF EXISTS pcs.putResultAll
  ( p_group pcs_state.controlgroup%TYPE,
    p_source pcs_state.sourcetable%TYPE,
    p_state pcs_state.state%TYPE,
    p_result pcs_state.result%TYPE);
CREATE OR REPLACE FUNCTION pcs.putResultAll
  ( p_group pcs_state.controlgroup%TYPE,
    p_source pcs_state.sourcetable%TYPE,
    p_state pcs_state.state%TYPE,
    p_result pcs_state.result%TYPE) RETURNS INTEGER AS $$
----------------------------------------------------------------------------
--- pcs.putResultAll(p_group,p_source,p_state,p_result)
--- Update the result value of all the matching PCS_STATE state-id rows if its current result value is not the same. 
--- If at least one row was updated (at least one new result value) and the new result != 0, transition to any new states.
--- Returns 0 if no matching rows were found to update.
--- Returns number of updated rows if at least one row was updated.
--- Returns 0 if all the matching rows already had the same result value (none updated).
----------------------------------------------------------------------------
DECLARE
    v_istat INTEGER := 0;
    v_idummy INTEGER := 0;
  --
BEGIN
      UPDATE trinetdb.pcs_state SET result = coalesce(p_result, 0) WHERE 
        controlgroup = p_group AND sourcetable = p_source AND state = p_state
        AND (result IS NULL OR result != p_result);
      --
      GET DIAGNOSTICS v_istat = ROW_COUNT;
      --
      -- If no change v_istat = 0
      IF (v_istat > 0) THEN
        IF  (p_result <> 0) THEN
          v_idummy := pcs.transition(p_group, p_source, p_state);
        ELSE  -- commit 0 result update
          -- RAISE NOTICE 'OK to commit';
          --COMMIT;
        END IF;
      END IF;
      --
      RETURN v_istat;
      --
END 
$$ LANGUAGE plpgsql;
--
-- -1 id not posted, 0 processed with result, 1 posted unprocessed, >1 rank blocked can't process
--
DROP FUNCTION IF EXISTS pcs.run_status(p_group pcs_state.controlgroup%TYPE, 
          p_source pcs_state.sourcetable%TYPE, p_state pcs_state.state%TYPE, 
          p_id pcs_state.id%TYPE);
CREATE OR REPLACE FUNCTION pcs.run_status(p_group pcs_state.controlgroup%TYPE, 
          p_source pcs_state.sourcetable%TYPE, p_state pcs_state.state%TYPE, 
          p_id pcs_state.id%TYPE) RETURNS INTEGER AS $$
DECLARE
   -- v_sr_rec_tab state_row_rec_table;
   r pcs_state%ROWTYPE;
   v_maxrank pcs_state.rank%TYPE := -1;
   v_count INTEGER :=0;
   --
BEGIN
      -- get all state postings for this id.
       SELECT count(*) FROM trinetdb.pcs_state into v_count where id = p_id;
       IF (v_count = 0) THEN
            RETURN -1; -- no postings for id 
       END IF;
       FOR r IN SELECT  * FROM trinetdb.pcs_state where id = p_id LOOP
        -- 
        -- Find max posting rank of all postings for id.
        IF (r.rank > v_maxrank) THEN
            v_maxrank := r.rank;
        END IF;
       END LOOP;
        --
        -- Loop over array to find any group,table,state matching inputs
       FOR r IN SELECT  * FROM trinetdb.pcs_state where id = p_id LOOP
          IF (
              (r.controlGroup = p_group) AND
              (r.sourceTable = p_source) AND
              (r.state = p_state)
          ) THEN
            IF (r.result != 0) THEN
              return 0; -- stale, old result, already run
            END IF;
            -- return i+1; // i:=0 ready to run, if i>0 state run is rank blocked
            IF (r.rank >= v_maxrank) THEN
              RETURN 1;
            END IF;
            RETURN 2;
          END IF;
       END LOOP;
       RETURN -1; -- no postings match id
END 
$$ LANGUAGE plpgsql;

  --
  --
DROP FUNCTION IF EXISTS pcs.result_of(p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE, p_state pcs_state.state%TYPE, p_id pcs_state.id%TYPE);
CREATE OR REPLACE  FUNCTION pcs.result_of(p_group pcs_state.controlgroup%TYPE, 
      p_source pcs_state.sourcetable%TYPE, p_state pcs_state.state%TYPE, 
      p_id pcs_state.id%TYPE) RETURNS DOUBLE PRECISION AS $$
DECLARE
    --
    -- v_sr_tab state_row_rec_table := NULL;
   r pcs_state%ROWTYPE;
   c CURSOR FOR SELECT * FROM trinetdb.pcs_state WHERE
      controlGroup = p_group AND sourceTable = p_source AND state = p_state AND id = p_id ORDER BY sr.rank DESC;
    --
BEGIN
    OPEN c;
    FETCH FROM c into r;
    RETURN r.result;
END 
$$ LANGUAGE plpgsql;
  --
  --
DROP FUNCTION IF EXISTS pcs.recycle(p_groupOld pcs_state.controlgroup%TYPE, 
                    p_sourceOld pcs_state.sourcetable%TYPE, p_stateOld pcs_state.state%TYPE, 
                    p_result pcs_state.result%TYPE, p_rankOld pcs_state.rank%TYPE, 
                    p_groupNew pcs_state.controlgroup%TYPE, p_sourceNew pcs_state.sourcetable%TYPE, 
                    p_stateNew pcs_state.state%TYPE, p_rank pcs_state.rank%TYPE);
CREATE OR REPLACE  FUNCTION pcs.recycle(p_groupOld pcs_state.controlgroup%TYPE, 
                    p_sourceOld pcs_state.sourcetable%TYPE, p_stateOld pcs_state.state%TYPE, 
                    p_result pcs_state.result%TYPE, p_rankOld pcs_state.rank%TYPE, 
                    p_groupNew pcs_state.controlgroup%TYPE, p_sourceNew pcs_state.sourcetable%TYPE, 
                    p_stateNew pcs_state.state%TYPE, p_rank pcs_state.rank%TYPE) RETURNS INTEGER AS $$

DECLARE
    --
    -- v_sr_tab state_row_rec_table := NULL;
    v_do_insert BOOLEAN := TRUE;
    -- v_new_sr_rec state_row_rec := NULL;
    v_istat INTEGER := 0;
   r pcs_state%ROWTYPE;
   query_str VARCHAR;
   rins pcs_state%ROWTYPE;
   c1 CURSOR FOR SELECT * FROM trinetdb.pcs_state WHERE controlGroup = p_groupOld AND sourceTable = p_sourceOld AND state = p_stateOld AND result = COALESCE(p_result, 0);
   c2 CURSOR FOR SELECT * FROM trinetdb.pcs_state WHERE controlGroup = p_groupOld AND sourceTable = p_sourceOld AND state = p_stateOld AND result = COALESCE(p_result, 0) AND rank = p_rankOld;
    --
BEGIN
        -- Get old postings with result code
        IF (p_rankOld IS NULL) THEN -- any rank
		OPEN c1;
		FETCH FROM c1 into r;
		query_str = 'SELECT * FROM trinetdb.pcs_state WHERE controlGroup =' || quote_ident(p_groupOld) ||
			' AND state=' || quote_ident(p_stateOld) || 
			' AND result = ' || COALESCE(p_result, 0);
        ELSE  -- specific rank
		OPEN c2;
		FETCH FROM c2 into r;
		query_str = 'SELECT * FROM trinetdb.pcs_state WHERE controlGroup =' || quote_ident(p_groupOld) ||
			' AND state=' || quote_ident(p_stateOld) || 
			' AND result = ' || COALESCE(p_result, 0) || ' AND rank = ' || p_rankOld;
        END IF;
        --
        IF (r.result IS NULL) THEN
            RETURN 0;
        END IF;
        --
        -- Do new states exist?
        v_do_insert := (p_groupNew IS NOT NULL AND
                        p_sourceNew IS NOT NULL AND
                        p_stateNew IS NOT NULL AND
                        p_rank > 0);
        --
        -- Set fixed column values common to all new rows
        IF (v_do_insert) THEN 
            rins.controlGroup := p_groupNew;
            rins.sourceTable := p_sourceNew;
            rins.state := p_stateNew;
            rins.rank := p_rank;
            rins.result := 0;
        END IF;
        --
        -- Look through old postings, delete row, then post new if specified
        FOR r IN EXECUTE query_str LOOP
            DELETE FROM trinetdb.pcs_state WHERE
            controlGroup = r.controlGroup AND sourceTable = r.sourceTable
            AND state = r.state AND id = r.id AND rank = r.rank;
            --
            IF (v_do_insert) THEN
                rins.id := r.id;
                INSERT INTO trinetdb.pcs_state (controlgroup,sourcetable,id,state,rank,result,subsource,lddate) VALUES
                    (rins.controlgroup, rins.sourcetable, rins.id, rins.state,
                     rins.rank, rins.result, current_database()::varchar(8), timezone('UTC'::text,NOW()) );
            END IF;
            v_istat := v_istat + 1;
        END LOOP;
        --
        IF (v_istat > 0) THEN
          -- RAISE NOTICE 'OK to commit';
          --COMMIT; -- after delete and post
        END IF;
        --
        RETURN v_istat;
  END
$$ LANGUAGE plpgsql;
  --
  --
DROP FUNCTION IF EXISTS pcs.recycle(p_groupOld pcs_state.controlgroup%TYPE, 
       p_sourceOld pcs_state.sourcetable%TYPE, p_stateOld pcs_state.state%TYPE, 
       p_result pcs_state.result%TYPE, p_rankOld pcs_state.rank%TYPE);
CREATE OR REPLACE FUNCTION pcs.recycle(p_groupOld pcs_state.controlgroup%TYPE, 
       p_sourceOld pcs_state.sourcetable%TYPE, p_stateOld pcs_state.state%TYPE, 
       p_result pcs_state.result%TYPE, p_rankOld pcs_state.rank%TYPE) RETURNS INTEGER
  AS $$
BEGIN
      RETURN pcs.recycle(p_groupOld, p_sourceOld, p_stateOld, p_result, p_rankOld, NULL, NULL, NULL, 0);
END
$$ LANGUAGE plpgsql;
  --
  --
DROP FUNCTION IF EXISTS pcs.recycle(p_groupOld pcs_state.controlgroup%TYPE, 
                   p_sourceOld pcs_state.sourcetable%TYPE, p_stateOld pcs_state.state%TYPE, 
                   p_result pcs_state.result%TYPE, p_groupNew pcs_state.controlgroup%TYPE, 
                   p_sourceNew pcs_state.sourcetable%TYPE, p_stateNew pcs_state.state%TYPE, 
                   p_rank pcs_state.rank%TYPE);
CREATE OR REPLACE FUNCTION pcs.recycle(p_groupOld pcs_state.controlgroup%TYPE, 
                   p_sourceOld pcs_state.sourcetable%TYPE, p_stateOld pcs_state.state%TYPE, 
                   p_result pcs_state.result%TYPE, p_groupNew pcs_state.controlgroup%TYPE, 
                   p_sourceNew pcs_state.sourcetable%TYPE, p_stateNew pcs_state.state%TYPE, 
                   p_rank pcs_state.rank%TYPE) RETURNS INTEGER AS $$
BEGIN
      RETURN pcs.recycle(p_groupOld, p_sourceOld, p_stateOld, p_result, NULL, p_groupNew, p_sourceNew, p_stateNew, p_rank);
END
$$ LANGUAGE plpgsql;
  --
  --
DROP FUNCTION IF EXISTS pcs.recycle(p_groupOld pcs_state.controlgroup%TYPE, 
                  p_sourceOld pcs_state.sourcetable%TYPE, p_stateOld pcs_state.state%TYPE, 
                  p_result pcs_state.result%TYPE);
CREATE OR REPLACE FUNCTION pcs.recycle(p_groupOld pcs_state.controlgroup%TYPE, 
                  p_sourceOld pcs_state.sourcetable%TYPE, p_stateOld pcs_state.state%TYPE, 
                  p_result pcs_state.result%TYPE) RETURNS INTEGER AS $$
BEGIN
      RETURN pcs.recycle(p_groupOld, p_sourceOld, p_stateOld, p_result, NULL, NULL, NULL, NULL, 0);
END
$$ LANGUAGE plpgsql;
  --
  --

DROP FUNCTION IF EXISTS pcs.getNonNullResults();
CREATE OR REPLACE  FUNCTION pcs.getNonNullResults() RETURNS SETOF trinetdb.pcs_state  AS $$
BEGIN
    RETURN QUERY SELECT sr.controlGroup, sr.sourceTable, sr.id, sr.state, sr.rank, sr.result,
      sr.secondaryid, sr.secondarysourcetable, sr.auth, sr.subsource, sr.lddate
      FROM trinetdb.pcs_state sr WHERE (result <> 0 AND result IS NOT NULL) 
      ORDER BY result DESC;
END 
$$ LANGUAGE plpgsql;
  --
  --
DROP FUNCTION IF EXISTS pcs.getNonNullResults(p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE);
CREATE OR REPLACE  FUNCTION pcs.getNonNullResults(p_group pcs_state.controlgroup%TYPE, 
                   p_source pcs_state.sourcetable%TYPE) RETURNS SETOF trinetdb.pcs_state AS $$
BEGIN
    RETURN QUERY SELECT sr.controlGroup, sr.sourceTable, sr.id, sr.state, sr.rank, sr.result,
      sr.secondaryid, sr.secondarysourcetable, sr.auth, sr.subsource, sr.lddate
      FROM trinetdb.pcs_state sr WHERE
        controlGroup = p_group
        AND sourceTable = p_source
        AND (result <> 0 AND result IS NOT NULL)
        ORDER BY result DESC;
END
$$ LANGUAGE plpgsql;
  --
  --
DROP FUNCTION IF EXISTS pcs.getNonNullResults(p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE, p_state pcs_state.state%TYPE);
CREATE OR REPLACE  FUNCTION pcs.getNonNullResults(p_group pcs_state.controlgroup%TYPE, 
                   p_source pcs_state.sourcetable%TYPE, p_state pcs_state.state%TYPE) 
                   RETURNS SETOF trinetdb.pcs_state AS $$
BEGIN
    RETURN QUERY SELECT sr.controlGroup, sr.sourceTable, sr.id, sr.state, sr.rank, sr.result,
      sr.secondaryid, sr.secondarysourcetable, sr.auth, sr.subsource, sr.lddate
      FROM trinetdb.pcs_state sr WHERE
      controlGroup = p_group
      AND sourceTable = p_source
      AND state = p_state
      AND (result <> 0 AND result IS NOT NULL) 
      ORDER BY result DESC;
END
$$ LANGUAGE plpgsql;
  --
  --
DROP FUNCTION IF EXISTS pcs.getNonNullResults(p_group pcs_state.controlgroup%TYPE,
                    p_source pcs_state.sourcetable%TYPE, p_state pcs_state.state%TYPE,
                    p_id pcs_state.id%TYPE);
CREATE OR REPLACE  FUNCTION pcs.getNonNullResults(p_group pcs_state.controlgroup%TYPE, 
                   p_source pcs_state.sourcetable%TYPE, p_state pcs_state.state%TYPE, 
                   p_id pcs_state.id%TYPE) RETURNS SETOF trinetdb.pcs_state AS $$
BEGIN
    RETURN QUERY SELECT sr.controlGroup, sr.sourceTable, sr.id, sr.state, sr.rank, sr.result,
      sr.secondaryid, sr.secondarysourcetable, sr.auth, sr.subsource, sr.lddate
      FROM trinetdb.pcs_state sr WHERE
      controlGroup = p_group
      AND sourceTable = p_source
      AND state = p_state
      AND id = p_id
      AND (result <> 0 AND result IS NOT NULL) 
      ORDER BY result DESC;
END
$$ LANGUAGE plpgsql;
  --
  --
-- For each PCS_STATE passed to this function, it finds which new states it should transition to from the PCS_TRANSITION table
-- It uses the PCS_STATE.controlgroup, sourcetable, state, and result in the where clause. Result cannot be 0 and not be NULL.
-- Then it returns the new PCS_STATES groupnew, sourcenew, statenew, ranknew, resultnew.
--    controlgroup         VARCHAR(20),
--    sourcetable          VARCHAR(20),
--    id                   NUMBER(15),
--    state                VARCHAR(20),
--    rank                 NUMBER(15),
--    result               NUMBER(15),
DROP FUNCTION IF EXISTS pcs.getNewStates(p_controlGroup pcs_state.controlgroup%TYPE,
          p_sourceTable pcs_state.sourcetable%TYPE, p_id pcs_state.id%TYPE, 
          p_state pcs_state.state%TYPE, p_rank pcs_state.rank%TYPE, 
          p_result pcs_state.result%TYPE);
CREATE OR REPLACE  FUNCTION pcs.getNewStates(p_controlGroup pcs_state.controlgroup%TYPE,
          p_sourceTable pcs_state.sourcetable%TYPE, p_id pcs_state.id%TYPE, 
          p_state pcs_state.state%TYPE, p_rank pcs_state.rank%TYPE, 
          p_result pcs_state.result%TYPE) RETURNS SETOF trinetdb.pcs_state AS $$ 
DECLARE
	R trinetdb.pcs_transition%ROWTYPE;
        v_sr_rec trinetdb.pcs_state%ROWTYPE;
BEGIN
	FOR R in
        	SELECT tr.groupOld, tr.sourceOld, tr.stateOld, tr.resultOld,
        		tr.groupNew, tr.sourceNew, tr.stateNew, tr.rankNew, tr.resultNew,
        		tr.auth, tr.subsource, tr.lddate
        	FROM trinetdb.pcs_transition tr WHERE
        		groupOld = p_controlGroup AND sourceOld = p_sourceTable AND
        		stateOld = p_state AND resultOld = p_result
        	ORDER BY groupOld, sourceOld, stateOld, resultOld,
                 	groupNew, sourceNew, stateNew DESC LOOP
        -- use the TransitionRows to make new State Table entries
            v_sr_rec.controlGroup := R.groupNew;  -- not null, but 'null' flags none
            v_sr_rec.sourceTable  := R.sourceNew; -- not null, but 'null' flags none
            v_sr_rec.id           := p_id;
            v_sr_rec.state        := R.stateNew;  -- not null, but 'null' flags none
            v_sr_rec.rank         := coalesce(R.rankNew, p_rank);
            v_sr_rec.result       := coalesce(R.resultNew, 0);
            RETURN NEXT v_sr_rec;
            --
        END LOOP;
        --
        RETURN;
END 
$$ LANGUAGE plpgsql;
  --
  --
DROP FUNCTION IF EXISTS pcs.transition(p_controlGroup pcs_state.controlgroup%TYPE, p_sourceTable pcs_state.sourcetable%TYPE, p_id pcs_state.id%TYPE, p_state pcs_state.state%TYPE, p_rank pcs_state.rank%TYPE, p_result pcs_state.result%TYPE);
CREATE OR REPLACE  FUNCTION pcs.transition(p_controlGroup pcs_state.controlgroup%TYPE, p_sourceTable pcs_state.sourcetable%TYPE, p_id pcs_state.id%TYPE, p_state pcs_state.state%TYPE, p_rank pcs_state.rank%TYPE, p_result pcs_state.result%TYPE) RETURNS INTEGER AS $$
DECLARE
      --
      -- v_new_sr_tab state_row_rec_table;
      --
      -- OKAY, POSTGRESQL doesn't allow tables to be passed in as arguments (nor RECORD or ROWTYPES), so we need
      -- to refactor this "transition" function in Oracle to one that does just one row
      --
      R trinetdb.pcs_state%ROWTYPE;
      --
      v_rowcount INTEGER := 0;
      v_idummy INTEGER := 0;
      v_istat INTEGER := 0;
BEGIN
      -- nothing to do if all input params are NULL
      -- RAISE NOTICE 'in full transition function';
      IF (coalesce(p_controlGroup,p_sourceTable,p_state) IS NULL AND p_id IS NULL AND coalesce(p_rank,p_result) IS NULL) THEN
            RETURN 0;
      END IF;
      -- 
          --
          -- input row should have non-null,non-blank processing description strings
      FOR R IN SELECT * FROM pcs.getNewStates(p_controlGroup,p_sourceTable,p_id,p_state,p_rank,p_result) LOOP -- filters out "NULL" new state 
          --
          -- RAISE NOTICE USING MESSAGE ='old: ' || p_controlGroup || '-' || p_sourceTable || '-' || p_id || '-' || p_state || '-' || p_rank || '-' || p_result;
          -- RAISE NOTICE USING MESSAGE ='new: ' || R.controlGroup || '-' || R.sourceTable || '-' || R.id || '-' || R.state || '-' || R.rank || '-' || R.result;
          IF (R.id IS NOT NULL ) THEN -- transitions
              --  
              -- RAISE NOTICE USING MESSAGE ='new: ' || R.controlGroup || '-' || R.sourceTable || '-' || R.id || '-' || R.state || '-' || R.rank || '-' || R.result;
              -- RAISE NOTICE 'Deleting old pcs_state row';
              DELETE FROM trinetdb.pcs_state WHERE 
                controlGroup = p_controlGroup AND sourceTable = p_sourceTable
                AND state = p_state AND id = p_id AND rank = p_rank;

	      GET DIAGNOSTICS v_rowcount = ROW_COUNT;
              --
              v_istat := v_istat + v_rowcount; -- just count of those rows deleted
              -- 
              -- insert new states
              -- Note 'NULL' stateNew is flags delete, no new posting
              -- RAISE NOTICE USING MESSAGE= 'State: ' || R.state;
              IF (UPPER(R.state) <> 'NULL') THEN
                   v_idummy := v_idummy + pcs.post_id(R.controlGroup,
                                                R.sourceTable,
                                                R.id,
                                                R.state,
                                                R.rank,
                                                R.result,  -- result used to be 0
                                                0); -- no commit
              END IF;
          END IF;
      END LOOP;

      RETURN v_istat;

EXCEPTION
    WHEN UNIQUE_VIOLATION THEN
      RAISE NOTICE USING MESSAGE='In transition, unable to post new state: ' || SQLSTATE || ' ' || SQLERRM;
      RETURN -1;
    WHEN OTHERS THEN
      RAISE NOTICE USING MESSAGE='PostgreSQL Error: ' || SQLSTATE || ' ' || SQLERRM;
      RETURN -1; 
END
$$ LANGUAGE plpgsql;
  --
  -- get all rows with an non-zero result code in State table
DROP FUNCTION IF EXISTS pcs.transition();
CREATE OR REPLACE  FUNCTION pcs.transition() RETURNS INTEGER AS $$
DECLARE
      R trinetdb.pcs_state%ROWTYPE;
      v_istat INTEGER := 0;
BEGIN
    FOR R in SELECT * FROM pcs.getNonNullResults() LOOP
       v_istat := v_istat + pcs.transition(R.controlGroup,R.sourceTable,R.id,R.state,R.rank,R.result);
    END LOOP;
    RETURN v_istat;
END
$$ LANGUAGE plpgsql;

  --
  -- get all rows with an non-zero result code
DROP FUNCTION IF EXISTS pcs.transition(p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE);
CREATE OR REPLACE  FUNCTION pcs.transition(p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE) RETURNS INTEGER AS  $$
DECLARE
      R trinetdb.pcs_state%ROWTYPE;
      v_istat INTEGER := 0;
BEGIN
      FOR R in SELECT * FROM pcs.getNonNullResults(p_group, p_source) LOOP
       v_istat := v_istat + pcs.transition(R.controlGroup,R.sourceTable,R.id,R.state,R.rank,R.result);
    END LOOP;
    RETURN v_istat;
END 
$$ LANGUAGE plpgsql;

  --  
  -- get all rows with an non-zero result code
DROP FUNCTION IF EXISTS pcs.transition(p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE, p_state pcs_state.state%TYPE);
CREATE OR REPLACE  FUNCTION pcs.transition(p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE, p_state pcs_state.state%TYPE) RETURNS INTEGER AS  $$
DECLARE
      R trinetdb.pcs_state%ROWTYPE;
      v_istat INTEGER := 0;
BEGIN
    FOR R in SELECT * FROM pcs.getNonNullResults(p_group, p_source, p_state) LOOP
       v_istat := v_istat + pcs.transition(R.controlGroup,R.sourceTable,R.id,R.state,R.rank,R.result);
    END LOOP;
    RETURN v_istat;
END 
$$ LANGUAGE plpgsql;

  --
  -- get all rows with an non-zero result code
DROP FUNCTION IF EXISTS pcs.transition(p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE, p_state pcs_state.state%TYPE, p_id pcs_state.id%TYPE);
CREATE OR REPLACE  FUNCTION pcs.transition(p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE, p_state pcs_state.state%TYPE, p_id pcs_state.id%TYPE) RETURNS INTEGER AS $$
DECLARE
      R trinetdb.pcs_state%ROWTYPE;
      v_istat INTEGER := 0;
BEGIN
    -- RAISE NOTICE USING MESSAGE = 'in pcs.transition(group,source,state,id) ' || p_group || p_source || p_state || p_id;
    FOR R in SELECT * FROM pcs.getNonNullResults(p_group, p_source, p_state, p_id) LOOP
       -- RAISE NOTICE USING MESSAGE = 'begin- ' || R.controlgroup || R.sourcetable || R.id || R.state || R.rank || R.result || '-end';
       v_istat := v_istat + pcs.transition(R.controlgroup,R.sourcetable,R.id,R.state,R.rank,R.result);
    END LOOP;
    RETURN v_istat;
END 
$$ LANGUAGE plpgsql;

--
--  As above but only transition the rows whose rank matches input rank.
--    FUNCTION transition(p_group VARCHAR, p_source VARCHAR, p_state VARCHAR, p_id INTEGER, p_rank INTEGER) RETURNS INTEGER AS
--    BEGIN
--    END transition;
--
--
DROP FUNCTION IF EXISTS pcs.put_trans(p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE, 
                       p_state pcs_state.state%TYPE, p_result pcs_state.result%TYPE,
                       p_groupNew pcs_state.controlgroup%TYPE, p_sourceNew pcs_state.sourcetable%TYPE, 
                       p_stateNew pcs_state.state%TYPE, p_rankNew pcs_state.rank%TYPE,
                       p_resultNew pcs_state.result%TYPE);
CREATE OR REPLACE  FUNCTION pcs.put_trans(p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE, 
                       p_state pcs_state.state%TYPE, p_result pcs_state.result%TYPE,
                       p_groupNew pcs_state.controlgroup%TYPE, p_sourceNew pcs_state.sourcetable%TYPE, 
                       p_stateNew pcs_state.state%TYPE, p_rankNew pcs_state.rank%TYPE,
                       p_resultNew pcs_state.result%TYPE) RETURNS INTEGER AS $$
DECLARE
      v_rowcount INTEGER := 0;
BEGIN
    --
    INSERT INTO trinetdb.pcs_transition (groupold,sourceold,stateold,resultold,groupnew,sourcenew,statenew,ranknew,resultnew,subsource)
       VALUES (p_group, p_source, p_state, p_result, p_groupNew, p_sourceNew, p_stateNew, p_rankNew, p_resultNew, current_database()::VARCHAR(8));
    GET DIAGNOSTICS v_rowcount = ROW_COUNT;
    --
    RETURN v_rowcount;
END 
$$ LANGUAGE plpgsql;
--
--
DROP FUNCTION IF EXISTS pcs.del_trans(p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE, p_state pcs_state.state%TYPE, p_result pcs_state.result%TYPE,
                     p_groupNew pcs_state.controlgroup%TYPE, p_sourceNew pcs_state.sourcetable%TYPE,
                     p_stateNew pcs_state.state%TYPE, p_resultNew pcs_state.result%TYPE);
CREATE OR REPLACE  FUNCTION pcs.del_trans(p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE, p_state pcs_state.state%TYPE, p_result pcs_state.result%TYPE,
                     p_groupNew pcs_state.controlgroup%TYPE DEFAULT NULL, p_sourceNew pcs_state.sourcetable%TYPE DEFAULT NULL,
                     p_stateNew pcs_state.state%TYPE DEFAULT NULL, p_resultNew pcs_state.result%TYPE DEFAULT NULL) RETURNS INTEGER AS $$
DECLARE
      v_rowcount INTEGER := 0;
BEGIN
    IF (p_groupNew IS NULL) THEN
      -- delete all old transitions
      DELETE FROM trinetdb.pcs_transition WHERE
        GROUPOLD  = p_group AND
        SOURCEOLD = p_source AND
        STATEOLD  = p_state AND
        RESULTOLD = p_result;
      --
    ELSIF (p_sourceNew IS NULL) THEN
      -- delete this old transtion for new group
      DELETE FROM trinetdb.pcs_transition WHERE
        GROUPOLD  = p_group AND
        SOURCEOLD = p_source AND
        STATEOLD  = p_state AND
        RESULTOLD = p_result AND
        --
        GROUPNEW  = p_groupNew;
        --
    ELSIF (p_stateNew IS NULL) THEN
      -- delete this old transtion for new group and source
      DELETE FROM trinetdb.pcs_transition WHERE
        GROUPOLD  = p_group AND
        SOURCEOLD = p_source AND
        STATEOLD  = p_state AND
        RESULTOLD = p_result AND
        --
        GROUPNEW  = p_groupNew AND
        SOURCENEW = p_sourceNew;
        --
    ELSIF (p_resultNew IS NULL) THEN
      -- delete this old transtion for new group, source, and state
      DELETE FROM trinetdb.pcs_transition WHERE
        GROUPOLD  = p_group AND
        SOURCEOLD = p_source AND
        STATEOLD  = p_state AND
        RESULTOLD = p_result AND
        --
        GROUPNEW  = p_groupNew AND
        SOURCENEW = p_sourceNew AND
        STATENEW  = p_stateNew;
        --
    ELSE
      -- delete this old transtion for new group, source, state, and result
      DELETE FROM trinetdb.pcs_transition WHERE
        GROUPOLD  = p_group AND
        SOURCEOLD = p_source AND
        STATEOLD  = p_state AND
        RESULTOLD = p_result AND
        --
        GROUPNEW  = p_groupNew AND
        SOURCENEW = p_sourceNew AND
        STATENEW  = p_stateNew AND
        RESULTNEW = p_resultNew;
        --
    END IF;
    --
    GET DIAGNOSTICS v_rowcount = ROW_COUNT;
    RETURN v_rowcount;
END 
$$ LANGUAGE plpgsql;
-- -----------------------------------------------------
DROP FUNCTION IF EXISTS pcs.next_id(p_group pcs_state.controlgroup%TYPE,
                   p_source pcs_state.sourcetable%TYPE,
                   p_state pcs_state.state%TYPE,
                   p_delay DOUBLE PRECISION);
CREATE OR REPLACE FUNCTION pcs.next_id(p_group pcs_state.controlgroup%TYPE,
                   p_source pcs_state.sourcetable%TYPE,
                   p_state pcs_state.state%TYPE,
                   p_delay INTEGER) RETURNS BIGINT AS $$
  -- this function returns the highest ranked, and if multiple, first added, 
  -- un-resulted (result=0 or NULL) id from the PCS_STATE table.
  --
  DECLARE
     v_rec RECORD; 
     v_id pcs_state.id%TYPE := 0;
  --
  BEGIN
      FOR v_rec IN
         SELECT ps.id FROM trinetdb.pcs_state ps
         WHERE ps.controlGroup = p_group AND ps.sourceTable = p_source AND ps.state = p_state
         AND (ps.result = 0 OR ps.result IS NULL)
         AND ps.rank = (SELECT MAX(rank) FROM trinetdb.pcs_state WHERE id = ps.id)
         AND ps.lddate <= ( SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) - TO_CHAR(p_delay,'"''"FM9999999 "seconds''"')::INTERVAL)
         ORDER BY ps.lddate, ps.id DESC LOOP
         v_id := v_rec.id;
      END LOOP;
     -- return the last id from this query
     RETURN v_id; 
  --
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 0;
    WHEN OTHERS THEN
      RAISE NOTICE USING MESSAGE='PostgreSQL Error: ' || SQLSTATE || ' ' || SQLERRM;
      RETURN -1;
  END;
$$ LANGUAGE plpgsql;
--
--
DROP FUNCTION IF EXISTS pcs.next_id(p_group pcs_state.controlgroup%TYPE,
                   p_source pcs_state.sourcetable%TYPE,
                   p_state pcs_state.state%TYPE);
CREATE OR REPLACE FUNCTION pcs.next_id(p_group pcs_state.controlgroup%TYPE,
                   p_source pcs_state.sourcetable%TYPE,
                   p_state pcs_state.state%TYPE) RETURNS BIGINT AS $$
  BEGIN
      RETURN pcs.next_id(p_group, p_source, p_state, 0);
  END;
$$ LANGUAGE plpgsql;
--
--
DROP FUNCTION IF EXISTS pcs.getNext(p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE, p_state pcs_state.state%TYPE);
CREATE OR REPLACE FUNCTION pcs.getNext(p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE, p_state pcs_state.state%TYPE)  RETURNS BIGINT AS $$
  BEGIN
    RETURN pcs.next_id(p_group, p_source, p_state, 0);
  END;
$$ LANGUAGE plpgsql;
  --
-- Renate's here --------------------------------------
---- Renate: I don't think there  is a need for this function
--  -- Id is posted for some state (result status undetermined). */
----CREATE OR REPLACE FUNCTION pcs.post_ids(p_ids   ID_TABLE,
----                    p_group VARCHAR,
----                    p_source VARCHAR,
----                    p_state VARCHAR,
----                    p_rank NUMERIC) RETURNS INTEGER AS $$
----  DECLARE
----    v_istat INTEGER := 0;
----  BEGIN
----    FOR i IN p_ids.FIRST..p_ids.LAST LOOP
----      v_istat := v_istat + post_id(p_group, p_source, p_ids(i), p_state, p_rank, 0, 0); -- no commit
----    END LOOP;
----    -- Commit also releases any row locks held by "FOR UPDATE" query
----    IF (v_istat > 0) THEN
----      COMMIT;
----    END IF;
----    RETURN v_istat;
----  --
----  EXCEPTION
----    WHEN OTHERS THEN
----      DBMS_OUTPUT.PUT_LINE('Oracle Error: ' || TO_CHAR(SQLCODE) || ' ' || SUBSTR(SQLERRM, 1, 100));
----      RETURN -1;
----  END post_ids;
----
----
--CREATE OR REPLACE FUNCTION bulk_post(p_ids   ID_TABLE,
--                    p_group VARCHAR,
--                    p_source VARCHAR,
--                    p_state VARCHAR,
--                    p_rank NUMERIC) RETURNS INTEGER AS $$
--  DECLARE
--    v_istat INTEGER := 0;
--  BEGIN
--    --
--    FORALL i IN p_ids.FIRST..p_ids.LAST 
--        INSERT INTO trinetdb.pcs_state (controlgroup,sourcetable,id,state,rank,result,subsource) VALUES
--        (p_group,p_source,p_ids(i),p_state,p_rank,0,current_database());
--    --
--    v_istat := SQL%ROWCOUNT;
--    --
--    IF (v_istat > 0) THEN
--      COMMIT;
--    END IF;
--    --
--    RETURN v_istat;
--    --
--  EXCEPTION
--    WHEN OTHERS THEN
--      DBMS_OUTPUT.PUT_LINE('Oracle Error: ' || TO_CHAR(SQLCODE) || ' ' || SUBSTR(SQLERRM, 1, 100));
--      RETURN -1;
--  END bulk_post;
----
--  FUNCTION bulk_post_id_table( p_table_name VARCHAR,
--                          p_group VARCHAR,
--                          p_source VARCHAR,
--                          p_state VARCHAR,
--                          p_rank NUMERIC) RETURNS INTEGER AS
--  BEGIN
--      RETURN bulk_post_id_table( p_table_name, 'id', p_group, p_source, p_state, p_rank);
--  END bulk_post_id_table;
----
----
--  FUNCTION bulk_post_id_table( p_table_name VARCHAR,
--                          p_table_column VARCHAR,
--                          p_group VARCHAR,
--                          p_source VARCHAR,
--                          p_state VARCHAR,
--                          p_rank NUMERIC) RETURNS INTEGER AS
--    v_istat INTEGER := 0;
--    v_ids ID_TABLE := NULL;
--    --
--    -- TYPE REFCUR IS REF CURSOR;
--    -- c REFCUR;
--    --
--  BEGIN
--    -- table must have a column for containing id(s)
--    EXECUTE IMMEDIATE 'SELECT ' || p_table_column || ' FROM ' || p_table_name BULK COLLECT INTO v_ids;
--    -- OPEN c FOR 'SELECT ' || p_table_column ' FROM ' || p_table_name;
--    -- FETCH c BULK COLLECT INTO v_ids;
--    -- CLOSE c;
--    --
--    -- if we have ids in table then post them for state
--    IF (v_ids IS NOT NULL AND v_ids.FIRST IS NOT NULL) THEN
--      FORALL i IN v_ids.FIRST..v_ids.LAST 
--        INSERT INTO trinetdb.pcs_state (controlgroup,sourcetable,id,state,rank,result,subsource) VALUES
--          (p_group,p_source,v_ids(i),p_state,p_rank,0,current_database());
--      v_istat := SQL%ROWCOUNT;
--      --
--      -- delete just posted ids from table
--      --FORALL i IN v_ids.FIRST..v_ids.LAST 
--      --  DELETE FROM ids2post WHERE id  = v_ids(i);
--      --
--      IF (v_istat > 0) THEN
--        COMMIT;
--      END IF;
--    END IF;
--    --
--    RETURN v_istat;
--    --
--  EXCEPTION
--    WHEN OTHERS THEN
--      DBMS_OUTPUT.PUT_LINE('Oracle Error: ' || TO_CHAR(SQLCODE) || ' ' || SUBSTR(SQLERRM, 1, 100));
--      RETURN -1;
--  END bulk_post_id_table;
----
----
-- RENATE HERE WITH TYPE CHANGES!
-- function has to return 1 if successful, 2 if already resulted.
DROP FUNCTION IF EXISTS pcs.result_id(p_group pcs_state.controlgroup%TYPE,
                     p_source pcs_state.sourcetable%TYPE,
                     p_id pcs_state.id%TYPE,
                     p_state pcs_state.state%TYPE,
                     p_rank pcs_state.rank%TYPE,
                     p_result pcs_state.result%TYPE);
CREATE OR REPLACE FUNCTION pcs.result_id(p_group pcs_state.controlgroup%TYPE,
                     p_source pcs_state.sourcetable%TYPE,
                     p_id pcs_state.id%TYPE,
                     p_state pcs_state.state%TYPE,
                     p_rank pcs_state.rank%TYPE,
                     p_result pcs_state.result%TYPE) RETURNS INTEGER AS $$
----------------------------------------------------------------------------
--- pcs.result_id(p_group, p_source, p_id, p_state, p_rank, p_result)
--- Update the result value of the matching PCS_STATE row if its current result value is not the same. 
--- If p_rank is NULL, then update the result value of all the rows for this state and id.
--- If p_rank < 0, then only update the row with the highest rank for this state and id.
--- else: update all the rows of matching rank, state, and id.
--- Returns 0 if no matching rows were found to update.
--- Returns 1 is at least one row was updated.
--- Returns 2 if all the matching row already had the same result value (none updated).
--- Returns -1 if an error occurred.
----------------------------------------------------------------------------
  DECLARE
    curs REFCURSOR;
    v_istat INTEGER := 0;
    v_result pcs_state.result%TYPE := NULL;
    v_rowcount INTEGER := 0;
    R pcs_state%ROWTYPE;
    --v_sr_tab state_row_rec_table := NULL;
--
  BEGIN
    -- query attached to cursor depends on input parameter p_rank
    IF (p_rank IS NULL) THEN -- All rows for same input id 
      OPEN curs FOR
      SELECT sr.controlGroup, sr.sourceTable, sr.id, sr.state, sr.rank, sr.result,
        sr.secondaryid, sr.secondarysourcetable, sr.auth, sr.subsource, sr.lddate
        FROM trinetdb.pcs_state sr 
        WHERE sr.controlgroup = p_group AND sr.sourcetable = p_source AND sr.state = p_state AND sr.id = p_id;
      --
    ELSIF (p_rank < 0) THEN -- Highest rank for input id
      OPEN curs FOR
      SELECT sr.controlGroup, sr.sourceTable, sr.id, sr.state, sr.rank, sr.result,
        sr.secondaryid, sr.secondarysourcetable, sr.auth, sr.subsource, sr.lddate
        FROM trinetdb.pcs_state sr 
        WHERE sr.controlgroup = p_group AND sr.sourcetable = p_source AND sr.state = p_state AND sr.id = p_id ORDER BY sr.rank DESC;
      --
    ELSE -- matching rank for input id
      OPEN curs FOR
      SELECT sr.controlGroup, sr.sourceTable, sr.id, sr.state, sr.rank, sr.result,
        sr.secondaryid, sr.secondarysourcetable, sr.auth, sr.subsource, sr.lddate
        FROM trinetdb.pcs_state sr 
        WHERE sr.controlgroup = p_group AND sr.sourcetable = p_source AND sr.state = p_state AND sr.id = p_id AND sr.rank = p_rank;
      --
    END IF;
    --
    -- At first v_istat = 0
    LOOP
      FETCH curs into R;
      -- exit the loop if no more records in curs
      EXIT WHEN NOT FOUND;

      v_result := R.result;
      IF (v_result IS NOT NULL AND v_result = p_result AND v_istat = 0) THEN -- don't change if already updated a row i.e. v_istat>0
        v_istat := 2; -- used to be 1, before 2010/06/01 aww
      --
      ELSE
        UPDATE trinetdb.pcs_state SET result = coalesce(p_result, 0) WHERE 
         controlgroup = p_group AND sourcetable = p_source AND state = p_state AND id = p_id AND rank = R.rank;
         -- AND (result IS NULL OR result != p_result);
         --
        GET DIAGNOSTICS v_rowcount = ROW_COUNT;
        IF (v_rowcount > 0) THEN
          v_istat := 1; -- at least one row updated
        END IF;
        --
      END IF;
      -- One iteration, highest rank
      EXIT WHEN (p_rank < 0);
    END LOOP;
    CLOSE curs;
    --
    IF (v_istat = 1) THEN
      --RAISE NOTICE 'OK to commit';
      --COMMIT;
    END IF;
    --
    RETURN v_istat; -- 1 if any row updated, 2 if all matching rows already have same result
  --
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 0;
    WHEN OTHERS THEN
      RAISE NOTICE USING MESSAGE='PostgreSQL Error: ' || SQLSTATE || ' ' || SQLERRM;
      RETURN -1;
  END;
$$ LANGUAGE plpgsql;
--
--
-- All ranks for input id
DROP FUNCTION IF EXISTS pcs.result_id_all(p_group pcs_state.controlgroup%TYPE,
                  p_source pcs_state.sourcetable%TYPE,
                  p_id pcs_state.id%TYPE,
                  p_state pcs_state.state%TYPE,
                  p_result pcs_state.result%TYPE);
CREATE OR REPLACE FUNCTION pcs.result_id_all(p_group pcs_state.controlgroup%TYPE,
                  p_source pcs_state.sourcetable%TYPE,
                  p_id pcs_state.id%TYPE,
                  p_state pcs_state.state%TYPE,
                  p_result pcs_state.result%TYPE) RETURNS INTEGER AS $$
----------------------------------------------------------------------------
--- pcs.result_id_all(p_group, p_source, p_id, p_state, p_result)
--- Update the result value of the matching PCS_STATE row if its current result value is not the same. 
--- Updates the result value of all the rows for this state and id.
--- Returns 0 if no matching rows were found to update.
--- Returns 1 is at least one row was updated.
--- Returns 2 if all the matching row already had the same result value (none updated).
--- Returns -1 if an error occurred.
----------------------------------------------------------------------------
  BEGIN
      RETURN pcs.result_id(p_group, p_source, p_id, p_state, NULL, p_result);
  END;
$$ LANGUAGE plpgsql;

--
-- Highest ranking for input id
DROP FUNCTION IF EXISTS pcs.result_id(p_group pcs_state.controlgroup%TYPE,
                     p_source pcs_state.sourcetable%TYPE,
                     p_id pcs_state.id%TYPE,
                     p_state pcs_state.state%TYPE,
                     p_result pcs_state.result%TYPE);
CREATE OR REPLACE FUNCTION pcs.result_id(p_group pcs_state.controlgroup%TYPE,
                     p_source pcs_state.sourcetable%TYPE,
                     p_id pcs_state.id%TYPE,
                     p_state pcs_state.state%TYPE,
                     p_result pcs_state.result%TYPE) RETURNS INTEGER AS $$
--
----------------------------------------------------------------------------
--- pcs.result_id(p_group, p_source, p_id, p_state, p_result)
--- Update the result value of the matching PCS_STATE row if its current result value is not the same. 
--- Only updates the row with the highest rank for this state and id.
--- Returns 0 if no matching rows were found to update.
--- Returns 1 is at least one row was updated.
--- Returns 2 if all the matching row already had the same result value (none updated).
--- Returns -1 if an error occurred.
----------------------------------------------------------------------------
  BEGIN
    RETURN pcs.result_id(p_group, p_source, p_id, p_state, -1, p_result);
  END;
$$ LANGUAGE plpgsql;
--
-- Table "trinetdb.pcs_signal"
--   Column   |            Type             | Modifiers 
--------------+-----------------------------+-----------
-- sigid      | bigint                      | not null
-- pcs_group  | character varying(20)       | not null
-- pcs_thread | character varying(20)       | not null
-- pcs_state  | character varying(20)       | not null
-- algorithm  | character varying(16)       | not null
-- dbname     | character varying(9)        | not null
-- dbuser     | character varying(30)       | not null
-- userhost   | character varying(16)       | not null
-- username   | character varying(20)       | not null
-- startdate  | timestamp without time zone | not null
-- stopdate   | timestamp without time zone | 
-- message    | character varying(132)      | 
--Indexes:
--    "pk_sig" PRIMARY KEY, btree (sigid)

DROP FUNCTION IF EXISTS pcs.signal_start(p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE, p_state pcs_state.state%TYPE,
    p_algorithm pcs_signal.algorithm%TYPE, p_userhost pcs_signal.userhost%TYPE, p_username pcs_signal.username%TYPE);
CREATE OR REPLACE FUNCTION pcs.signal_start(p_group pcs_state.controlgroup%TYPE, p_source pcs_state.sourcetable%TYPE, p_state pcs_state.state%TYPE,
    p_algorithm pcs_signal.algorithm%TYPE, p_userhost pcs_signal.userhost%TYPE, p_username pcs_signal.username%TYPE) RETURNS BIGINT AS $$
--
  DECLARE
    v_id pcs_signal.sigid%TYPE := 0;
    v_dbuser pcs_signal.dbuser%TYPE := 'UNKNOWN';
    v_status INTEGER := 0;
--
BEGIN
    SELECT user INTO v_dbuser;
    SELECT nextval('sigseq') INTO v_id;
    --RAISE NOTICE '% %', v_dbuser, v_id;
--
    INSERT INTO trinetdb.pcs_signal
      VALUES (v_id, p_group, p_source, p_state, p_algorithm, current_database(), v_dbuser,
          substring(p_userhost for 16), p_username, SYS_EXTRACT_UTC(CURRENT_TIMESTAMP), NULL, 'STARTED');
--
    GET DIAGNOSTICS v_status = ROW_COUNT;
    IF (v_status = 1) THEN
      -- committing inside a transaction causes this error:
      --NOTICE:  PostgreSQL: 0A000 cannot begin/end transactions in PL/pgSQL
      --COMMIT;
    END IF;
--
    RETURN v_id;
--
EXCEPTION
    WHEN OTHERS THEN
      RAISE NOTICE USING MESSAGE='PostgreSQL: ' || SQLSTATE || ' ' || SQLERRM;
      RETURN -1;
END;
$$ LANGUAGE plpgsql;
--
--
DROP FUNCTION IF EXISTS pcs.signal_stop(p_id pcs_signal.sigid%TYPE);
CREATE OR REPLACE FUNCTION pcs.signal_stop(p_id pcs_signal.sigid%TYPE) RETURNS INTEGER AS $$
  BEGIN
  --
    RETURN pcs.signal_stop(p_id, 'STOPPED');
  --
  END;
$$ LANGUAGE plpgsql;
--
DROP FUNCTION IF EXISTS pcs.signal_stop(p_id pcs_signal.sigid%TYPE, p_message pcs_signal.message%TYPE);
CREATE OR REPLACE FUNCTION pcs.signal_stop(p_id pcs_signal.sigid%TYPE, p_message pcs_signal.message%TYPE) RETURNS INTEGER AS $$
--
  DECLARE
    v_istat INTEGER := 0;
--
  BEGIN
    UPDATE trinetdb.pcs_signal set message = p_message, stopdate = SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) WHERE sigid = p_id; 
--
    GET DIAGNOSTICS v_istat = ROW_COUNT;
    IF (v_istat = 1) THEN
      --COMMIT;
    END IF;
--
    RETURN v_istat;
--
  EXCEPTION
    WHEN OTHERS THEN
      RAISE NOTICE USING MESSAGE='PostgreSQL Error: ' || SQLSTATE || ' ' || SQLERRM;
      RETURN -1;
  END;
$$ LANGUAGE plpgsql;
--
--
DROP FUNCTION IF EXISTS pcs.has_stop_signal(p_id pcs_signal.sigid%TYPE);
CREATE OR REPLACE FUNCTION pcs.has_stop_signal(p_id pcs_signal.sigid%TYPE) RETURNS INTEGER AS $$
--
  DECLARE
     --v_stopped INTEGER := -1;
     v_stopped DOUBLE PRECISION := -1.;
--
  BEGIN
    -- RAISE NOTICE 'DEBUG: in has_stop_signal, before select';
    SELECT EXTRACT(HOUR from stopdate),
        CASE WHEN stopdate IS NULL THEN 0
        ELSE 1
        END
    INTO v_stopped FROM trinetdb.pcs_signal WHERE sigid = p_id; 

    RETURN CASE WHEN CAST(v_stopped AS INTEGER) > 0 THEN 1 ELSE 0 END;
--
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 0;
    WHEN OTHERS THEN
      RAISE NOTICE USING MESSAGE='PostgreSQL Error: ' || SQLSTATE || ' ' || SQLERRM;
      RETURN -1;
  END;
$$ LANGUAGE plpgsql;
--
--
DROP FUNCTION IF EXISTS pcs.run_time(p_id pcs_signal.sigid%TYPE);
CREATE OR REPLACE FUNCTION pcs.run_time(p_id pcs_signal.sigid%TYPE) RETURNS DOUBLE PRECISION AS $$
--
  DECLARE
    v_runtime DOUBLE PRECISION := 0.;
    v_date  TIMESTAMP := NULL;
--
  BEGIN
    --
    v_date := SYS_EXTRACT_UTC(CURRENT_TIMESTAMP); 
    --
    SELECT stopdate, startdate,
        CASE WHEN stopdate is NULL THEN (v_date - coalesce(startdate, v_date))
             ELSE (stopdate-startdate)
        END
        INTO v_runtime FROM trinetdb.pcs_signal WHERE sigid = p_id;
--  return time as hours
    RETURN (v_runtime * 24.);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 0;
    WHEN OTHERS THEN
      RAISE NOTICE USING MESSAGE='PostgreSQL Error: ' || SQLSTATE || ' ' || SQLERRM;
      RETURN 0.;
  END;
$$ LANGUAGE plpgsql;
-- 
-- 
DROP FUNCTION IF EXISTS pcs.signal_message(p_id pcs_signal.sigid%TYPE, p_message pcs_signal.message%TYPE);
CREATE OR REPLACE FUNCTION pcs.signal_message(p_id pcs_signal.sigid%TYPE, p_message pcs_signal.message%TYPE) RETURNS INTEGER AS $$
--
  DECLARE
    v_istat INTEGER := 0;
--
  BEGIN
    UPDATE trinetdb.pcs_signal set message = p_message WHERE sigid = p_id; 
--
    GET DIAGNOSTICS v_istat = ROW_COUNT;
    --IF (v_istat = 1) THEN
      --COMMIT;
    --END IF;
--
    RETURN v_istat;
--
  EXCEPTION
    WHEN OTHERS THEN
      RAISE NOTICE USING MESSAGE='PostgreSQL Error: ' || SQLSTATE || ' ' || SQLERRM;
      RETURN -1;
  END;
$$ LANGUAGE plpgsql;
-- 
--
DROP FUNCTION IF EXISTS pcs.delete_signal(p_id pcs_signal.sigid%TYPE);
CREATE OR REPLACE FUNCTION pcs.delete_signal(p_id pcs_signal.sigid%TYPE) RETURNS INTEGER AS $$
--
  DECLARE
    v_istat INTEGER := 0;
--
  BEGIN
    DELETE FROM trinetdb.pcs_signal WHERE sigid = p_id;
--
    GET DIAGNOSTICS v_istat = ROW_COUNT;
    IF (v_istat = 1) THEN
      --RAISE NOTICE 'OK to commit';
      --COMMIT;
    END IF;
--
    RETURN v_istat;
--
  EXCEPTION
    WHEN OTHERS THEN
      RAISE NOTICE USING MESSAGE='PostgreSQL Error: ' || SQLSTATE || ' ' || SQLERRM;
      RETURN -1;
  END;
$$ LANGUAGE plpgsql;
--TYPE transition_row_rec IS RECORD (
--    groupOld     VARCHAR(20),
--    sourceOld    VARCHAR(20),
--    stateOld     VARCHAR(20),
--    resultOld    NUMBER(15),
    --
--    groupNew     VARCHAR(20),
--    sourceNew    VARCHAR(20),
--    stateNew     VARCHAR(20),
--    rankNew      NUMBER(15),
--    resultNew    NUMBER(15),
    --
--    auth         VARCHAR(15),
--    subsource    VARCHAR(8),
--    lddate       DATE
--);
--
--TYPE transition_row_rec_table IS TABLE OF transition_row_rec; 
--
--TYPE state_row_rec IS RECORD (
--    controlgroup         VARCHAR(20),
--    sourcetable          VARCHAR(20),
--    id                   NUMBER(15),
--    state                VARCHAR(20),
--    rank                 NUMBER(15),
--    result               NUMBER(15),
--    secondaryid          NUMBER(15),
--    secondarysourcetable VARCHAR(20),
--    auth                 VARCHAR(15),
--    subsource            VARCHAR(8),
--    lddate               DATE
--);
--
--TYPE state_row_rec_table is TABLE OF state_row_rec; 
--
-- 
--                                Table "trinetdb.pcs_state"
--        Column        |            Type             |              Modifiers               
------------------------+-----------------------------+--------------------------------------
-- controlgroup         | character varying(20)       | not null
-- sourcetable          | character varying(20)       | not null
-- id                   | bigint                      | not null
-- state                | character varying(20)       | not null
-- rank                 | numeric                     | not null
-- result               | numeric                     | 
-- secondaryid          | bigint                      | 
-- secondarysourcetable | character varying(20)       | 
-- auth                 | character varying(15)       | 
-- subsource            | character varying(8)        | 
-- lddate               | timestamp without time zone | default timezone('UTC'::text, now())
--Indexes:
--    "pcs_state_pkey" PRIMARY KEY, btree (controlgroup, sourcetable, id, state)


