-- Run as user code
\i assoc_amp.sql
\i epref.sql
\i geo_region.sql
\i magpref.sql
\i match.sql
\i pcs.sql
\i quarry.sql
\i sequence.sql
\i truetime.sql
\i Init_Leap_Secs.sql
\i util.sql
\i wavefile.sql
\i wheres.sql
\i 2021_bestmag_unsetmagpref.sql
