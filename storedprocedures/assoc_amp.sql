--create schema if not exists assocamp authorization code;
--grant usage on schema assocamp to trinetdb_read, trinetdb_execute;

DROP FUNCTION IF EXISTS assocamp.getValidAmpSetId(p_evid AssocEvAmpset.evid%TYPE,
                            p_ampsettype AssocEvAmpset.ampsettype%TYPE,
                            p_subsrc AssocEvAmpset.subsource%TYPE
                          );

create or replace FUNCTION assocamp.getValidAmpSetId(p_evid AssocEvAmpset.evid%TYPE,
                            p_ampsettype AssocEvAmpset.ampsettype%TYPE, 
                            p_subsrc AssocEvAmpset.subsource%TYPE
                          ) RETURNS INTEGER AS $$
DECLARE
        v_ampsetid INTEGER := 0;
        -------------
        c CURSOR FOR SELECT a.ampsetid FROM trinetdb.assocevampset a WHERE
               (a.evid=p_evid) AND (a.ampsettype=p_ampsettype) AND (a.subsource LIKE p_subsrc) AND a.isvalid=1
               ORDER BY lddate DESC;
        ---------------
BEGIN
	OPEN c;
	FETCH c into v_ampsetid;
	if v_ampsetid IS NULL THEN
	   v_ampsetid = 0;
	END IF;
        --
        RETURN v_ampsetid;
        --
--    EXCEPTION WHEN OTHERS THEN
--        DBMS_OUTPUT.PUT_LINE('EXCEPTION assocamp.getValidAmpSetId('|| p_evid || ',' || p_ampsettype || ',' ||  p_subsrc || ')'
--            || ' ampsetid: ' || v_ampsetid || ' CODE: ' || TO_CHAR(SQLCODE));
--        DBMS_OUTPUT.PUT_LINE(SUBSTR(SQLERRM, 1, 130));
--        --------------
--        RETURN -1;
--        --------------
END
$$ LANGUAGE plpgsql;

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS assocamp.getValidAmpSetId(p_evid AssocEvAmpset.evid%TYPE,
                            p_ampsettype AssocEvAmpset.ampsettype%TYPE,
                            p_subsrc AssocEvAmpset.subsource%TYPE,
                            p_commit INTEGER
                          );

create or replace    FUNCTION assocamp.getValidAmpSetId(p_evid AssocEvAmpset.evid%TYPE,
                            p_ampsettype AssocEvAmpset.ampsettype%TYPE, 
                            p_subsrc AssocEvAmpset.subsource%TYPE,
                            p_commit INTEGER
                          ) RETURNS INTEGER AS $$
DECLARE
        v_ampsetid INTEGER := 0;
BEGIN
        -------------
        select assocamp.getValidAmpSetId(p_evid, p_ampsettype, p_subsrc) into v_ampsetid;
        -------------
        IF (v_ampsetid = 0) THEN  -- don't insert if error value < 0
            select assocamp.insertValidAmpSet(p_evid, p_ampsettype, p_subsrc, p_commit) into v_ampsetid;
        END IF;
        --------------
        RETURN v_ampsetid;
END
$$ LANGUAGE plpgsql;
-- --------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS assocamp.insertValidAmpSet(p_evid AssocEvAmpset.evid%TYPE,
                               p_ampsettype AssocEvAmpset.ampsettype%TYPE,
                               p_subsrc AssocEvAmpset.subsource%TYPE,
                               p_commit INTEGER
                              );
-- --------------------------------------------------------------------------------
create or replace    FUNCTION assocamp.insertValidAmpSet(p_evid AssocEvAmpset.evid%TYPE,
                               p_ampsettype AssocEvAmpset.ampsettype%TYPE, 
                               p_subsrc AssocEvAmpset.subsource%TYPE,
                               p_commit INTEGER
                              ) RETURNS INTEGER AS $$

DECLARE
        v_istat INTEGER := 0;
        v_ampsetid INTEGER := 0;
        v_evid Event.evid%TYPE := 0;
        v_ampsettype AssocEvAmpset.ampsettype%TYPE := NULL; 
        v_subsrc AssocEvAmpset.subsource%TYPE := NULL;
BEGIN
        SELECT 1 INTO v_istat FROM trinetdb.AMPSETTYPES WHERE ampsettype = p_ampsettype;
	IF v_istat IS NULL THEN
               RETURN -2;
 	END IF;
        --------------
        IF (p_subsrc IS NULL) THEN
            RETURN -3;
        END IF;
        --------------
        select NEXTVAL('AMPSETSEQ') into v_ampsetid;
        INSERT INTO ASSOCEVAMPSET (AMPSETID, AMPSETTYPE, EVID, SUBSOURCE, ISVALID)
               VALUES (v_ampsetid, p_ampsettype, p_evid, p_subsrc, 1);
        --------------
        IF (p_commit > 0) THEN
            --COMMIT; not allowed in postgresql
        END IF;
        --------------
        RETURN v_ampsetid;
        --------------
END 
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS assocamp.MoveAndAssoc(
              p_ampid Amp.ampid%TYPE,
              p_evid Event.evid%TYPE,
              p_orid Origin.orid%TYPE,
              p_subsrc Amp.subsource%TYPE,
              p_ampsettype AssocEvAmpset.ampsettype%TYPE,
              p_dist AssocAmo.delta%TYPE,
              p_az AssocAmo.seaz%TYPE,
              p_commit INTEGER
            );
-------------------------------------------------------------------------------------------------
    -- Associate Amp once the correct EVID is determined
    -- This means:
    --  1) Create new ampset if no valid set exists for specified evid, ampsettype, and subsource
    --  2) Copy amp row FROM UnassocAmp table to Amp table
    --  3) Insert Ampset row 
    --  4) Insert AssocAmO row using input orid
    --  5) Delete old UnassocAmp
    --  6) Commit the changes if p_commit > 0, else no commit
    -- Returns ampsetid on success, return <= 0 for bad input or SQL error.
CREATE OR REPLACE FUNCTION assocamp.MoveAndAssoc(
              p_ampid Amp.ampid%TYPE,
              p_evid Event.evid%TYPE,
              p_orid Origin.orid%TYPE,
              p_subsrc Amp.subsource%TYPE,
              p_ampsettype AssocEvAmpset.ampsettype%TYPE,
              p_dist AssocAmo.delta%TYPE,
              p_az AssocAmo.seaz%TYPE,
              p_commit INTEGER
            )  RETURNS INTEGER AS $$
  DECLARE
    v_istat INTEGER := 0;
    v_ampsetid INTEGER := 0;
    v_arow trinetdb.unassocamp%ROWTYPE;
  BEGIN
      -- check to see if there is an entry in the assocevampset table for this evid and p_ampsettype (must be valid type)
      -- If not, create one. return 0 if there is a problem creating an entry.
      IF (p_ampsettype IS NOT NULL) THEN
          BEGIN
              SELECT 1 INTO v_istat FROM trinetdb.ampsettypes WHERE ampsettype = p_ampsettype;
          EXCEPTION WHEN NO_DATA_FOUND THEN
              -- not a valid ampsettype
              RETURN -2;
          END;

          -- check if there is already an ampsetid for this event and ampsettype
          SELECT ampsetid INTO v_ampsetid FROM trinetdb.assocevampset WHERE evid=p_evid
          AND ampsettype=p_ampsettype AND subsource=p_subsrc AND isvalid=1 
          ORDER BY lddate DESC LIMIT 1;

          -- create new ampset if there is no valid ampset for this evid and type yet
          IF (v_ampsetid IS NULL OR v_ampsetid = 0) THEN
              SELECT nextval('AMPSETSEQ') INTO v_ampsetid;
              INSERT INTO trinetdb.assocevampset (ampsetid, ampsettype, evid, subsource, isvalid) VALUES
              (v_ampsetid, p_ampsettype, p_evid, p_subsrc, 1);
              GET DIAGNOSTICS v_istat = ROW_COUNT;
          END IF;

          -- if we still don't have an v_ampsetid or  v_istat = 0 something went wrong
          IF (v_ampsetid IS NULL OR v_ampsetid = 0 OR v_istat = 0) THEN
              RAISE NOTICE USING MESSAGE='assocamp.MoveAndAssoc insert assocevampset return status: ' || v_istat;
              RETURN 0;
          END IF;
      END IF;

      -- If we got this far, an ampset exists of this type for this event in the assocevampset table.
      -- Copy the amps from the unassocamp table and insert them into the amp table. Keep the ampid the same
      -- and also keep the lddate the same. Ampid is assumed to have been obtained using the ampseq.

      SELECT * INTO v_arow FROM trinetdb.unassocamp WHERE ampid = p_ampid;
      INSERT INTO trinetdb.amp (AMPID,COMMID,DATETIME,STA,NET,AUTH,SUBSOURCE,CHANNEL,CHANNELSRC,SEEDCHAN,LOCATION,
        IPHASE,AMPLITUDE,AMPTYPE,UNITS,AMPMEAS,ERAMP,FLAGAMP,PER,SNR,TAU,QUALITY,RFLAG,
        CFLAG,WSTART,DURATION, LDDATE) VALUES
        (v_arow.AMPID,v_arow.COMMID,v_arow.DATETIME,v_arow.STA,v_arow.NET,v_arow.AUTH,v_arow.SUBSOURCE,v_arow.CHANNEL,
        v_arow.CHANNELSRC,v_arow.SEEDCHAN,v_arow.LOCATION, v_arow.IPHASE,v_arow.AMPLITUDE,v_arow.AMPTYPE,v_arow.UNITS,
        v_arow.AMPMEAS,v_arow.ERAMP,v_arow.FLAGAMP,v_arow.PER,v_arow.SNR,v_arow.TAU,v_arow.QUALITY,v_arow.RFLAG,
        v_arow.CFLAG,v_arow.WSTART,v_arow.DURATION,v_arow.LDDATE);

      GET DIAGNOSTICS v_istat = ROW_COUNT;
      RAISE NOTICE USING MESSAGE='assocamp.MoveAndAssoc added ampid to amp, return status: ' || v_istat;

      -- add this ampid to the ampset table
      IF (v_istat > 0 AND v_ampsetid > 0) THEN
          INSERT INTO trinetdb.ampset (ampsetid,ampid) VALUES (v_ampsetid, p_ampid);
      END IF;
      GET DIAGNOSTICS v_istat = ROW_COUNT;
      RAISE NOTICE USING MESSAGE='assocamp.MoveAndAssoc added amp to ampset, return status: ' || v_istat;
  
      -- add this ampid to the assocamo table
      INSERT INTO trinetdb.AssocAmO (ORID, AMPID, AUTH, SUBSOURCE, RFLAG, DELTA, SEAZ) 
      VALUES (p_orid, p_ampid, v_arow.AUTH, v_arow.SUBSOURCE, v_arow.RFLAG, p_dist, p_az);
      GET DIAGNOSTICS v_istat = ROW_COUNT;
      RAISE NOTICE USING MESSAGE='assocamp.MoveAndAssoc added amp to assocamo, return status: ' || v_istat;

      -- now delete the no-longer unassociated amp
      DELETE FROM trinetdb.unassocamp WHERE ampid=v_arow.ampid;
      GET DIAGNOSTICS v_istat = ROW_COUNT;
      RAISE NOTICE USING MESSAGE='assocamp.MoveAndAssoc deleted amp from unassocamp, return status: ' || v_istat;
      
      IF (p_commit > 0) THEN
         RAISE NOTICE USING MESSAGE='Info: cannot commit from within plpgsql function, p_commit does nothing';
      END IF;

      RETURN v_ampsetid;
  
  EXCEPTION
    WHEN OTHERS THEN
      RAISE NOTICE USING MESSAGE='assocamp.MoveAndAssoc Error: ' || SQLSTATE || ' ' || SQLERRM;
      RETURN -1;

  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS assocamp.MoveAndAssocAmp(
              p_ampid Amp.ampid%TYPE,
              p_evid Event.evid%TYPE,
              p_orid Origin.orid%TYPE,
              p_subsrc Amp.subsource%TYPE,
              p_ampsettype AssocEvAmpset.ampsettype%TYPE,
              p_dist AssocAmo.delta%TYPE,
              p_az AssocAmo.seaz%TYPE
            );
-- --------------------------------------------------------------------------------
    -- Wrapper around previous function to allow "executeBatch" call in JDBC PreparedStatement.
    -- Batching works for procedures with inputs only, no outputs, like functions returning values.
    -- Does not commit, user must do that add batching to save changes.
CREATE OR REPLACE FUNCTION assocamp.MoveAndAssocAmp(
              p_ampid Amp.ampid%TYPE,
              p_evid Event.evid%TYPE,
              p_orid Origin.orid%TYPE,
              p_subsrc Amp.subsource%TYPE,
              p_ampsettype AssocEvAmpset.ampsettype%TYPE,
              p_dist AssocAmo.delta%TYPE,
              p_az AssocAmo.seaz%TYPE
            )  RETURNS VOID AS $$
  DECLARE
    v_istat INTEGER := 0;
  BEGIN
    v_istat := assocamp.MoveAndAssoc( p_ampid, p_evid, p_orid, p_subsrc, p_ampsettype,
                  p_dist, p_az, 0::INTEGER);
  END;
$$ LANGUAGE plpgsql;
