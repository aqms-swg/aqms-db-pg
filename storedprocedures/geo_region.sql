/**
version: 1.0.1 2020-07-10
 
used in production: yes, utc version
 
function/procedures for testing whether geographic point lies
inside or outside a polygonal region specified by lat,lon coordinates.
 
dependent applications:

- java org.trinet.jasi.tn.solutiontn using eventselectionproperties
 
depends on:

      - refs gazetteer_region, gazetteerpt tables objects 
      - oracle user defined datatype latlon, coordinates in public schema (synonym ok)
 
example: 
```
select evid, orid, lat,lon from event,origin
where orid=prefor and datetime > truetime.putepoch(truetime.string2true('2004/01/30 00:00:00'),'utc')
and geo_region.inside_border('airportlake', lat, lon);
```
*/

/**
get version number
*/
CREATE OR REPLACE FUNCTION geo_region.getPkgId() 
RETURNS varchar(80) AS $$
DECLARE
pkgId varchar(80) := 'geo_region 1.0.1 2020-07-10';
BEGIN
    RETURN pkgId;
END;
$$ LANGUAGE plpgsql;


/**
   A pl/pgsql version of geo_region.inside_border(name,lat,lon)
   Returns: 1 if lat,lon are inside of gazetteer_region with name=name, 0 if not, 
            and -1 if region not found
   Requires: PostGIS extension, gazetteer_region table.
   Dependencies: eventcoordinator (ec)
*/
CREATE OR REPLACE FUNCTION geo_region.inside_border(v_name gazetteer_region.name%TYPE, v_lat double precision, v_lon double precision)
RETURNS integer AS $$
DECLARE
result boolean;
p geometry;
region geometry;
BEGIN
    -- SRID 4326 is WGS84
    p := ST_SetSRID(ST_Point(v_lon, v_lat),4326);
    -- get the appropriate polygon out of the gazetteer_region table, convert to geometry
    SELECT geometry(border) INTO STRICT region FROM gazetteer_region WHERE name = v_name;
    result := ST_Within(p,region);
    IF result THEN
        RETURN 1;
    ELSE
        RETURN 0;
    END IF;
    -- exception block *has to* be at end
    EXCEPTION
        WHEN NO_DATA_FOUND THEN 
            RETURN -1;
        WHEN TOO_MANY_ROWS THEN
            RETURN -1;
END;
$$ LANGUAGE plpgsql;


/**
    Returns 0 if outside, 1 if inside, 2 if on the defined region border.
    The default coordinates are not set to the input values.
    Note : Inputs where latitude and longitude cross 0 are untested.
    Input : array of latlon types, reference lat, reference lon
*/
CREATE OR REPLACE FUNCTION geo_region.inside_border(v_coord latlon[], v_lat double precision, v_lon double precision)
RETURNS integer AS $$
DECLARE
    v_retval integer := 0;
    v_isicr  integer := 0;
    v_coord_length integer := 0;
BEGIN
    v_coord_length := array_length(v_coord,1);    
    IF v_coord_length < 3 THEN
        RAISE NOTICE USING MESSAGE = 'INVALID INPUT geo_region.inside_border(): coordinates count < 3 :' || v_coord_length;
        RETURN -1;
    END IF;

    IF (v_lat IS NULL) or (v_lon IS NULL) THEN
        RETURN 0;
    END IF;
    
    FOR idx IN 1..v_coord_length-1 
    LOOP
    v_isicr := ksicr(v_coord[idx].lon - v_lon,
                     v_coord[idx].lat - v_lat,
                     v_coord[idx+1].lon - v_lon,
                     v_coord[idx+1].lat - v_lat);
      IF (v_isicr = 4) THEN RETURN 2; END IF;
      v_retval := v_retval + v_isicr;
    END LOOP;
    v_isicr := ksicr(v_coord[v_coord_length].lon - v_lon, v_coord[v_coord_length].lat - v_lat,
                   v_coord[1].lon - v_lon, v_coord[1].lat - v_lat);
    IF (v_isicr = 4) THEN RETURN  2; ELSE RETURN (v_retval+v_isicr)/2; END IF;

END;
$$ LANGUAGE plpgsql;

/** 
    Private function
    Return 0 if point outside +/- 1 if point inside of border, else point is on an edge or vertex.
*/

CREATE OR REPLACE FUNCTION ksicr(v_lon1 double precision, v_lat1 double precision, v_lon2 double precision, v_lat2 double precision) 
RETURNS integer AS $$
BEGIN
    IF (v_lat1*v_lat2 > 0.) THEN RETURN 0; END IF;
    IF ( NOT ((v_lon1*v_lat2 <> v_lon2*v_lat1) OR (v_lon1*v_lon2 > 0.)) ) THEN RETURN 4; END IF;
    IF (v_lat1*v_lat2 < 0.) THEN
      IF (v_lat1 > 0.) THEN
        IF (v_lat1*v_lon2 >= v_lon1*v_lat2) THEN RETURN 0; ELSE RETURN  -2; END IF;
      END IF;
      IF (v_lon1*v_lat2 >= v_lat1*v_lon2) THEN RETURN 0; ELSE RETURN 2; END IF;
    END IF;
    IF (v_lat2 = 0.) THEN
      IF (v_lat1 = 0. OR v_lon2 > 0.) THEN RETURN 0; END IF;
      IF (v_lat1 > 0.) THEN RETURN  -1; ELSE RETURN 1; END IF;
    END IF;
    IF (v_lon1 > 0.) THEN  RETURN 0; END IF;
    IF (v_lat2 > 0.) THEN RETURN  1; ELSE RETURN  -1; END IF;
  END;
  $$ LANGUAGE plpgsql;


/**
   Returns name of the gazetteer_region associated with the specified p_grpid in the 
   assoc_region_group table whose polygon the input location is inside of, and in the
   case of multiple overlapping region boundaries within the same group, the one with
   the the lowest rg_order value.
   Returns NULL if input lat,lon point is outside of all regions associated with
   input group id.
*/
   CREATE OR REPLACE FUNCTION geo_region.authOfRegionInGroup(p_lat double precision, p_lon double precision, p_grpid varchar)
   RETURNS varchar AS $$
   DECLARE
     v_rname varchar;
     v_inside int;
     v_auth varchar := NULL;
   BEGIN
      FOR v_rname, v_inside IN
          SELECT g.auth, geo_region.inside_border(r.name, p_lat, p_lon)
          FROM gazetteer_region r, assoc_region_group g
          WHERE g.name=r.name AND g.grpid = p_grpid
          ORDER BY rg_order
      LOOP
         IF v_inside > 0 THEN
             v_auth := v_rname
             EXIT;
         END IF;
      END LOOP;
      RETURN v_auth;
   END;
$$ LANGUAGE plpgsql;
