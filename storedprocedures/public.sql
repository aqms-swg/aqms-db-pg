-- compatibility functions

DROP FUNCTION IF EXISTS public.SYS_EXTRACT_UTC(datetime timestamp with time zone);
CREATE OR REPLACE FUNCTION public.SYS_EXTRACT_UTC(datetime timestamp with time zone) RETURNS timestamp without time zone AS $$
    BEGIN
        return datetime at time zone 'UTC';
    END;
$$ LANGUAGE plpgsql;
