-- Forked from Oracle pl/sql implementation:
--   Revision: 8578
--   Last Changed Author: awwalter
--   Last Changed Rev: 7400
--   Last Changed Date: 2014-06-19 17:15:39 -0700 (Thu, 19 Jun 2014)
--   Text Last Updated: 2015-04-29 16:40:52 -0700 (Wed, 29 Apr 2015)

-- Ported to Postgresql plpgsql by kress August 2019

-- create schema if not exists db_cleanup authorization code;
-- grant usage on schema db_cleanup to trinetdb_read, trinetdb_execute;

-- Used in production: yes, UTC version
--
-- Package of PL/SQL procedures that delete table rows associated with specific event or origin ids.
-- This package must be installed in the user schema that owns the tables (e.g. TRINETDB), not the
-- CODE schema, as are the other packages.
--
-- Principal User:
--  Database Administrator to remove all records of unselected event "triggers"
--

-----------------------------------------------------------------------
-----------------------------------------------------------------------
-- Set output verbosity for debugging; default = 1, [0, 1, 2, 3]
-- set output verbosity level for debugging
CREATE OR REPLACE FUNCTION db_cleanup.set_verbosity(p_level INTEGER DEFAULT 1) RETURNS VOID AS $$
  BEGIN
    IF (p_level >= 0) THEN
      UPDATE t_global SET pkg_verbose = p_level;
      RAISE NOTICE 'Verbosity level :  %', p_level;
    END IF;
  END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------
-----------------------------------------------------------------------
-- Turn global wavefile deletion on/off, default is true
-- set deletion of waveform files from disk archive. 
CREATE OR REPLACE FUNCTION db_cleanup.set_wavefile_delete(p_delete_files INTEGER DEFAULT 1) RETURNS VOID AS $$
  BEGIN
    IF (p_delete_files = 1) THEN
      SELECT db_cleanup.set_wavefile_delete(true);
    ELSE
      SELECT db_cleanup.set_wavefile_delete(false);
    END IF;
  END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------
-----------------------------------------------------------------------
CREATE OR REPLACE FUNCTION db_cleanup.set_wavefile_delete(p_delete_files BOOLEAN DEFAULT true) RETURNS VOID AS $$
  DECLARE
    v_pkg_verbose INTEGER;
  BEGIN
    SELECT pkg_verbose FROM t_global INTO v_pkg_verbose;
    UPDATE t_global SET pkg_wavefile_delete_ok = p_delete_files;
    IF (v_pkg_verbose >=0) THEN
      IF (p_delete_files) THEN
        RAISE NOTICE 'Wavefile deletion is ON';
      ELSE
        RAISE NOTICE 'Wavefile deletion is OFF';
      END IF;
    END IF;
  END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------
-----------------------------------------------------------------------
-- logs input count rows as deleted for specified table and key id.
CREATE OR REPLACE FUNCTION db_cleanup.report_deletion(p_tableName VARCHAR, p_keyNumber BIGINT, p_count INT) RETURNS VOID AS $$
  DECLARE
    v_pkg_idFormat     VARCHAR(15);
    v_pkg_countFormat  VARCHAR(6);
  BEGIN
    SELECT pkg_idFormat,pkg_countFormat FROM t_global INTO v_pkg_idFormat,v_pkg_countFormat;
    IF (p_count > 0) THEN
      RAISE NOTICE '    Deleted from:  % for key: % count: %', RPAD(p_tableName,18),
                                                            TO_CHAR(p_keyNumber, v_pkg_idFormat),
                                                            TO_CHAR(p_count, v_pkg_countFormat);
    ELSE
      RAISE NOTICE '    No deletion from: % for key: %', RPAD(p_tableName,18),
                                                      TO_CHAR(coalesce(p_keyNumber, 0), v_pkg_idFormat);
    END IF;
  END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------
-----------------------------------------------------------------------
-- deletes all database data associated uniquely with the input event id parameter.
-- only if the event was not selected for archiving (selectflag=0).
-- Includes event and origin table associations and the triggered waveform data files.
-- returns number of rows deleted
CREATE OR REPLACE FUNCTION db_cleanup.delete_trigger(p_evid trinetdb.event.evid%TYPE) RETURNS INTEGER AS $$
  BEGIN
    RETURN db_cleanup.delete_event_type(p_evid , 0::SMALLINT);
  EXCEPTION
    WHEN OTHERS THEN
      RAISE EXCEPTION 'delete_trigger exception: % - %', SQLSTATE, SQLERRM;
  END;
$$ LANGUAGE plpgsql;
-----------------------------------------------------------------------
-----------------------------------------------------------------------
    -- deletes data associated uniquely with any triggered event not selected for archiving
    -- originating within the range start time (inclusive) and before end of range (exclusive).
    -- returns number of triggers deleted.
CREATE OR REPLACE FUNCTION db_cleanup.delete_triggers(p_startTime DOUBLE PRECISION, p_endTime DOUBLE PRECISION) RETURNS INTEGER AS $$
  DECLARE
    v_totalCount    INTEGER := 0;
    v_trigger_count INTEGER := 0;
    v_evid_table    BIGINT[];
  BEGIN
    IF (p_startTime > p_endTime) THEN
      RAISE NOTICE 'ERROR *delete_triggers* : p_startTime < p_endTime - % > %', p_startTime, p_endTime;
      RAISE data_exception;
    END IF;
    -- VCK: next statement was adapted fairly directly from oracle query.  Much room for improvement.
    v_evid_table := ARRAY(
      SELECT e.evid FROM trinetdb.event e WHERE e.selectflag = 0
         AND e.evid IN
        ((SELECT o.evid from trinetdb.origin o,trinetdb.event e WHERE o.evid=e.evid
               AND o.datetime BETWEEN 
               truetime.putEpoch(p_startTime,'UTC') AND truetime.putEpoch(p_endTime,'UTC'))
         UNION 
         (SELECT o.evid FROM trinetdb.origin o,trinetdb.event e WHERE e.evid::text = o.locevid
	      AND o.datetime BETWEEN 
              truetime.putEpoch(p_startTime,'UTC') AND truetime.putEpoch(p_endTime,'UTC'))
        )
       );
    SELECT array_length(v_evid_table,1) INTO v_trigger_count;
    RAISE NOTICE ' db_cleanup.delete_triggers found: % triggers in specified input range. ', v_trigger_count;
    IF (v_trigger_count IS NULL) THEN
     RETURN 0;
    END IF; 
    -- have trigger id list in indexed by table, process it
    FOR idx IN 1 .. v_trigger_count LOOP
      v_totalCount := v_totalCount + db_cleanup.delete_trigger(v_evid_table[idx]);
    END LOOP;
    RAISE NOTICE ' db_cleanup.delete_triggers deleted: % total rows for % triggers.',v_totalCount,v_trigger_count;
    RETURN v_trigger_count;
  EXCEPTION
    WHEN data_exception THEN
      RAISE NOTICE ' delete_triggers value error';
      RETURN 0;
    WHEN OTHERS THEN
      RAISE EXCEPTION 'ERROR *delete_trigers* % - %. rows deleted: % for % triggers.',SQLSTATE,SQLERRM,v_totalCount,v_trigger_count;
  END;
$$ LANGUAGE plpgsql;
-----------------------------------------------------------------------
-----------------------------------------------------------------------
-- deletes data associated uniquely with any triggered event not selected for archiving
-- originating within the range start time (inclusive) and before end of range (exclusive).
-- returns number of triggers deleted.
CREATE OR REPLACE FUNCTION db_cleanup.delete_triggers(p_startDate DATE, p_endDate DATE) RETURNS INTEGER AS $$
  DECLARE
    v_startTime DOUBLE PRECISION := truetime.string2true(TO_CHAR(p_startDate,'YYYY/MM/DD'));
    v_endTime   DOUBLE PRECISION := truetime.string2true(TO_CHAR(p_endDate,'YYYY/MM/DD'));
  BEGIN
    RETURN db_cleanup.delete_triggers(v_startTime, v_endTime);
  END;
$$ LANGUAGE plpgsql;
-----------------------------------------------------------------------
-----------------------------------------------------------------------
-- deletes data associated uniquely with any triggered event not selected for archiving
-- originating within the range start time (inclusive) and before end of range (exclusive).
-- returns number of triggers deleted.
-- Input string FORMAT 'YYYY/MM/DD'
CREATE OR REPLACE FUNCTION db_cleanup.delete_triggers(p_startDate VARCHAR, p_endDate VARCHAR) RETURNS INTEGER AS $$
  DECLARE
    v_startTime DOUBLE PRECISION := truetime.string2true(p_startDate);
    v_endTime   DOUBLE PRECISION := truetime.string2true(p_endDate);
    v_return    INTEGER;
  BEGIN
    SELECT db_cleanup.delete_triggers(v_startTime, v_endTime) INTO v_return;
    RETURN v_return;
  END;
$$ LANGUAGE plpgsql;
-----------------------------------------------------------------------
-----------------------------------------------------------------------
-- deletes data associated uniquely with any triggered event originating
-- from the specified input date up until 24 hours later and not selected for archiving.
-- returns number of triggers deleted.
CREATE OR REPLACE FUNCTION db_cleanup.delete_triggers(p_date DATE) RETURNS INTEGER AS $$
  DECLARE
    v_startTime DOUBLE PRECISION := truetime.string2true(TO_CHAR(p_date, 'YYYY/MM/DD'));
    v_endTime   DOUBLE PRECISION := v_startTime + 86400.;
    v_return    INTEGER;
  BEGIN
    RETURN db_cleanup.delete_triggers( v_startTime, v_endTime );
  END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes data associated uniquely with any triggered event originating
-- from the specified input date until 24 hours later and not selected for archiving.
-- returns number of triggers deleted.
-- Input string FORMAT 'YYYY/MM/DD'
CREATE OR REPLACE FUNCTION db_cleanup.delete_triggers(p_date VARCHAR) RETURNS INTEGER AS $$
  DECLARE
    v_startTime DOUBLE PRECISION := truetime.string2true(p_date);
    v_endTime   DOUBLE PRECISION := v_startTime + 86400.;
    v_return    INTEGER;
  BEGIN
    SELECT db_cleanup.delete_triggers( v_startTime, v_endTime ) INTO v_return;
    RETURN v_return;
  END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all database data associated uniquely with the input event id parameter.
-- even if the event was selected for archiving (selectflag=1).
-- Includes event and origin table associations and the triggered waveform data files.
-- NOTE THIS FUNCTION WILL DELETE DATA FOR EVENTS SELECTED FOR ARCHIVING !!!
-- returns number of rows deleted
CREATE OR REPLACE FUNCTION db_cleanup.delete_event(p_evid trinetdb.event.evid%TYPE) RETURNS INTEGER AS $$
  DECLARE 
    v_return INTEGER;
  BEGIN
    SELECT db_cleanup.delete_event_type(p_evid , 1) INTO v_return;
    RETURN v_return;
  END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all table rows associated uniquely with the input event id parameter's associated origins.
CREATE OR REPLACE FUNCTION db_cleanup.delete_all_origins(p_evid trinetdb.event.evid%TYPE) RETURNS INTEGER AS $$
  DECLARE
    v_totalCount       INTEGER := 0;
    v_originCount      INTEGER := 0;
    v_eventDuplicates  INTEGER := 0;
    v_orid_table       BIGINT[];
    v_orid_count       INTEGER;
    -- No FOR UPDATE here, since "commit" code in called procedures invalidates cursor.
    -- producing error oracle messages FETCH out of sequence ORA-1002
    c_origin CURSOR FOR
        SELECT orid FROM trinetdb.origin WHERE evid = p_evid;
    --
    c_event_chk CURSOR (p_orid trinetdb.origin.orid%TYPE, p_evtId trinetdb.event.evid%TYPE) FOR
       SELECT count(*) FROM trinetdb.event WHERE prefor = p_orid AND evid != p_evtId;
  BEGIN
    -- get all orids for input evid
    v_orid_table := ARRAY( SELECT orid FROM trinetdb.origin WHERE evid = p_evid );
    SELECT array_length(v_orid_table,1) INTO v_orid_count;
    -- if no orids for this evid, return
    IF (v_orid_count IS NULL) THEN
        RETURN 0;
    END IF;
    -- have orid data for evid
    FOR idx IN 1..v_orid_count LOOP
        OPEN c_event_chk(v_orid_table[idx], p_evid);
            FETCH c_event_chk INTO v_eventDuplicates;
        CLOSE c_event_chk;
        IF (v_eventDuplicates = 0) THEN
           RAISE DEBUG 'v_eventDuplicates: %',v_eventDuplicates;
           v_totalCount  := v_totalCount + db_cleanup.delete_origin(v_orid_table[idx]);
           v_originCount := v_originCount + 1;
        ELSE
           RAISE NOTICE ' => SAVING data for orid: % referenced by % other evid.',v_orid_table[idx],v_eventDuplicates;
        END IF;
    END LOOP;
    RETURN v_totalCount;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE EXCEPTION 'ERROR *delete_all_origins* % - %. rows deleted: % for % total origins of evid: %',
                       SQLSTATE, SQLERRM, v_totalCount, v_originCount, p_evid;
  END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes AMP table row corresponding to input ampid key
-- if no origin or magnitude children are associated with this ampid.
-- since constraints prohibit parent deletion if foreign key in child tables.
CREATE OR REPLACE FUNCTION db_cleanup.delete_amp(p_ampid trinetdb.amp.ampid%TYPE) RETURNS INTEGER AS $$
    DECLARE
        v_pkg_verbose INTEGER;
        v_countOrg    INTEGER := 0;
        v_countMag    INTEGER := 0;
	v_del	      INTEGER := 0;
    BEGIN
        SELECT pkg_verbose FROM t_global INTO v_pkg_verbose;
        SELECT COUNT(*) INTO v_countOrg FROM trinetdb.assocamo WHERE ampid = p_ampid;
        IF (v_countOrg != 0) THEN
           RETURN 0;
        END IF;
        SELECT COUNT(*) INTO v_countMag FROM trinetdb.assocamm WHERE ampid = p_ampid;
        IF (v_countMag != 0) THEN
           RETURN 0;
        END IF;
        --
        -- 2010/12/21 code assumes ampset table exists in db schema
        DELETE FROM trinetdb.ampset WHERE AMPID = p_ampid;
        GET DIAGNOSTICS v_del = ROW_COUNT;
        IF (v_pkg_verbose > 2) THEN 
          PERFORM db_cleanup.report_deletion('AMPSET',p_ampid,v_del); 
        END IF;
        --
        DELETE FROM trinetdb.amp WHERE AMPID = p_ampid;
	GET DIAGNOSTICS v_del = ROW_COUNT;
        IF (v_pkg_verbose > 2) THEN
          PERFORM db_cleanup.report_deletion('AMP',p_ampid, v_del); 
        END IF;
        RETURN v_del;
    EXCEPTION
        WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege to delete from table amp';
          RAISE;
        WHEN OTHERS THEN
            RAISE EXCEPTION 'ERROR *delete_amp* : % - %', SQLSTATE, SQLERRM;
    END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all ASSOCAMM and AMP table rows associated uniquely with input magid parameter.
CREATE OR REPLACE FUNCTION db_cleanup.delete_assocamm(p_magid trinetdb.netmag.magid%TYPE) RETURNS INTEGER AS $$
    DECLARE
        v_pkg_verbose INTEGER;
        v_totalCount  INTEGER := 0;
	v_del         INTEGER := 0;
	r_amp         RECORD;
        c_amp CURSOR FOR
            SELECT ampid FROM trinetdb.assocamm WHERE magid = p_magid FOR UPDATE; --  NOWAIT;
    BEGIN
        SELECT pkg_verbose FROM t_global INTO v_pkg_verbose;
	OPEN c_amp;
        LOOP
	    FETCH c_amp INTO r_amp;
	    EXIT WHEN NOT FOUND;
            -- delete netmag (child) associations
            DELETE FROM trinetdb.assocamm WHERE CURRENT OF c_amp;
            GET DIAGNOSTICS v_del = ROW_COUNT;
            v_totalCount := v_totalCount + v_del;
            IF (v_pkg_verbose > 2) THEN
              PERFORM db_cleanup.report_deletion('ASSOCAMM', r_amp.ampid, v_del);
            END IF;
            -- delete parent if now unassociated
            v_totalCount := v_totalCount + db_cleanup.delete_amp(r_amp.ampid);
        END LOOP;
	CLOSE c_amp;
        RETURN v_totalCount;
    EXCEPTION
        WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege to delete from table assocamm';
        WHEN OTHERS THEN
            RAISE EXCEPTION 'ERROR *delete_assocamm* : % %. rows deleted: %', SQLSTATE, SQLERRM, v_totalCount;
    END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all ASSOCAMO and AMP table rows associated uniquely with input orid parameter.
CREATE OR REPLACE FUNCTION db_cleanup.delete_assocamo(p_orid trinetdb.origin.orid%TYPE) RETURNS INTEGER AS $$
    DECLARE
        v_pkg_verbose INTEGER;
        v_totalCount  INTEGER := 0;
	v_del         INTEGER := 0;
	r_amp         RECORD;
        c_amp CURSOR FOR
          SELECT ampid FROM trinetdb.assocamo WHERE orid = p_orid FOR UPDATE; --  NOWAIT;
    BEGIN
        SELECT pkg_verbose FROM t_global INTO v_pkg_verbose;
	OPEN c_amp;
        LOOP
	    FETCH c_amp INTO r_amp;
	    EXIT WHEN NOT FOUND;
            -- delete origin (child) associations
            DELETE FROM trinetdb.assocamo WHERE CURRENT OF c_amp;
            GET DIAGNOSTICS v_del = ROW_COUNT;
            v_totalCount := v_totalCount + v_del;
            IF (v_pkg_verbose > 2) THEN
              PERFORM db_cleanup.report_deletion('ASSOCAMO', r_amp.ampid, v_del);
            END IF;
            -- delete parent if now unassociated
            v_totalCount := v_totalCount + db_cleanup.delete_amp(r_amp.ampid);
        END LOOP;
	CLOSE c_amp;
        RETURN v_totalCount;
    EXCEPTION
        WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege to delete from table assocamo';
        WHEN OTHERS THEN
            RAISE EXCEPTION 'ERROR *delete_assocamo* % %. rows deleted: %', SQLSTATE, SQLERRM, v_totalCount;
    END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all AMP table rows associated uniquely with input origin and magnitude parameters.
CREATE OR REPLACE FUNCTION db_cleanup.delete_amps(p_orid trinetdb.origin.orid%TYPE, p_magid trinetdb.netmag.magid%TYPE) RETURNS INTEGER AS $$
    DECLARE
        v_pkg_verbose     INTEGER;
	v_pkg_countFormat VARCHAR(6);
        v_totalCount      INTEGER := 0;
    BEGIN
        SELECT pkg_verbose,pkg_countFormat FROM t_global INTO v_pkg_verbose,v_pkg_countFormat;
        v_totalCount := v_totalCount + db_cleanup.delete_assocamm(p_magid);
        v_totalCount := v_totalCount + db_cleanup.delete_assocamo(p_orid);
        IF (v_pkg_verbose > 1) THEN
            RAISE NOTICE'    *delete_amps* % rows deleted: ',TO_CHAR(v_totalCount,v_pkg_countFormat);
        END IF;
        RETURN v_totalCount;
    EXCEPTION
        WHEN OTHERS THEN
            RAISE EXCEPTION 'ERROR *delete_amps* % - %. rows deleted: %',SQLSTATE,SQLERRM,TO_CHAR(v_totalCount,v_pkg_countFormat);
    END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all CODA table rows associated uniquely with input origin and netmag id parameters.
-- assumes assoccom coid are subset of assocco coid.
CREATE OR REPLACE FUNCTION db_cleanup.delete_codas(p_orid trinetdb.origin.orid%TYPE, p_magid trinetdb.netmag.magid%TYPE) RETURNS INTEGER AS $$
    DECLARE
        v_pkg_verbose      INTEGER;
        v_pkg_countFormat  VARCHAR(6);
        v_totalCount       INTEGER := 0;
        v_countOrg         INTEGER := 0;
        v_countMag         INTEGER := 0;
        v_keyId            BIGINT := 0;
        v_del              INTEGER := 0;
	r_coda             RECORD;
        c_coda CURSOR FOR
          SELECT coid FROM trinetdb.assoccoo WHERE orid = p_orid FOR UPDATE; --  NOWAIT;
    BEGIN
        SELECT pkg_verbose,pkg_countFormat FROM t_global INTO v_pkg_verbose,v_pkg_countFormat;
	OPEN c_coda;
        LOOP
	    FETCH c_coda INTO r_coda;
	    EXIT WHEN NOT FOUND;
            v_keyId := r_coda.coid;
            -- delete origin (child) associations
            DELETE FROM trinetdb.assoccoo WHERE CURRENT OF c_coda;
	    GET DIAGNOSTICS v_del = ROW_COUNT;
            v_totalCount := v_totalCount + v_del;
            IF (v_pkg_verbose > 2) THEN
              PERFORM db_cleanup.report_deletion('ASSOCCOO', v_keyId, v_del);
            END IF;
            DELETE FROM trinetdb.assoccom WHERE magid = p_magid AND coid = v_keyId;
	    GET DIAGNOSTICS v_del = ROW_COUNT;
            v_totalCount := v_totalCount + v_del;
            IF (v_pkg_verbose > 2) THEN
              PERFORM db_cleanup.report_deletion('ASSOCCOM', v_keyId, v_del);
            END IF;
            -- delete parent if now unassociated
            SELECT COUNT(*) INTO v_countOrg FROM trinetdb.assoccoo WHERE coid = v_keyId;
            IF (v_countOrg = 0) THEN
                SELECT COUNT(*) INTO v_countMag FROM trinetdb.assoccom WHERE coid = v_keyId;
                IF (v_countMag = 0) THEN
                    DELETE FROM trinetdb.coda WHERE coid = v_keyId;
		    GET DIAGNOSTICS v_del = ROW_COUNT;
                    v_totalCount := v_totalCount + v_del;
                    IF (v_pkg_verbose > 2) THEN
                      PERFORM db_cleanup.report_deletion('CODA', v_keyId, v_del);
                    END IF;
                END IF;
            END IF;
        END LOOP;
	CLOSE c_coda;
        IF (v_pkg_verbose > 1) THEN
            RAISE NOTICE '    *delete_codas* rows deleted: % ',TO_CHAR(v_totalCount,v_pkg_countFormat);
        END IF;
        RETURN v_totalCount;
    EXCEPTION
        WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege to delete from coda affiliated table';
        WHEN OTHERS THEN
            RAISE EXCEPTION 'ERROR *delete_codas* % - %.  rows deleted: %',SQLSTATE,SQLERRM,TO_CHAR(v_totalCount,v_pkg_countFormat);
    END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all ARRIVAL table rows associated uniquely with input origin id parameter.
CREATE OR REPLACE FUNCTION db_cleanup.delete_arrivals(p_orid trinetdb.origin.orid%TYPE) RETURNS INTEGER AS $$
    DECLARE
        v_pkg_verbose      INTEGER;
        v_pkg_countFormat  VARCHAR(6);
        v_totalCount       INTEGER := 0;
        v_countOrg         INTEGER := 0;
        v_keyId            BIGINT;
        v_del              INTEGER := 0;
	r_arrival          RECORD;
        c_arrival CURSOR FOR
          SELECT arid FROM trinetdb.assocaro WHERE orid = p_orid FOR UPDATE; --  NOWAIT;
    BEGIN
        SELECT pkg_verbose,pkg_countFormat FROM t_global INTO v_pkg_verbose,v_pkg_countFormat;
	OPEN c_arrival;
        LOOP
	    FETCH c_arrival INTO r_arrival;
	    EXIT WHEN NOT FOUND;
            v_keyId := r_arrival.arid;
            --
            -- delete origin (child) associations
            DELETE FROM trinetdb.assocaro WHERE CURRENT OF c_arrival;
	    GET DIAGNOSTICS v_del = ROW_COUNT;
            v_totalCount := v_totalCount + v_del;
            IF (v_pkg_verbose > 2) THEN
              PERFORM db_cleanup.report_deletion('ASSOCARO', v_keyId, v_del);
            END IF;
            -- delete parent if now unassociated
            SELECT COUNT(*) INTO v_countOrg FROM trinetdb.assocaro WHERE arid = v_keyId;
            IF (v_countOrg = 0) THEN
                DELETE FROM trinetdb.arrival WHERE arid = v_keyId;
                GET DIAGNOSTICS v_del = ROW_COUNT;
                v_totalCount := v_totalCount + v_del;
                IF (v_pkg_verbose > 2) THEN
                   PERFORM db_cleanup.report_deletion('ARRIVAL', v_keyId, v_del);
                END IF;
            END IF;
        END LOOP;
	CLOSE c_arrival;
        IF (v_pkg_verbose > 1) THEN
            RAISE NOTICE '    *delete_arrivals* rows deleted: %',TO_CHAR(v_totalCount,v_pkg_countFormat);
        END IF;
        RETURN v_totalCount;
    EXCEPTION
        WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege to delete from arrival affiliated table';
        WHEN OTHERS THEN
            RAISE EXCEPTION 'ERROR *delete_arrivals* % - %.  rows deleted: %',SQLSTATE,SQLERRM,TO_CHAR(v_totalCount,v_pkg_countFormat);
    END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all NETTRIG table rows associated uniquely with input event id parameter.
CREATE OR REPLACE FUNCTION db_cleanup.delete_nettrig(p_evid trinetdb.event.evid%TYPE) RETURNS INTEGER AS $$
    DECLARE
        v_pkg_verbose INTEGER;
        v_pkg_countFormat  VARCHAR(6);
        v_totalCount   INTEGER := 0;
        v_countEvt     INTEGER := 0;
        v_keyId        BIGINT;
	v_del	       INTEGER := 0;
	r_assocnte     RECORD;
        c_trig CURSOR FOR
          SELECT ntid FROM trinetdb.assocnte WHERE evid = p_evid FOR UPDATE; --  NOWAIT;
    BEGIN
        SELECT pkg_verbose,pkg_countFormat FROM t_global INTO v_pkg_verbose,v_pkg_countFormat;
	OPEN c_trig;
	LOOP
            FETCH c_trig INTO r_assocnte;
	    EXIT WHEN NOT FOUND;
            v_keyId := r_assocnte.ntid;
            -- delete origin (child) associations
            DELETE FROM trinetdb.assocnte WHERE CURRENT OF c_trig;
	    GET DIAGNOSTICS v_del = ROW_COUNT;
            v_totalCount := v_totalCount + v_del;
            IF (v_pkg_verbose > 2) THEN
              PERFORM db_cleanup.report_deletion('ASSOCNTE', v_keyId, v_del);
            END IF;
            SELECT COUNT(*) INTO v_countEvt FROM trinetdb.assocnte WHERE ntid = v_keyId;
            -- delete parent if now unassociated
            IF (v_countEvt = 0) THEN
                DELETE FROM trinetdb.trig_channel WHERE ntid = v_keyId;
		GET DIAGNOSTICS v_del = ROW_COUNT;
                v_totalCount := v_totalCount + v_del;
                IF (v_pkg_verbose > 2) THEN
                  PERFORM db_cleanup.report_deletion('TRIG_CHANNEL', v_keyId, v_del);
                END IF;
                DELETE FROM trinetdb.nettrig WHERE ntid = v_keyId;
		GET DIAGNOSTICS v_del = ROW_COUNT;
                v_totalCount := v_totalCount + v_del;
                IF (v_pkg_verbose > 2) THEN
                  PERFORM db_cleanup.report_deletion('NETTRIG', v_keyId, v_del);
                END IF;
            END IF;
        END LOOP;
	CLOSE c_trig;
        IF (v_pkg_verbose > 1) THEN
            RAISE NOTICE '    *delete_nettrig* rows deleted: %',
                TO_CHAR(v_totalCount, v_pkg_countFormat);
        END IF;
        RETURN v_totalCount;
    EXCEPTION
        WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege to delete from trigger affiliated table';
        WHEN undefined_table THEN 
            IF (v_pkg_verbose > 1) THEN
              RAISE NOTICE 'Info: Trigger tables D.N.E!'; 
            END IF;
            RETURN 0;
        WHEN OTHERS THEN
            RAISE WARNING 'ERROR *delete_nettrig*  % - %. rows deleted: %  for evid: %  performing ROLLBACK.',
	           SQLSTATE,SQLERRM,v_totalCount,p_evid;
    END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes REMARK table row associated with the input event id parameter.
CREATE OR REPLACE FUNCTION db_cleanup.delete_remark(p_commid trinetdb.remark.commid%TYPE) RETURNS INTEGER AS $$
    DECLARE
        v_pkg_verbose INTEGER;
	v_del	      INTEGER := 0;
    BEGIN
        SELECT pkg_verbose FROM t_global INTO v_pkg_verbose;
        DELETE FROM trinetdb.remark WHERE commid = p_commid;
	GET DIAGNOSTICS v_del = ROW_COUNT;
        IF (v_pkg_verbose > 2) THEN
          PERFORM db_cleanup.report_deletion('REMARK', p_commid, v_del);
        END IF;
        RETURN v_del;
    EXCEPTION
        WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege to delete from remark table';
        WHEN OTHERS THEN
            RAISE EXCEPTION 'e_delete_remark: % - %',SQLSTATE,SQLERRM;
    END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes ORIGIN_ERROR table row associated with the input origin id parameter.
CREATE OR REPLACE FUNCTION db_cleanup.delete_credit(p_id trinetdb.EVENT.evid%TYPE, p_table VARCHAR) RETURNS INTEGER AS $$
    DECLARE
        v_pkg_verbose INTEGER;
	v_del	      INTEGER := 0;
    BEGIN
        SELECT pkg_verbose FROM t_global INTO v_pkg_verbose;
        DELETE FROM trinetdb.credit WHERE id = p_id AND tname = p_table;
	GET DIAGNOSTICS v_del = ROW_COUNT;
        IF (v_pkg_verbose > 2) THEN
          PERFORM db_cleanup.report_deletion('CREDIT:' || p_table , p_id, v_del);
        END IF;
        RETURN v_del;
    EXCEPTION
        WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege to delete from credit table';
        WHEN undefined_table THEN 
            IF (v_pkg_verbose > 1) THEN
              RAISE INFO 'Info: CREDIT table D.N.E!'; 
            END IF;
            RETURN 0;
        WHEN OTHERS THEN
            RAISE EXCEPTION 'e_delete_credit: % - %',SQLSTATE,SQLERRM;
    END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes ORIGIN_ERROR table row associated with the input origin id parameter.
CREATE OR REPLACE FUNCTION db_cleanup.delete_origin_error(p_orid trinetdb.origin.orid%TYPE) RETURNS INTEGER AS $$
    DECLARE
        v_pkg_verbose INTEGER;
	v_del	      INTEGER := 0;
    BEGIN
        SELECT pkg_verbose FROM t_global INTO v_pkg_verbose;
        DELETE FROM trinetdb.origin_error WHERE orid = p_orid;
	GET DIAGNOSTICS v_del = ROW_COUNT;
        IF (v_pkg_verbose > 2) THEN
          PERFORM db_cleanup.report_deletion('ORIGIN_ERROR', p_orid, v_del);
        END IF;
        RETURN v_del;
    EXCEPTION
        WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege to delete from origin_error table';
        WHEN undefined_table THEN 
            IF (v_pkg_verbose > 1) THEN
              RAISE INFO 'Info: ORIGIN_ERROR table D.N.E!'; 
            END IF;
            RETURN 0;
        WHEN OTHERS THEN
            RAISE EXCEPTION 'delete_origin_error';
    END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all NETMAG table rows associated with the input origin id parameter.
CREATE OR REPLACE FUNCTION db_cleanup.delete_netmags(p_orid trinetdb.origin.orid%TYPE) RETURNS INTEGER AS $$
    DECLARE
        v_pkg_verbose INTEGER;
        --
        c_mag_chk CURSOR (p_magid trinetdb.netmag.magid%TYPE, p_orid trinetdb.origin.orid%TYPE) FOR
            SELECT count(*) FROM trinetdb.origin WHERE prefmag = p_magid AND orid != p_orid;
        --
        v_magid_table      BIGINT[];
        v_magid_count      INTEGER;
        v_totalCount       INTEGER := 0;
        v_eventDuplicates  INTEGER := 0;
	v_del	           INTEGER := 0;
    BEGIN
        SELECT pkg_verbose FROM t_global INTO v_pkg_verbose;
        -- get all magids for input orid
        v_magid_table := ARRAY( SELECT magid FROM trinetdb.netmag WHERE orid = p_orid );
        SELECT array_length(v_magid_table,1) INTO v_magid_count;
       -- if no magids for this orid, return
        IF (v_magid_count IS NULL) THEN
            RETURN 0;
        END IF;
        -- have magnitude data for orid
        FOR idx IN 1..v_magid_count LOOP
            -- Check for any other orid's pointing to this magid
            OPEN c_mag_chk(v_magid_table[idx], p_orid);
                FETCH c_mag_chk INTO v_eventDuplicates;
            CLOSE c_mag_chk;
            -- delete netmag row only if no other ids reference this magid
            IF (v_eventDuplicates = 0) THEN
                DELETE FROM trinetdb.netmag WHERE magid = v_magid_table[idx];
                GET DIAGNOSTICS v_del = ROW_COUNT;
                IF (v_pkg_verbose > 2) THEN
                  PERFORM db_cleanup.report_deletion('NETMAG', p_orid, v_del);
                END IF;
                v_totalCount := v_totalCount +  v_del;
                v_totalCount := v_totalCount +  db_cleanup.delete_credit(v_magid_table[idx], 'NETMAG');
            ELSE
                RAISE NOTICE ' => SAVING data for magid: % referenced by % other orid.',v_magid_table[idx],v_eventDuplicates;
            END IF;
        END LOOP;
        RETURN v_totalCount;
    EXCEPTION
        WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege to delete from netmag table';
        WHEN OTHERS THEN
            RAISE EXCEPTION 'e_delete_netmags: % - %',SQLSTATE,SQLERRM;
    END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all PREFMAG table rows associated with the input event id parameter.
CREATE OR REPLACE FUNCTION db_cleanup.delete_prefmags(p_evid trinetdb.event.evid%TYPE) RETURNS INTEGER AS $$
    DECLARE
       v_pkg_verbose INTEGER;
       v_totalCount  INTEGER := 0;
       v_del	     INTEGER := 0;
    BEGIN
       SELECT pkg_verbose FROM t_global INTO v_pkg_verbose;
       DELETE FROM trinetdb.eventprefmag WHERE evid = p_evid;
       GET DIAGNOSTICS v_del = ROW_COUNT;
       IF (v_pkg_verbose > 2) THEN
         PERFORM db_cleanup.report_deletion('EVENTPREFMAG', p_evid, v_del);
       END IF;
       v_totalCount := v_totalCount +  v_del;
       RETURN v_totalCount;
    EXCEPTION
        WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege to delete from eventprefmag table';
        WHEN OTHERS THEN
            RAISE EXCEPTION 'e_delete_prefmags: % - %',SQLSTATE,SQLERRM;
    END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all PREFOR table rows associated with the input event id parameter.
CREATE OR REPLACE FUNCTION db_cleanup.delete_prefors(p_evid trinetdb.event.evid%TYPE) RETURNS INTEGER AS $$
    DECLARE
       v_pkg_verbose INTEGER;
       v_totalCount  INTEGER := 0;
       v_del         INTEGER := 0;
    BEGIN
       SELECT pkg_verbose FROM t_global INTO v_pkg_verbose;
       EXECUTE IMMEDIATE 'DELETE FROM trinetdb.eventprefor WHERE evid = :1' USING p_evid;
       GET DIAGNOSTICS v_del = ROW_COUNT;
       IF (v_pkg_verbose > 2) THEN
         PERFORM db_cleanup.report_deletion('EVENTPREFOR', p_evid, v_del);
       END IF;
       v_totalCount := v_totalCount +  v_del;
       RETURN v_totalCount;
    EXCEPTION
        WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege to delete from eventprefor table';
        WHEN OTHERS THEN
            RAISE EXCEPTION 'e_delete_prefors: % - %',SQLSTATE,SQLERRM;
    END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all PREFMEC table rows associated with the input event id parameter.
CREATE OR REPLACE FUNCTION db_cleanup.delete_prefmecs(p_evid trinetdb.event.evid%TYPE) RETURNS INTEGER AS $$
    DECLARE
       v_pkg_verbose INTEGER;
       v_totalCount  INTEGER := 0;
       v_del         INTEGER := 0;
    BEGIN
       SELECT pkg_verbose FROM t_global INTO v_pkg_verbose;
       DELETE FROM trinetdb.eventprefmec WHERE evid = p_evid;
       GET DIAGNOSTICS v_del = ROW_COUNT;
       IF (v_pkg_verbose > 2) THEN
         PERFORM db_cleanup.report_deletion('EVENTPREFMEC', p_evid, v_del);
       END IF;
       v_totalCount := v_totalCount + v_del;
       RETURN v_totalCount;
    EXCEPTION
       WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege to delete from eventprefmec table';
       WHEN OTHERS THEN
          RAISE EXCEPTION 'e_delete_prefmecs: % - %',SQLSTATE,SQLERRM;
    END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all ALARM_ACTION table rows associated with the input event id parameter.
CREATE OR REPLACE FUNCTION db_cleanup.delete_alarms(p_evid trinetdb.event.evid%TYPE) RETURNS INTEGER AS $$
    DECLARE
       v_pkg_verbose INTEGER;
       v_totalCount  INTEGER := 0;
       v_del         INTEGER := 0;
    BEGIN
       SELECT pkg_verbose FROM t_global INTO v_pkg_verbose;
       DELETE FROM trinetdb.alarm_action WHERE event_id = p_evid;
       GET DIAGNOSTICS v_del = ROW_COUNT;
       IF (v_pkg_verbose > 2) THEN
         PERFORM db_cleanup.report_deletion('ALARM_ACTION', p_evid, v_del);
       END IF;
       v_totalCount := v_totalCount + v_del;
       RETURN v_totalCount;
    EXCEPTION
       WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege to delete from alarm_action table';
       WHEN OTHERS THEN
          RAISE EXCEPTION 'e_delete_alarms: % - %',SQLSTATE,SQLERRM;
    END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all PREFMAG table rows associated with the input event id parameter.
CREATE OR REPLACE FUNCTION db_cleanup.delete_requests(p_evid trinetdb.event.evid%TYPE) RETURNS INTEGER AS $$
    DECLARE
       v_pkg_verbose INTEGER;
       v_totalCount  INTEGER := 0;
    BEGIN
       SELECT pkg_verbose FROM t_global INTO v_pkg_verbose;
       DELETE FROM trinetdb.request_card WHERE evid = p_evid;
       GET DIAGNOSTICS v_totalCount = ROW_COUNT;
       IF (v_pkg_verbose > 2) THEN
         PERFORM db_cleanup.report_deletion('REQUEST_CARD', p_evid, v_totalCount);
       END IF;
       RETURN v_totalCount;
    EXCEPTION
       WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege to delete from request_card table';
       WHEN OTHERS THEN
          RAISE EXCEPTION 'e_delete_requests: % - %',SQLSTATE,SQLERRM;
    END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all reading table rows associated uniquely with the input origin id and its associated magids.
-- does a commit after the associated tables data are deleted for each origin associated magnitude
CREATE OR REPLACE FUNCTION db_cleanup.delete_origin_mag_assoc(p_orid trinetdb.origin.orid%TYPE) RETURNS INTEGER AS $$
    DECLARE
        v_pkg_verbose     INTEGER;
	v_pkg_idFormat    VARCHAR(15);
        v_magid_table     BIGINT[];
	v_magid_count     INTEGER := 0;
        v_totalCount      INTEGER := 0;
        v_magid           trinetdb.netmag.magid%TYPE;
        v_magtype         trinetdb.netmag.magtype%TYPE;
        v_loopCount       INTEGER := 1;
        ----
    BEGIN
        SELECT pkg_verbose,pkg_idFormat FROM t_global INTO v_pkg_verbose,v_pkg_idFormat;
       -- get all magids for this orid
        v_magid_table := ARRAY( SELECT magid FROM trinetdb.netmag WHERE orid = p_orid );
        SELECT array_length(v_magid_table,1) INTO v_magid_count;
        --
        -- check for existance of any magnitude associated with this input orid
        IF (v_magid_count IS NOT NULL) THEN
            IF (v_pkg_verbose > 1)  THEN
                RAISE NOTICE '*delete_origin_mag_assoc* orid: % oridMagidCount: %.', 
                      TO_CHAR(p_orid, v_pkg_idFormat), TO_CHAR(v_magid_count, '09');
            END IF;
            -- delete all rows assocs with magids
            FOR idx IN 1 .. v_magid_count LOOP
                v_magid := v_magid_table[idx];
                BEGIN
                    SELECT magtype INTO v_magtype FROM trinetdb.netmag WHERE magid=v_magid; -- implicit cursor, uses index though
                    IF (v_pkg_verbose > 1) THEN
                        RAISE NOTICE '.   magid: % magtype: %', TO_CHAR(v_magid, v_pkg_idFormat), v_magtype;
                    END IF;
                    IF (v_magtype != 'c' AND v_magtype != 'd') THEN
                        -- known coda types
                        v_totalCount := v_totalCount + db_cleanup.delete_amps(p_orid, v_magid);
                    ELSE
                        -- amps assoc with known types: 'e','l','n' ... ?
                        v_totalCount := v_totalCount + db_cleanup.delete_codas(p_orid, v_magid);
                    END IF;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        RAISE WARNING 'WARNING - delete_origin_mag_assoc : no magtype found for orid: % magid: %',
                            p_orid, v_magid;
                END;
            END LOOP;  -- end of mag loop
        END IF;
        --
        RETURN v_totalCount;
    EXCEPTION
        WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege delete_origin_mag_assoc to delete from coda/amp table';
          RAISE;
        WHEN OTHERS THEN
          RAISE EXCEPTION 'delete_origin_mag_assoc error: % - %',SQLSTATE,SQLERRM;
    END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
--  package body private function
--  Delete rows from those mec tables affiliated with input mecid
-- MECFREQDATA pk mecfreqid
-- MECFREQ pk mecfreqid
-- MECCHANNEL pk mecdataid
-- MECDATA pk mecdataid mecfreqid fk mecid fk
-- MECOBJECT pk mecid
-- MEC  pk mecid
--
CREATE OR REPLACE FUNCTION db_cleanup.delete_mec_data( p_mecid trinetdb.mec.mecid%TYPE) RETURNS INTEGER AS $$
    DECLARE
        v_pkg_verbose INTEGER;
        v_totalCount  INTEGER := 0;
	v_del	      INTEGER := 0;
        --
        v_dataid trinetdb.mecdata.mecdataid%TYPE := 0; -- FK PK
        --
	c_mec_dataid CURSOR FOR SELECT mecdataid FROM trinetdb.mecdata WHERE mecid = p_mecid;
        --
        v_table VARCHAR(16) := NULL; -- name of table for next deletions  
        --
    BEGIN
        SELECT pkg_verbose FROM t_global INTO v_pkg_verbose;
        -- Check for valid input mec id
        IF (p_mecid IS NULL OR p_mecid <= 0) THEN
          IF (v_pkg_verbose > 2) THEN 
              RAISE NOTICE 'FYI: delete_mec_data: no-op, invalid input mecid %', p_mecid;
          END IF;
          RETURN 0;
        END IF;
        --
        -- Get ancillary table key ids associated with input mecid
        --
        v_del := 0;
        FOR v_dataid IN c_mec_dataid LOOP
            -- NOTE: Usually multiple mecfreqid PK row associated with mecid
            -- SELECT mecdataid, mecfreqid INTO v_dataid, v_freqid FROM mecdata WHERE mecid = p_mecid;
            --
            -- Delete all table rows associated with mecdata table mecdataid and mecfreqid keys
            --
            -- !!!!  Don't delete mecfreq mecfreqdata rows they are static lookup used all mechs
            --
            -- Delete mecchannel and mecdata rows
            --
            IF (v_dataid > 0) THEN
              v_table := 'MECCHANNEL';
              DELETE FROM trinetdb.mecchannel WHERE mecdataid = v_dataid;
              GET DIAGNOSTICS v_del = ROW_COUNT;
              IF (v_pkg_verbose > 2) THEN
                PERFORM db_cleanup.report_deletion('MECCHANNEL', v_dataid, v_del);
              END IF;
              v_totalCount := v_totalCount + v_del;
              --
              v_table := 'MECDATA';
              DELETE FROM trinetdb.mecdata WHERE mecdataid = v_dataid;
              GET DIAGNOSTICS v_del = ROW_COUNT;
              IF (v_pkg_verbose > 2) THEN 
                PERFORM db_cleanup.report_deletion('MECDATA', v_dataid, v_del); 
              END IF;
              v_totalCount := v_totalCount + v_del;
              --
            END IF;
            --
        END LOOP;
        IF (v_del = 0) THEN
          RAISE NOTICE 'No mecdata found for mecid: %', p_mecid;
        END IF;
        --
        -- Delete mecobject and mec rows
        --
        v_table := 'MECOBJECT';
        DELETE FROM trinetdb.mecobject WHERE mecid = p_mecid;
        GET DIAGNOSTICS v_del = ROW_COUNT;
        IF (v_pkg_verbose > 2) THEN
          PERFORM db_cleanup.report_deletion('MECOBJECT', p_mecid, v_del);
        END IF;
        v_totalCount := v_totalCount + v_del;
        --
        v_table := 'MEC';
        DELETE FROM trinetdb.mec WHERE mecid = p_mecid;
        GET DIAGNOSTICS v_del = ROW_COUNT;
        IF (v_pkg_verbose > 2) THEN
          PERFORM db_cleanup.report_deletion('MEC', p_mecid, v_del);
        END IF;
        v_totalCount := v_totalCount + v_del;
        --
        RETURN v_totalCount; -- Total rows deleted by function call
        --
    EXCEPTION
        WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege to delete from table %', v_table;
          RAISE;
        WHEN OTHERS THEN
          RAISE EXCEPTION 'delete_mec data %', v_table;
    END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all mec table rows associated uniquely with the input origin id.
CREATE OR REPLACE FUNCTION db_cleanup.delete_mecs(p_orid trinetdb.origin.orid%TYPE) RETURNS INTEGER AS $$
    DECLARE
        v_pkg_verbose INTEGER;
	v_pkg_idFormat VARCHAR(15);
	v_mecid_table     BIGINT[];
        v_mecid_count     INTEGER := 0;
        v_totalCount      INTEGER := 0;
        v_mecid           trinetdb.mec.mecid%TYPE;
        v_loopCount       INTEGER := 1;
        v_eventDuplicates  INTEGER := 0;
        ----
        c_mec_chk CURSOR (p_mecid trinetdb.mec.mecid%TYPE, p_orid trinetdb.origin.orid%TYPE) FOR
            SELECT count(*) FROM trinetdb.origin WHERE prefmec = p_mecid AND orid != p_orid;
        --
    BEGIN
        SELECT pkg_verbose,pkg_idFormat FROM t_global INTO v_pkg_verbose,v_pkg_idFormat;
        -- get all mecids for this orid
        v_mecid_table := ARRAY( SELECT mecid FROM trinetdb.mec WHERE oridin = p_orid OR oridout = p_orid );
        SELECT array_length(v_mecid_table,1) INTO v_mecid_count;
	

        -- check for existance of any mec associated with this input orid
        IF (v_mecid_table IS NOT NULL AND v_mecid_count IS NOT NULL) THEN
            IF (v_pkg_verbose > 1)  THEN
                RAISE NOTICE '*delete_mecs* orid: % mecid_count: %.',
                       TO_CHAR(p_orid, v_pkg_idFormat), TO_CHAR(v_mecid_count, '09');
            END IF;
            -- delete all rows assocs with mecids
            FOREACH v_mecid IN ARRAY v_mecid_table LOOP
                -- Check for any other orid's pointing to this mecid
                OPEN c_mec_chk(v_mecid, p_orid);
                FETCH c_mec_chk INTO v_eventDuplicates;
                CLOSE c_mec_chk;
                -- delete mec row only if no other ids reference this mecid
                IF (v_eventDuplicates = 0) THEN
                    v_totalCount := v_totalCount + db_cleanup.delete_mec_data(v_mecid);
                ELSE
                    RAISE NOTICE ' => SAVING data for mecid: % referenced by % other orid.',
                                v_mecid, v_eventDuplicates;
                END IF;
            END LOOP;  -- end of mec loop
        END IF;
        --
        RETURN v_totalCount;
    EXCEPTION
        WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege to delete from mec table';
          RAISE;
        WHEN OTHERS THEN
            RAISE EXCEPTION 'delete_mecs';
END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all table rows associated uniquely with the input origin id parameter.
-- does a commit after each associated table type of data is deleted for input orid
CREATE OR REPLACE FUNCTION db_cleanup.delete_origin(p_orid trinetdb.origin.orid%TYPE) RETURNS INTEGER AS $$
    DECLARE
        v_pkg_verbose INTEGER;
        v_pkg_countFormat  VARCHAR(6);
        v_totalCount  INTEGER := 0;
	v_del	      INTEGER := 0;
        v_commid      trinetdb.remark.commid%TYPE;
        ----
    BEGIN
        SELECT pkg_verbose,pkg_countFormat FROM t_global INTO v_pkg_verbose,v_pkg_countFormat;
        --
        -- Moved code block that was here into new function call -aww 01/24/2006 
        v_totalCount := v_totalCount + db_cleanup.delete_origin_mag_assoc(p_orid);
        --
        v_totalCount := v_totalCount + db_cleanup.delete_arrivals(p_orid);
        --
        -- not sure about deletion order for foreign key dependency deletion cascade
        UPDATE trinetdb.origin SET prefmag = NULL, prefmec = NULL WHERE orid = p_orid;
        --
        v_totalCount := v_totalCount + db_cleanup.delete_netmags(p_orid);
        --
        -- added new function call for "mec" below -aww 01/24/2006 
        v_totalCount := v_totalCount + db_cleanup.delete_mecs(p_orid);
        --
        v_totalCount := v_totalCount + db_cleanup.delete_origin_error(p_orid);
        --
        BEGIN
            v_commid := NULL;
            SELECT commid INTO v_commid FROM trinetdb.origin WHERE orid = p_orid; --  implicit cursor, uses index though
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RAISE WARNING 'WARNING - delete_origin : no commid found for orid: % comid: %.',
                        p_orid, v_commid;
        END;
        IF (v_commid IS NOT NULL) THEN
           v_totalCount := v_totalCount + db_cleanup.delete_remark(v_commid);
        END IF;
        --
        v_totalCount := v_totalCount + db_cleanup.delete_credit(p_orid, 'ORIGIN');
        --
        -- DELETE FROM origin WHERE orid = p_orid;
        DELETE FROM trinetdb.origin WHERE orid = p_orid;
	GET DIAGNOSTICS v_del = ROW_COUNT;
        v_totalCount := v_totalCount + v_del;
        -- IF (v_pkg_verbose > 2) THEN db_cleanup.report_deletion('ORIGIN', p_orid, v_del); END IF;
        IF (v_pkg_verbose > 1) THEN
            RAISE NOTICE '    *delete_origin* rows deleted: % for orid: %.',
                TO_CHAR(v_totalCount, v_pkg_countFormat), p_orid;
        END IF;
        RETURN v_totalCount;
    EXCEPTION
        WHEN insufficient_privilege THEN
          RAISE WARNING 'No privilege to delete from origin table';
          RAISE;
        WHEN OTHERS THEN
            RAISE WARNING 'ERROR *delete_origin* % - %. rows deleted: % for orid %. performing ROLLBACK.',
                    SQLSTATE,SQLERRM,v_totalCount, p_orid;
END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all table rows associated uniquely with the input event id parameter's associated origins.
-- only deletes rows associated uniquely with specified origin subtypes
CREATE OR REPLACE FUNCTION db_cleanup.delete_origin_type(p_evid trinetdb.event.evid%TYPE,
                            p_subsrc trinetdb.origin.subsource%TYPE,
                            p_algor trinetdb.origin.subsource%TYPE) RETURNS INTEGER  AS $$
    DECLARE
        v_pkg_verbose     INTEGER;
        v_pkg_countFormat VARCHAR(6);
        v_totalCount      INTEGER := 0;
        v_orid            trinetdb.origin.orid%TYPE;
        v_orid_table      BIGINT[];
        v_orid_count      INTEGER := 0;
        --
    BEGIN
        SELECT pkg_verbose,pkg_countFormat FROM t_global INTO v_pkg_verbose,v_pkg_countFormat;
        -- get all orids for input evid
        v_orid_table := ARRAY(SELECT orid FROM trinetdb.origin
               WHERE evid = p_evid AND subsource = p_subsrc AND algorithm = p_algor );
        SELECT array_length(v_orid_table,1) INTO v_orid_count;
        -- if no orids for this evid, return
        IF (v_orid_count IS NULL) THEN
            RETURN 0;
        END IF;
        -- have orid data for evid
        FOREACH v_orid IN ARRAY v_orid_table LOOP
             v_totalCount := v_totalCount + db_cleanup.delete_origin(v_orid);
        END LOOP;
        IF (v_pkg_verbose > 1) THEN
            RAISE NOTICE '    *delete_origin_type* rows deleted: %', TO_CHAR(v_totalCount, v_pkg_countFormat);
        END IF;
        RETURN v_totalCount;
    EXCEPTION
        WHEN OTHERS THEN
            RAISE WARNING 'ERROR *delete_origin_type* rows deleted: % for evid: % subsource: % algor: %.',
                     v_totalCount, p_evid, p_subsrc, p_algor;
            RAISE;
END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- Deletes waveform trace data file of from file system.
-- creates the pathfile of the disk file from input parameters
-- returns 1 if file deleted, or if file D.N.E., else -1 if error.

-- VCK needs rewriting from scratch
CREATE OR REPLACE FUNCTION db_cleanup.delete_wave_file( p_fileid  trinetdb.filename.fileid%TYPE) RETURNS INTEGER AS $$
    DECLARE
        v_pkg_verbose INTEGER;
        v_fileRoot    trinetdb.waveroots.fileroot%TYPE;   -- root directory name of waveform files
        v_filePath    VARCHAR(511);
        v_directory   VARCHAR(255);
        v_deleteFileStatus INTEGER := 0;
        v_deleteDirStatus  INTEGER := 0;
        c_root CURSOR FOR
            SELECT fileroot FROM trinetdb.waveroots
                WHERE ( (net = p_net) OR (net='*') ) AND (archive LIKE p_archive) AND (wavetype LIKE p_wavetype)
                    AND (status LIKE p_status) AND (wave_fmt LIKE p_fmt);
    BEGIN
        RETURN 0;   --this is to prevent using incomplete function.
        SELECT pkg_verbose FROM t_global INTO v_pkg_verbose;
        IF (p_evid IS NULL OR p_dfile is NULL OR
            p_archive IS NULL OR p_wavetype IS NULL OR p_status IS NULL) THEN
               RAISE data_exception;
        END IF;
        IF (v_pkg_verbose > 1) THEN
            RAISE NOTICE '*    delete_wave_file net: % file: % archive: % wavetype: % status: % fmt: % dir: %.',
                 p_net, p_dfile, p_archive, p_wavetype, p_status, p_fmt, p_subdirName;
        END IF;
        OPEN  c_root;
        LOOP
          FETCH c_root into v_fileroot;
          EXIT WHEN c_root%NOTFOUND;
          IF (p_wavetype = 'T' AND p_status = 'T') THEN
            -- delete trigger
            v_directory := v_fileRoot  || p_evid::text;
            v_filePath  := v_directory || '/' || p_dfile;
            v_deleteFileStatus := db_cleanup.delete_file(v_filePath);
            -- delete the directory corresponding to event id, actually deletes only if empty
            v_deleteDirStatus  := db_cleanup.delete_file(v_directory);
          ELSE
            -- data center archived events should have non-null subdirname
            IF (p_subdirName IS NULL) THEN
                 RAISE data_exception;
            END IF;
            v_filePath := v_fileRoot || '/' || p_subdirName || '/' || p_dfile;
            v_deleteFileStatus := db_cleanup.delete_file(v_filePath);
          END IF;
          IF (v_pkg_verbose > 1) THEN
            RAISE NOTICE '*    delete_wave_file delete_file status: %', v_deleteFileStatus;
            --
            IF (v_deleteFileStatus > 0) THEN
                -- file deleted or has been deleted already
                RAISE NOTICE '.    deleted waveform file for path: %', v_filePath;
            ELSE
                -- absent data, or permission access problem
                RAISE NOTICE '.    No waveform file deleted for path: %', v_filePath;
            END IF;
            IF (v_deleteDirStatus > 0) THEN
                -- must be empty to be deleted or has been deleted already
                RAISE NOTICE '.    deleted empty waveform DIRECTORY file : %', v_directory;
            END IF;
          END IF;
        END LOOP;
        CLOSE c_root;
        --
        RETURN v_deleteFileStatus;
        --
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE WARNING 'ERROR *delete_wave_file*  waveroots data not found, check id: % %.%.%.%.',
                p_evid, p_dfile, p_archive, p_wavetype, p_status;
            RETURN 0;
        WHEN data_exception THEN
          IF (v_pkg_verbose > 2) THEN
            RAISE WARNING 'ERROR *delete_wave_file*  input parameter value(s) for id: % %.%.%.%.',
                p_evid, p_dfile, p_archive, p_wavetype, p_status;
          END IF;
          RETURN 0;
        WHEN OTHERS THEN
          RAISE WARNING 'ERROR *delete_wave_file*  input parameters for id: % %.%.%.%',
                p_evid, p_dfile, p_archive, p_wavetype, p_status;
            RAISE;
END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- Schedule deletion of waveform trace data files for input trigger id.
CREATE OR REPLACE FUNCTION db_cleanup.schedule_event(p_evid trinetdb.event.evid%TYPE) RETURNS INTEGER AS $$
    DECLARE
        v_pkg_verbose INTEGER;
        v_dbName VARCHAR(32) := 'DEFAULT';
        v_state  VARCHAR(16);
        v_rank   INTEGER;
        v_status INTEGER := 0; -- needed for POST_ID return -aww
    BEGIN
        SELECT pkg_verbose,pkg_defaultState,pkg_defaultRank FROM t_global INTO v_pkg_verbose,v_state,v_rank;
        -- v_dbName  := SYS_CONTEXT('USERENV', 'DB_NAME');
        -- last parameters result=0, p_commit=0, do not commit, a commit can cause ORA-01002 fetch on invalid cursor inside waveform loop
        v_status := PCS.POST_ID('DB_CLEANUP', v_dbName, p_evid, v_state, v_rank, 0);
        IF (v_pkg_verbose > 1) THEN
            RAISE NOTICE '    posted id: % to state: % at rank: % return status: %',
                      p_evid, v_state, v_rank, v_status;
        END IF;
        --
        RETURN v_status;
        --
    EXCEPTION WHEN OTHERS THEN
        RETURN -1;
END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all waveform related table rows and waveform file FROM disk.
-- check file existance before attempting to delete it.
CREATE OR REPLACE FUNCTION db_cleanup.delete_waveforms(p_evid trinetdb.event.evid%TYPE) RETURNS INTEGER AS $$
   DECLARE
        v_pkg_verbose     INTEGER;
        v_pkg_init_ok     BOOLEAN;
        v_pkg_countFormat VARCHAR(6);
	v_pkg_wavefile_delete_ok  BOOLEAN := false;
        v_wfid                 trinetdb.waveform.wfid%TYPE;
        v_fileid               trinetdb.waveform.fileid%TYPE;
        v_countWaveEvid        INTEGER := 0;
        v_countFileEvid        INTEGER := 0;
        v_countEventWaveInFile INTEGER := 0;
        v_countDeletedFiles    INTEGER := 0;
        v_totalCount           INTEGER := 0;
	v_del                  INTEGER := 0;
        c_wave CURSOR FOR
                SELECT wfid FROM trinetdb.assocwae WHERE evid = p_evid FOR UPDATE; --  NOWAIT;
        -- use an explicit rather than implicit cursor
        c_file CURSOR (k_wfid trinetdb.waveform.wfid%TYPE) FOR
                SELECT fileid FROM trinetdb.waveform WHERE fileid=k_wfid;
        v_status INTEGER := 0; -- needed for POST_ID return -aww
   BEGIN
        SELECT pkg_verbose,pkg_init_ok,pkg_countFormat,pkg_wavefile_delete_ok FROM t_global INTO v_pkg_verbose,v_pkg_init_ok,v_pkg_countFormat,v_pkg_wavefile_delete_ok;
        IF (NOT v_pkg_init_ok) THEN
          RAISE WARNING 'DB_CLEANUP package initialization failed- tables D.N.E.?, see init()';
          RETURN -1;
        END IF;
        --
	OPEN c_wave;
        LOOP
            -- label <<one_waveform_row_block>>
            BEGIN
	        FETCH c_wave INTO v_wfid;
		EXIT WHEN NOT FOUND;
                DELETE FROM trinetdb.assocwae WHERE CURRENT OF c_wave;  -- delete loop wfid event association
                v_totalCount := v_totalCount + 1;
                -- look for other evid's that are associated with the loop's current waveform row
                SELECT COUNT(*) INTO v_countWaveEvid FROM trinetdb.assocwae WHERE wfid = v_wfid;
                IF (v_countWaveEvid = 0) THEN
		  OPEN c_file(v_wfid);
		  LOOP
                    FETCH c_file INTO v_fileid;
		    EXIT WHEN NOT FOUND;
                    -- delete current waveform row because no other ids are associated with it
                    DELETE FROM trinetdb.waveform WHERE wfid = v_wfid;
                    GET DIAGNOSTICS v_del = ROW_COUNT;
                    v_totalCount := v_totalCount + v_del;
                    IF (v_pkg_verbose > 2) THEN
                      PERFORM db_cleanup.report_deletion('WAVEFORM', v_keyId, v_del);
                    END IF;
                    /*
                    -- only add this block to accomodate continuous data deletions
                    DELETE FROM assoc_wvday WHERE wfid = v_keyId;
                    GET DIAGNOSTICS v_del = ROW_COUNT;
                    v_totalCount := v_totalCount + v_del;
                    IF (v_pkg_verbose > 2) THEN
                      PERFORM db_cleanup.report_deletion('ASSOC_WVDAY', v_keyId, v_del);
                    END IF;
                    */
                    -- Now look for any wfid that uses current waveform's fileid
                    SELECT COUNT(*) INTO v_countFileEvid
                        FROM trinetdb.waveform WHERE waveform.fileid = v_fileid;
                    --
                    -- Check count, if 0, ok to delete the filename row for fileid and the waveform archive file
                    -- only can delete if no children, clones pointing to the parent waveform.
                    -- If clones point to parent wavefile could take min assocwae evid
                    -- but not always true that a cloned event id from a different source event sequence
                    -- is greater than the parent id, use lddate?
                    IF (v_countFileEvid = 0) THEN
                        -- derive canonical name implied by algorithm for triggered waveform file
                        -- ORACLE INSTANCE NEEDS WRITE PERMISSION TO ENABLE FILE DELETION FROM DISK
                        IF (v_pkg_wavefile_delete_ok) THEN
                            v_countDeletedFiles := v_countDeletedFiles + 1;
                            -- 
                            -- Calling function below requires too many grants and permission changes
                            -- so defer wavefile deletion to an evid posting to be processed later
                            --v_countDeletedFiles := v_countDeletedFiles + db_cleanup.delete_wave_file( v_fileid );
                        END IF;
                        -- delete db rows anyway
                        DELETE FROM trinetdb.filename WHERE fileid = v_fileid;
                        GET DIAGNOSTICS v_del = ROW_COUNT;
                        v_totalCount := v_totalCount + v_del;
                        IF (v_pkg_verbose > 2) THEN
                          PERFORM db_cleanup.report_deletion('FILENAME', v_fileid, v_del);
                        END IF;
                    END IF; -- skips block when other evid wfid assocation points to fileid wave file
                  END LOOP;
		  CLOSE c_file;
                END IF; -- skips block when multiple evid's are sharing the loop's current assocwae.wfid
                --
                IF (v_pkg_wavefile_delete_ok  AND v_countDeletedFiles > 0 ) THEN
                    IF (v_pkg_verbose > 1) THEN
                        RAISE NOTICE '*delete_waveforms % waveform files need deletion for evid: %',
                             v_countDeletedFiles, p_evid;
                    END IF;
                    v_status := db_cleanup.schedule_event(p_evid);
                END IF;
            EXCEPTION
                -- select query on waveform file data into r_waveFileDesc failed
                WHEN NO_DATA_FOUND THEN
                    RAISE WARNING 'ERROR *delete_waveforms* : failed waveform join data evid,wfid = % , %.  no row deletion committed.',
                        p_evid,v_keyId;
            END ;
            -- label <<one_waveform_row_block>>
        END LOOP;
	CLOSE c_wave;
        IF (v_pkg_verbose > 1) THEN
            RAISE NOTICE '    *delete_waveforms* % rows deleted: ', TO_CHAR(v_totalCount, v_pkg_countFormat);
            RAISE NOTICE '    *delete_waveforms* % files deleted: ', TO_CHAR(v_countDeletedFiles, v_pkg_countFormat);
        END IF;
        RETURN v_totalCount;
   EXCEPTION
        WHEN insufficient_privilege THEN
          RAISE NOTICE 'No privilege to delete from waveform affiliated table';
        WHEN OTHERS THEN
            RAISE WARNING 'ERROR *delete_waveforms* % - %. rows deleted: % files deleted: % for evid: %. performing ROLLBACK.',
                           SQLSTATE,SQLERRM,v_totalCount,v_countDeletedFiles,p_evid;
   END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- deletes all database data associated uniquely with the input event id parameter.
-- includes event and origin table associations and the triggered waveform data files.
-- Performs a separate commit of the deletions for each table type.
-- For p_selectflag = 0 deletes data for event only if its selectflag is also set to  0.
-- For p_selectflag = 1 deletes data for event, regardless of its archive selection status.
-- returns number of rows deleted
-- see PRAGMA RESTRICT_REFERENCES(NWPS, NRPS)
CREATE OR REPLACE FUNCTION db_cleanup.delete_event_type(p_evid trinetdb.event.evid%TYPE, p_selectflag trinetdb.event.selectflag%TYPE)
    RETURNS INTEGER AS $$
    DECLARE
        v_pkg_verbose     INTEGER;
	v_pkg_init_ok     BOOLEAN;
	v_pkg_idFormat    VARCHAR(15);
        v_pkg_countFormat VARCHAR(6);
        v_totalCount      INTEGER := 0;
        v_selectflag      trinetdb.event.selectflag%TYPE;
        v_commid          trinetdb.event.commid%TYPE;
        v_count           INTEGER := 0;
        v_del             INTEGER := 0;
    BEGIN
        SELECT pkg_verbose,pkg_init_ok,pkg_idFormat,pkg_countFormat FROM t_global
	        INTO v_pkg_verbose,v_pkg_init_ok,v_pkg_idFormat,v_pkg_countFormat;
        IF (NOT v_pkg_init_ok) THEN
          RAISE NOTICE 'DB_CLEANUP package initialization failed- tables D.N.E.?, see init()';
          RETURN -1;
        END IF;
        --
        IF (v_pkg_verbose > 1) THEN
            RAISE NOTICE '    *delete_event_type* selectflag: % processing evid: %',
                              TO_CHAR(p_selectflag,'9'), TO_CHAR(p_evid, v_pkg_idFormat);
        END IF;
        --
        SELECT selectflag,commid FROM trinetdb.event INTO v_selectflag,v_commid WHERE evid = p_evid;
        IF NOT FOUND THEN
          IF (v_pkg_verbose > 1) THEN
            RAISE WARNING 'ERROR *delete_event_type* no event row found for evid = %.', p_evid;
          END IF;
        ELSIF (p_selectflag = 0 AND v_selectflag = 1) THEN
           RAISE WARNING 'ERROR *delete_event_type* aborted for evid : % desired selectflag=0 (trigger) and database row selectflag=1 (archive)', p_evid;
            RETURN 0;
        END IF;
        --
        -- set null foreign key for dependency cascade, note assumes prefmag, prefmec have origin
        UPDATE trinetdb.event SET prefmag = NULL, prefor = NULL, prefmec = NULL WHERE evid = p_evid;
        --
        v_totalCount := v_totalCount + db_cleanup.delete_requests(p_evid); 
        --
        v_totalCount := v_totalCount + db_cleanup.delete_alarms(p_evid); 
        --
        -- aww 12/04 added new table for preferred magtypes:
        v_totalCount := v_totalCount + db_cleanup.delete_prefmags(p_evid); 
        --
        -- aww 2012/04/07 added new table for preferred mectypes :
        v_totalCount := v_totalCount + db_cleanup.delete_prefmecs(p_evid); 
        --
        -- test here for optional eventprefor table -- aww 2014/01/06 
        SELECT COUNT(*) INTO v_count FROM information_schema.tables WHERE table_schema='trinetdb' AND table_name='EVENTPREFOR'; 
        IF ( v_count = 1 ) THEN
            v_totalCount := v_totalCount + db_cleanup.delete_prefors(p_evid); 
        END IF;
        --
        -- delete_all_origin commits, error handle origin delete exception
        v_totalCount := v_totalCount + db_cleanup.delete_all_origins(p_evid);
        -- 
        --
        -- delete_nettrig commits, error handle trigger delete exception
        v_totalCount := v_totalCount + db_cleanup.delete_nettrig(p_evid);
        --
        -- delete_waveforms commits, error handle waveform delete exception
        v_totalCount := v_totalCount + db_cleanup.delete_waveforms(p_evid);
        --
        -- 2010/12/21 code assumes assocevampset table exists in db schema
        -- and that the ampset table rows are deleted already when amps were deleted
        DELETE FROM trinetdb.assocevampset WHERE evid = p_evid;
 	GET DIAGNOSTICS v_del = ROW_COUNT;
        v_totalCount := v_totalCount + v_del;
        --
        IF (v_commid IS NOT NULL) THEN
           v_totalCount := v_totalCount + db_cleanup.delete_remark(v_commid);
        END IF;
        --
        DELETE FROM trinetdb.event WHERE evid = p_evid;
 	GET DIAGNOSTICS v_del = ROW_COUNT;
        v_totalCount := v_totalCount + v_del;
        IF (v_pkg_verbose > 1) THEN
          PERFORM db_cleanup.report_deletion('EVENT', p_evid, v_del);
        END IF;
        --
        IF (v_pkg_verbose > 0) THEN
            RAISE NOTICE '    *delete_event_type* rows deleted: % for evid: %.',
               TO_CHAR(v_totalCount, v_pkg_countFormat), LPAD(p_evid::text, 15);
        END IF;
        --
        v_totalCount := v_totalCount +  db_cleanup.delete_credit(p_evid, 'EVENT');
        --
        RETURN v_totalCount;  -- best guess at total of all schema table rows deleted for event
        --
    EXCEPTION
        WHEN insufficient_privilege THEN
          RAISE EXCEPTION 'No privilege to delete from event table';
        WHEN NO_DATA_FOUND THEN
          RAISE WARNING 'ERROR *delete_event_type* no event row found for evid = %.',p_evid;
            RETURN 0;
        WHEN OTHERS THEN
          RAISE WARNING 'ERROR *delete_event_type* % - %. rows deleted: % for evid: % performing ROLLBACK.',
	                 SQLSTATE,SQLERRM,TO_CHAR(v_totalCount, v_pkg_countFormat), p_evid;
            -- use RAISE_APPLICATION_error(intCode, messageString, replaceStackBoolean)
            -- RAISE_APPLICATION_error(-20101, 'delete_event_type exception', false
            -- RAISE;
            -- RETURN v_totalCount;
            RETURN -1;
  END;
$$ LANGUAGE plpgsql;
-----------------------------------------------------------------------
-----------------------------------------------------------------------
--  Return SVN Id string.
CREATE OR REPLACE FUNCTION db_cleanup.getPkgId() RETURNS VARCHAR AS $$
DECLARE
  v_pkgId VARCHAR := 'Testing'; 
BEGIN
  RETURN v_pkgId;
END;
$$ LANGUAGE plpgsql;
-------------------------------------------------------------------
-------------------------------------------------------------------
-- Package initialize checks for user table names used by package
-- package initialization routine
CREATE OR REPLACE FUNCTION db_cleanup.init() RETURNS VOID AS $$
  DECLARE
    --TYPE t_pkg_req_table IS TABLE OF VARCHAR(32);
    v_pkg_my_tablenames VARCHAR(32)[] := ARRAY[
      'event','remark','waveform','assocwae','filename',
      'origin','netmag','arrival','assocaro',
      'amp','assocamo','assocamm',
      'coda','assoccoo','assoccom',
      'origin_error', 'mec','mecobject',
      'mecdata','mecfreq','mecfreqdata','mecchannel',
      'assocnte','trig_channel','nettrig',
      'eventprefmag', 'eventprefmec', 'credit'
      ,'assocevampset', 'ampset'
      ,'alarm_action', 'request_card'
    ];
    v_pkg_a_table_name VARCHAR(32); -- holds user table name string returned by query
    v_pkg_init_ok BOOLEAN := NULL;
  BEGIN
    -- Test for existence of user tables to be modified before proceeding
    -- to prevent accidental cross-over into other schemas through synonyms
    FOREACH v_pkg_a_table_name IN ARRAY v_pkg_my_tablenames LOOP
      IF NOT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema='trinetdb' AND table_name = v_pkg_a_table_name) THEN 
        v_pkg_init_ok := false;
	RAISE WARNING 'DB_CLEANUP initialize failure; missing required user table: %',v_pkg_a_table_name;
      END IF;
    END LOOP;
    --
    DROP TABLE IF EXISTS t_global;
    CREATE TEMP TABLE t_global(
      -- verbose output
      pkg_verbose            INTEGER     DEFAULT 1,          -- package default logging level
      pkg_init_ok            BOOLEAN     DEFAULT false,
      -- default number format for ids
      pkg_idFormat           VARCHAR(15) DEFAULT '999999999999999',
      pkg_countFormat        VARCHAR(6)  DEFAULT '999999',
      pkg_defaultState       VARCHAR(16) DEFAULT 'TRIGDEL',
      pkg_defaultRank        INTEGER     DEFAULT 100,
      -- related to wavefile deletion which is handled by pcs
      pkg_wavefile_delete_ok BOOLEAN     DEFAULT false
      );
    IF (v_pkg_init_ok IS NULL) THEN
      v_pkg_init_ok := true;
      INSERT INTO t_global (pkg_init_ok) VALUES (v_pkg_init_ok);
      RAISE INFO 'db_cleanup initialized at : %', now();
    ELSE
      RAISE EXCEPTION 'DB_CLEANUP initialization failed!';
    END IF;
    --
END;
$$ LANGUAGE plpgsql;
-----------------------------------------------------------------------
-----------------------------------------------------------------------
