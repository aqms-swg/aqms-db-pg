/**
 The function latlon computes the latitude and longitude in degrees   
 of a point that is specified by a great circle distance and azimuth  
 from north from a given latitude and longitude.                      
                                                                      
 Latitudes and longitudes are given in decimal degrees.               
 The epicentral distance is given in kilometers.                      
 The azimuth is given in decimal degrees.                             
 flag = 0 ==> Returns latitude ; flag <> 0 ==> Returns longitude.     
*/
CREATE OR REPLACE FUNCTION tools.latlon(v_lat DOUBLE PRECISION, v_lon DOUBLE PRECISION, v_km DOUBLE PRECISION, v_az DOUBLE PRECISION, v_flag INTEGER) RETURNS DOUBLE PRECISION AS $$
DECLARE
  p_str varchar;
  p geography;
  p_new geography;
  p_value double precision;
BEGIN
  -- tools.latlon (:delta_lat, :delta_lon, :delta_max, 180., 0) INTO :lat_min;
  p_str := 'SRID=4326;POINT(' || v_lon || ' ' || v_lat || ')';
  p := ST_GeographyFromText(p_str);

  -- RAISE NOTICE USING MESSAGE='p: ' || ST_AsText(p);

  -- find point at distance v_km in direction v_az
  p_new := ST_Project( p, v_km*1000.0, radians(v_az) );
  -- RAISE NOTICE USING MESSAGE='p_new: ' || ST_AsText(p_new);

  IF  ( v_flag = 0 ) THEN
      SELECT * INTO p_value FROM ST_Y(p_new::geometry);
  ELSE
      SELECT * INTO p_value FROM ST_X(p_new::geometry);
  END IF;
  
  RETURN p_value;
EXCEPTION
  WHEN OTHERS THEN
     RAISE NOTICE USING MESSAGE='PostgreSQL Error: ' || SQLSTATE || ' ' || SQLERRM;
     RETURN -9.;
END
$$ LANGUAGE plpgsql;

--  /**************************************************************************
--  * The function inpoly determines if a point (v_lat, v_lon) is within an   *
--  * arbitrary polygon defined by string p. POLYGON((lon lat, lon lat, ..))  *
--  * The function inpoly returns 1 if p is inside  the number of crossings.  *
--  ***************************************************************************/
CREATE OR REPLACE FUNCTION tools.inpoly(v_spoly VARCHAR(5120), v_lat DOUBLE PRECISION, v_lon DOUBLE PRECISION)
RETURNS INTEGER AS $$
DECLARE
result boolean;
p geometry;
region geography;
sqlstr VARCHAR := '';
BEGIN
    -- v_spoly: POLYGON(( lon lat, lon lat, lon lat))
    -- SRID 4326 is WGS84
    p := ST_SetSRID(ST_Point(v_lon, v_lat),4326);
    -- create a geography polygon, convert to geometry
    sqlstr := sqlstr || 'SELECT * FROM ST_GeographyFromText(''SRID=4326;' || v_spoly || ''')';
    EXECUTE sqlstr INTO region;
    result := ST_Within(p,region::geometry);
    IF result THEN
        RETURN 1;
    ELSE
        RETURN 0;
    END IF;
    -- exception block *has to* be at end
    EXCEPTION
        WHEN NO_DATA_FOUND THEN 
            RETURN -1;
        WHEN TOO_MANY_ROWS THEN
            RETURN -1;
END;
$$ LANGUAGE plpgsql;
