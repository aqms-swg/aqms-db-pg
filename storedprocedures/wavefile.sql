--create schema if not exists wavefile authorization code;
--grant usage on schema wavefile to trinetdb_read, trinetdb_execute;

-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return only the name of file containing the waveform specified by input Waveform.wfid.
-- Example: call WAVEFILE.dfile(1111) into :v_string;
--
CREATE OR REPLACE FUNCTION wavefile.dfile(p_wfid Waveform.wfid%TYPE) RETURNS VARCHAR(32) AS $$
  DECLARE
    v_dfile VARCHAR(32);
  BEGIN
    --
    SELECT dfile INTO v_dfile FROM Waveform w LEFT OUTER JOIN Filename f
        ON w.fileid = f.fileid
        WHERE w.wfid = p_wfid;
    --
    RETURN v_dfile;
    --
  EXCEPTION WHEN NO_DATA_FOUND THEN
     RETURN NULL;
  END;
$$ LANGUAGE plpgsql;


-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return Event.evid associated with input waveform id (Waveform.wfid) in AssocWaE table.
-- If multiple evid are associated with wfid return the evid of earliest loaded row.
-- Example: call WAVEFILE.getHotEvid(1111) into :v_number;
-- Defaults are triggered, temporary, miniseed ?
CREATE OR REPLACE FUNCTION wavefile.getHotEvid(p_wfid Waveform.wfid%TYPE) RETURNS BIGINT AS $$
    --
  DECLARE
    v_evid BIGINT := 0;
    --
    -- Get list of AssocWaE rows connected to this waveform.
    -- In the case there are more than one, get the earliest (smallest lddate)
    -- c_evid is a "bound cursor", i.e when OPEN is called it will execute the query
    c_evid CURSOR FOR
           SELECT evid FROM AssocWaE WHERE wfid = p_wfid ORDER BY lddate;
    --
  BEGIN
    --
    OPEN  c_evid; -- OPEN executes the cursor SELECT statment
    FETCH c_evid INTO v_evid;
    CLOSE c_evid;
    IF (v_evid IS NULL) THEN  -- No entries 
      v_evid := 0;
    END IF;
    --
    RETURN v_evid;
    --
  END;
$$ LANGUAGE plpgsql;

--


-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return only the directory part of the pathname of the file specified by input Waveform.wfid
-- whose WAVEFORM table row column values match those of a WAVEROOT table row whose wcopy column
-- value is 1 and whose net column value either matches exactly or is '*'.
-- Example: call WAVEFILE.path(1111) into :v_string;
CREATE OR REPLACE FUNCTION wavefile.path(p_wfid Waveform.wfid%TYPE, p_copy Waveroots.wcopy%TYPE) RETURNS VARCHAR AS $$
  --
  DECLARE
        v_fileid     Waveform.fileid%TYPE := NULL;
        v_arch       Waveform.archive%TYPE := NULL; 
        v_net        Waveform.net%TYPE := NULL;
        -- added sta and seedchan for wavetype='C' type subdir string building
        v_sta        Waveform.sta%TYPE := NULL;
        v_seed       Waveform.seedchan%TYPE := NULL;
        v_loc        Waveform.location%TYPE := NULL;
        v_wavetype   Waveform.wavetype%TYPE := NULL;
        v_status     Waveform.status%TYPE := NULL;
        v_fmt        Waveform.wave_fmt%TYPE := NULL;
        v_datetime   Waveform.datetime_on%TYPE;
        v_subdirname Subdir.subdirname%TYPE := NULL;
        v_pathname   VARCHAR(255) := NULL;
        v_year       VARCHAR(4) := NULL;
        v_mo         VARCHAR(2) := NULL;
        v_date       DATE := NULL;
  --
  BEGIN
        --SELECT fileid, net, sta, seedchan, location,archive, wavetype, status, wave_fmt, truetime.getEpoch(datetime_on,'UTC')
        SELECT fileid, net, sta, seedchan, location,archive, wavetype, status, wave_fmt, datetime_on
            INTO v_fileid, v_net, v_sta, v_seed, v_loc,v_arch, v_wavetype, v_status, v_fmt, v_datetime
            FROM Waveform WHERE wfid = p_wfid;
        --
        -- BK wave archiver populates SUBDIR
        SELECT subdirname INTO v_subdirname FROM Filename LEFT OUTER JOIN Subdir
            ON Filename.subdirid = Subdir.subdirid
            WHERE Filename.fileid = v_fileid;
        --
        -- RAISE NOTICE USING MESSAGE = v_net || v_arch || v_wavetype || v_status || v_fmt || v_datetime || p_copy;
        v_pathname := wavefile.root(v_net, v_arch, v_wavetype, v_status, v_fmt, v_datetime, p_copy);
        -- RAISE NOTICE '%', v_pathname;
        --
        -- Else for repositories using the CI type wave archiver:
        -- construct a subdir element by formula:
        IF (v_subdirname IS NULL) THEN
          IF (v_status = 'A') THEN -- for archived C or T wavetype 
              -- do date conversion once
              -- Ignore seconds since ORACLE date only accept 59 seconds max, not allowing 60 leap
              v_date := TO_DATE(SUBSTR(truetime.true2string( truetime.getEpoch(v_datetime, 'UTC') ),1,10), 'YYYY/MM/DD');
              -- Year e.g. "2000"
              -- Note: earlier use of 'IYYY' resulted in WRONG answer on 1/1/leap-year!
              v_year := CAST( EXTRACT(YEAR from v_date) AS TEXT);
              -- Month e.g. "04"
              v_mo   := CAST( EXTRACT(MONTH from v_date) AS TEXT);
              -- build the path/filename e.g. <root>/2001/200104/
              v_subdirname := v_year || '/' || v_year || v_mo ;
              --
          ELSIF (v_status = 'T') THEN -- Temporary
              IF (v_wavetype = 'T') THEN  -- triggered
                  v_subdirname := CAST(wavefile.getHotEvid(p_wfid) AS TEXT);
              ELSIF (v_wavetype = 'C') THEN -- continuous
                IF (v_loc = '  ') THEN
                  v_subdirname := v_net||'.'||v_sta||'.'||v_seed;
                ELSE
                  v_subdirname := v_net||'.'||v_sta||'.'||v_seed||'.'||v_loc;
                END IF;
              END IF;
          END IF;
        END IF;
        --
        -- NOTE: assumes v_pathname has '/' already appended:
        RETURN  v_pathname || v_subdirname || '/';
        --
  EXCEPTION WHEN NO_DATA_FOUND THEN
     RETURN NULL;
  END;
$$ LANGUAGE plpgsql;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return only the directory part of the pathname of the file specified by input Waveform.wfid
-- whose WAVEFORM table row column values match those of a WAVEROOT table row whose wcopy column
-- value matches the input parameter value and whose net column value either matches exactly or is '*'.
-- ---------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION wavefile.path(p_wfid Waveform.wfid%TYPE) RETURNS VARCHAR AS $$
  BEGIN
    RETURN wavefile.path(p_wfid, 1.0);
  END;
$$ LANGUAGE plpgsql;


-- ---------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS wavefile.doRoots(p_net Waveform.net%TYPE,
                   p_archive Waveform.archive%TYPE,
                   p_wavetype Waveform.wavetype%TYPE,
                   p_status Waveform.status%TYPE,
                   p_fmt Waveform.wave_fmt%TYPE,
                   p_datetime Waveform.datetime_on%TYPE,
                   p_copy Waveroots.wcopy%TYPE,
                   p_action INTEGER
                  );

-- define function with default input parameters
-- find the filename root.
-- p_action = 1: return number of file roots
-- p_action = 2: print list of roots to screen (raise notice)
-- p_action = other (e.g. 3): return the file root
CREATE OR REPLACE FUNCTION wavefile.doRoots(p_net Waveform.net%TYPE = NULL,
                   p_archive Waveform.archive%TYPE = NULL,
                   p_wavetype Waveform.wavetype%TYPE = NULL,
                   p_status Waveform.status%TYPE = NULL, 
                   p_fmt Waveform.wave_fmt%TYPE = NULL,
                   p_datetime Waveform.datetime_on%TYPE = NULL,
                   p_copy Waveroots.wcopy%TYPE = NULL,
                   p_action INTEGER = NULL
                  ) RETURNS VARCHAR AS $$
  DECLARE
    --
    -- RH:  most of these "default" values are actually NULL, will leave in for now.
    pkg_def_wtype Waveform.wavetype%TYPE := NULL;
    pkg_def_wstatus Waveform.status%TYPE := NULL;
    pkg_def_wfmt Waveform.wave_fmt%TYPE := NULL;
    pkg_def_net CONSTANT VARCHAR(2) := '*';
    pkg_def_archive CONSTANT VARCHAR(8) := 'SCEDC';

    v_net  Waveform.net%TYPE := NULL;
    v_arch Waveform.archive%TYPE := NULL; 
    v_wave Waveform.wavetype%TYPE := NULL; 
    v_stat Waveform.status%TYPE := NULL;
    v_fmt  Waveform.wave_fmt%TYPE := NULL;
    v_date Waveform.datetime_on%TYPE := NULL;

    v_use_this_net Waveform.net%TYPE := NULL;
    --
    v_tab_record RECORD;
    --
    v_tab_roots_rowcount INTEGER := 0;

    v_cnt INTEGER := 0;
    v_root VARCHAR := NULL;
    v_result VARCHAR := NULL;

    c_count_by_net CURSOR (network VARCHAR) FOR
           SELECT count(*) FROM Waveroots wr
           WHERE (wr.net = network) AND (wr.archive = v_arch) AND (wr.wavetype = v_wave) AND (wr.status = v_stat) AND (wr.wave_fmt = v_fmt) AND
           wr.dateTime_on <= v_date AND wr.dateTime_off > v_date;
    c_wf_copies CURSOR (network VARCHAR) FOR
           SELECT wr.net, wr.wave_fmt, wr.fileroot, wr.wcopy FROM Waveroots wr
           WHERE wr.net = network AND wr.archive = v_arch AND wr.wavetype = v_wave AND wr.status = v_stat AND (wr.wave_fmt = v_fmt) AND
               wr.datetime_on <= v_date AND wr.datetime_off > v_date
           ORDER BY wcopy;
    --
  BEGIN
     --
     IF (p_net IS NOT NULL) THEN
       v_net := p_net;
     ELSE
       v_net := pkg_def_net;
     END IF;
     --
     --
     -- Defaults are triggered, temporary, miniseed ?
     v_arch := coalesce(p_archive, pkg_def_archive); 
     v_wave := coalesce(p_wavetype, pkg_def_wtype);
     v_stat := coalesce(p_status, pkg_def_wstatus);
     v_fmt  := coalesce(p_fmt, pkg_def_wfmt);
     --
     IF ( p_datetime IS NULL) THEN
       v_date := truetime.putEpoch(truetime.string2true(TO_CHAR(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP),'YYYY/MM/DD HH24:MI:SS')), 'UTC');
     ELSE
       v_date := p_datetime;
     END IF;
     --
     --RAISE NOTICE USING MESSAGE = 'v_net: ' || v_net || 'v_arch: ' || v_arch || 'v_wave: ' || v_wave || 'v_stat: ' || v_stat || 'v_date: ' || truetime.true2string(cast(v_date AS BIGINT));
     --RAISE NOTICE USING MESSAGE = 'p_action: ' || p_action || ' p_fmt: ' || p_fmt;
     --
     IF (v_net != pkg_def_net) THEN -- Input net is not the wildcard net
         /*
          NOTE that "datetime_on <= p_datetime" AND "datetime_off > p_datetime"
          The "<=" at the start and the ">" at the end insures that a request right at
          the transition time won't return two paths.
         */
           -- RAISE NOTICE USING MESSAGE = 'About to check for specific network v_net: ' || v_net;
           OPEN c_count_by_net (network := v_net);
           FETCH FROM c_count_by_net INTO v_tab_roots_rowcount;
           CLOSE c_count_by_net;
           v_use_this_net := v_net;
     END IF;
     -- RAISE NOTICE USING MESSAGE = 'After checking for specific network v_tab_roots_rowcount: ' || v_tab_roots_rowcount;
     --
     -- If no match, try for a wildcard net match
     IF (v_tab_roots_rowcount = 0) THEN
           --RAISE NOTICE USING MESSAGE = 'About to check for default network pkg_def_net: ' || pkg_def_net;
           OPEN c_count_by_net (network := pkg_def_net);
           FETCH FROM c_count_by_net INTO v_tab_roots_rowcount;
           CLOSE c_count_by_net;
           v_use_this_net := pkg_def_net;
     END IF;
     --RAISE NOTICE USING MESSAGE = 'After checking for wildcard network v_tab_roots_rowcount: ' || v_tab_roots_rowcount;

     -- if STILL no match, return NULL
     IF (v_tab_roots_rowcount = 0) THEN
         RAISE NOTICE 'NO ROOTS FOUND: %', v_tab_roots_rowcount;
         RETURN NULL;
     END IF;
     --

     -- Will only get to this section if there actually is a root to be found.
     --RAISE NOTICE '%', v_use_this_net;
     OPEN c_wf_copies ( network := v_use_this_net );
     LOOP
       FETCH c_wf_copies INTO v_tab_record;
           EXIT WHEN NOT FOUND;
       IF (p_action = 2) THEN
         RAISE NOTICE USING MESSAGE = 'net:' || v_tab_record.net ||  ' fmt:' || v_tab_record.wave_fmt ||
                      ' root:' || v_tab_record.fileroot || ' copy:' || v_tab_record.wcopy;
       END IF;
       -- trim trailing blanks from fileroot and add slash if is isn't there yet
       v_root := TRIM(TRAILING ' ' FROM v_tab_record.fileroot);
       -- RAISE NOTICE '%', v_root;
       v_cnt := LENGTH(v_root);
       IF (v_cnt > 0) THEN 
          IF (SUBSTR(v_root, v_cnt) != '/') THEN
            v_root := v_root || '/';
          END IF;
       END IF;

       IF ( v_tab_record.wcopy = p_copy ) THEN
           v_result := v_root;
       END IF;
     END LOOP;

     CLOSE c_wf_copies;
 
     -- Return total count  if p_action is 1 or 2
     IF (p_action = 1 OR p_action = 2) THEN
       RETURN CAST (v_tab_roots_rowcount AS VARCHAR);
     END IF;

     -- Return pathname root or NULL
     RETURN v_result;
     --
  EXCEPTION WHEN OTHERS THEN
     RETURN NULL;
  END; 
$$ LANGUAGE plpgsql CALLED ON NULL INPUT;

-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
--
-- Return the root directory path of the waveform file archive whose WAVEROOT table row 
-- matches the specified input parameters and is valid (active) for the current time
-- and whose wave_fmt column values is 2 (miniseed), whose wcopy column value is 1,
-- and whose net column value matches exactly or is '*'.
-- Null input net defaults to '*'.
-- RH: Original function passed p_datetime NULL to doRoots, doesn't seem to work.
-- create p_datetime here.
-- Root time is NULL -> NOW, wave_fmt = 2 miniseed, wcopy = 1
CREATE OR REPLACE FUNCTION wavefile.root(p_net Waveform.net%TYPE,
                p_archive Waveform.archive%TYPE,
                p_wavetype Waveform.wavetype%TYPE,
                p_status Waveform.status%TYPE
               ) RETURNS VARCHAR AS $$
  DECLARE
    p_datetime Waveform.datetime_on%TYPE;
  BEGIN
    p_datetime :=  truetime.putEpoch(truetime.string2true(TO_CHAR(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP),'YYYY/MM/DD HH24:MI:SS')), 'UTC');
    RETURN wavefile.doRoots(p_net, p_archive, p_wavetype, p_status, CAST(2 AS SMALLINT), p_datetime, 1, 3); 
    --
  END;
$$ LANGUAGE plpgsql;

-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return the root directory path of the waveform file archive whose WAVEROOT table row 
-- matches the specified input parameters and is valid (active) for the current time
-- and whose wcopy column value is 1, and whose net column value matches exactly or is '*'.
-- Null input net defaults to '*'.
-- RH: Original function passed p_datetime NULL to doRoots, doesn't seem to work.
-- create p_datetime here.
-- Root time is NULL -> NOW, wcopy = 1
CREATE OR REPLACE FUNCTION wavefile.root(p_net Waveform.net%TYPE,
                p_archive Waveform.archive%TYPE,
                p_wavetype Waveform.wavetype%TYPE,
                p_status Waveform.status%TYPE,
                p_fmt Waveform.wave_fmt%TYPE
               ) RETURNS VARCHAR AS $$
  DECLARE
     p_datetime Waveform.datetime_on%TYPE;
  BEGIN
    p_datetime :=  truetime.putEpoch(truetime.string2true(TO_CHAR(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP),'YYYY/MM/DD HH24:MI:SS')), 'UTC');
    RETURN wavefile.doRoots(p_net, p_archive, p_wavetype, p_status, p_fmt, p_datetime, 1, 3); 
    --
  END;
$$ LANGUAGE plpgsql;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return root directory path of the file archive whose WAVEROOT table row matching the
-- specified by input parameters, whose net column value matches exactly or is '*'.
-- Null input net defaults to '*'.
-- Null input datetime corresponds to those rows active for current time, i. e.
-- rows whose datetime_on to datetime_off range includes the time.
-- input datetime time should be same time base as database (true or nominal)
-- Example: call WAVEFILE.root('BK', 'NCEDC', 'T', 'A', 2, NULL, 1) into :v_string;
CREATE OR REPLACE FUNCTION wavefile.root(p_net Waveform.net%TYPE,
                p_archive Waveform.archive%TYPE,
                p_wavetype Waveform.wavetype%TYPE,
                p_status Waveform.status%TYPE, 
                p_fmt Waveform.wave_fmt%TYPE,
                p_datetime Waveform.datetime_on%TYPE,
                p_copy Waveroots.wcopy%TYPE
               ) RETURNS VARCHAR AS $$
  BEGIN
      RETURN wavefile.doRoots(p_net, p_archive, p_wavetype, p_status, p_fmt, p_datetime, p_copy, 3); 
  END;
$$ LANGUAGE plpgsql;
