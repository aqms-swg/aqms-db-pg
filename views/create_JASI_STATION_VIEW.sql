CREATE OR REPLACE  VIEW JASI_STATION_VIEW 
  (NET, STA, LAT, LON, ELEV, STANAME, ONDATE, OFFDATE) AS
    select net,sta,lat,lon,elev,staname,ondate,offdate from station_data;         
--
