CREATE OR REPLACE  VIEW JASI_CONFIG_VIEW (NAME, PROGID, 
    NET, STA, SEEDCHAN, LOCATION, CONFIG, ONDATE, OFFDATE) AS         
      select p.name,c.progid,c.net,c.sta,c.seedchan,c.location,c.config,
        c.ondate,c.offdate
      from appchannels c, applications p where p.progid=c.progid;                 
--
