CREATE OR REPLACE VIEW ML_MAGPARMS_VIEW (NET, STA, SEEDCHAN, LOCATION, CHANNEL, 
CHANNELSRC, ONDATE, OFFDATE, CORR, CORR_FLAG, CORR_TYPE, AUTH, CLIP, SUMMARY_WT) AS
SELECT distinct cd.net, cd.sta, cd.seedchan, cd.location, cd.channel,cd.channelsrc, 
cd.ondate, cd.offdate, sc.corr, sc.corr_flag, sc.corr_type, sc.auth, cd.clip, 
-- Note: hardcoded summary_wt here, for variable wts you would need to add another table column to join
1.0 summary_wt                                                         
FROM channelmap_ampparms cd LEFT OUTER JOIN stacorrections sc 
ON 
cd.net = sc.net
AND cd.sta = sc.sta
AND cd.seedchan = sc.seedchan
AND cd.location = sc.location
WHERE sc.corr_type = 'ml'
  AND sc.corr_flag <> 'D';
