CREATE OR REPLACE  VIEW JASI_AMPPARMS_VIEW 
  (NET, STA, SEEDCHAN, LOCATION, ONDATE, OFFDATE, MAXAMP, CLIP) AS
    SELECT net,sta,seedchan,location,ondate,offdate,clip,clip 
FROM channelmap_ampparms;
--
