# sequence {#man-db-sequence}
## Overview
 version: 1.0.1 2021-06-16
 
 used in production: yes, utc version
 
 functions for retrieving sequence values.
 
 dependent applications:
 
 - trimag, ampgen,
 
 depends on:
 
 - sequence schema has to exist.
 
 examples:
 ```
 select * from sequence.getIncrement('ampseq');
 getincrement
 --------------
 5
 (1 row)
 
 archdb=> \gset
 archdb=> select sequence.getNext('ampseq',3) a seqrange;
 seqrange
 -------------------
 12768593-12768603
 (1 row)
 ```
 The sequence range is parsed by functions aware of the increment size
 in the tndb/Database class, which should return the three sequence
 values: `12768593, 12768598, 12768603`
 
## Functions
 ### sequence.getPkgId() RETURNS varchar(80) 
 get version number
 ### sequence.getIncrement(seqname varchar) RETURNS bigint 
 get increment value of the named sequence
 ### sequence.getNext(p_seqname VARCHAR, p_numseq INTEGER) RETURNS VARCHAR 
 Return a string containing a range of requested sequence numbers.
 seqnr1 - (seqnr_numseq - increment)
 If something else has advanced the sequence at the same time, the
 sequences may not advance sequentially with step increment, in that case
 it can return multiple sequence ranges. For example: '2314134-2314135 2314138-2314488'
 The list can also have isolated sequence numbers, for example:
 '2314134-2314135 2314138-2314488 2314490 2314498'
 The sequence name is specified by input p_seqname.
 The number of desired sequenced numbers is specified by in/out p_numseq.
 The number of sequence numbers returned may be less than that requested,
 the maximum output size of the varchar is 6000
 The total is returned to the p_numseq parameter.
 ### sequence.getSeqTable(p_seqname VARCHAR, p_numseq INTEGER) RETURNS SETOF BIGINT 
 Pipelined function return can be cast as table to get result set
 e.g. "Select column_value from table (sequence.getSeqTable('ARSEQ',?));"
 where the number of desired sequenced numbers is specified by bind variable ?
